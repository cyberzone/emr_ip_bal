﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
   public  class EMR_PTProcedure
    {
        dbOperation objDB = new dbOperation();

        public string BRANCH_ID { set; get; }
        public string EPP_ID { set; get; }
        public string EPP_DIAG_CODE { set; get; }
        public string EPP_DIAG_NAME { set; get; }
        public string EPP_COST { set; get; }
        public string EPP_REMARKS { set; get; }


        public string EPP_QTY { set; get; }
        public string EPP_PRICE { set; get; }
        public string EPP_ORDERTYPE { set; get; }

        public string EPP_USEPRICE { set; get; }
        public string EPP_NOTES { set; get; }

        public string EPP_TEMPLATE_CODE { set; get; }
        public string Action { set; get; }


        public DataSet ProceduresGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_PTProceduresGet", Param);

            return DS;


        }


        public void ProceduresAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPP_BRANCH_ID", BRANCH_ID);
            Param.Add("EPP_ID", EPP_ID);
            Param.Add("EPP_DIAG_CODE", EPP_DIAG_CODE);
            Param.Add("EPP_DIAG_NAME", EPP_DIAG_NAME);
            Param.Add("EPP_COST", EPP_COST);
            Param.Add("EPP_REMARKS", EPP_REMARKS);
            Param.Add("EPP_QTY", EPP_QTY);
            Param.Add("EPP_PRICE", EPP_PRICE);
            Param.Add("EPP_ORDERTYPE", EPP_ORDERTYPE);
            Param.Add("EPP_USEPRICE", EPP_USEPRICE);
            Param.Add("EPP_NOTES", EPP_NOTES);
            Param.Add("Action", Action);
            Param.Add("EPP_TEMPLATE_CODE", EPP_TEMPLATE_CODE);

            objDB.ExecuteNonQuery("HMS_SP_EMR_ProceduresAdd", Param);

        }




        public void ProceduresDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPP_BRANCH_ID", BRANCH_ID);
            Param.Add("EPP_ID", EPP_ID);
            Param.Add("EPP_DIAG_CODE", EPP_DIAG_CODE);

            objDB.ExecuteNonQuery("HMS_SP_EMR_ProceduresDelete", Param);

        }
    }
}
