﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;


namespace EMR_IP_BAL
{
    public class EMR_PTPharmacy
    {



        dbOperation objDB = new dbOperation();

        public string BRANCH_ID { set; get; }
        public string EPP_ID { set; get; }
        public string EPP_PHY_CODE { set; get; }
        public string EPP_PHY_NAME { set; get; }
        public string EPP_DOSAGE { set; get; }
        public string EPP_ROUTE { set; get; }
        public string EPP_REFILL { set; get; }
        public string EPP_LIFE_LONG { set; get; }
        public string EPP_DURATION { set; get; }
        public string EPP_DURATION_TYPE { set; get; }
        public string EPP_START_DATE { set; get; }
        public string EPP_END_DATE { set; get; }
        public string EPP_REMARKS { set; get; }
        public string EPP_REFERENCE { set; get; }
        public string EPP_QTY { set; get; }
        public string EPP_ICD { set; get; }
        public string EPP_DOSAGE1 { set; get; }
        public string EPP_TYPE { set; get; }
        public string EPP_INV_IsuNo { set; get; }
        public string EPP_HHS_CODE { set; get; }
        public string EPP_TEMPLATE_CODE { set; get; }
        public string EPP_REF_OUTSIDE { set; get; }
        public string EPP_TAKEN { set; get; }
        public string EPP_TIME { set; get; }
        public string EPP_UNITTYPE { set; get; }
        public string EPP_FREQUENCY { set; get; }
        public string EPP_FREQUENCYTYPE { set; get; }
        public string EPP_UNIT { set; get; }

        public string SelectedEMR_ID { set; get; }
        public string SelectedBranch_ID { set; get; }





     public string TemplateCode{ set; get; }

        public DataSet PharmacyGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_PTPharmacyGet", Param);

            return DS;


        }


        public DataSet WEMR_spS_GetPharmacyHistoryList(string filenumber)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("filenumber", filenumber);
            DS = objDB.ExecuteReader("WEMR_spS_GetPharmacyHistoryList", Param);

            return DS;


        }


        public void PharmacyAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPP_BRANCH_ID", BRANCH_ID);
            Param.Add("EPP_ID", EPP_ID);
            Param.Add("EPP_PHY_CODE", EPP_PHY_CODE);
            Param.Add("EPP_PHY_NAME", EPP_PHY_NAME);
            Param.Add("EPP_DOSAGE", EPP_DOSAGE);
            Param.Add("EPP_ROUTE", EPP_ROUTE);
            Param.Add("EPP_REFILL", EPP_REFILL);
            Param.Add("EPP_LIFE_LONG", EPP_LIFE_LONG);
            Param.Add("EPP_DURATION", EPP_DURATION);
            Param.Add("EPP_DURATION_TYPE", EPP_DURATION_TYPE);
            Param.Add("EPP_START_DATE", EPP_START_DATE);
            Param.Add("EPP_END_DATE", EPP_END_DATE);
            Param.Add("EPP_REMARKS", EPP_REMARKS);
            Param.Add("EPP_REFERENCE", EPP_REFERENCE);
            Param.Add("EPP_QTY", EPP_QTY);
            Param.Add("EPP_ICD", EPP_ICD);
            Param.Add("EPP_DOSAGE1", EPP_DOSAGE1);
            Param.Add("EPP_TYPE", EPP_TYPE);
            Param.Add("EPP_INV_IsuNo", EPP_INV_IsuNo);
            Param.Add("EPP_HHS_CODE", EPP_HHS_CODE);
            Param.Add("EPP_TEMPLATE_CODE", EPP_TEMPLATE_CODE);
            Param.Add("EPP_REF_OUTSIDE", EPP_REF_OUTSIDE);
            Param.Add("EPP_TAKEN", EPP_TAKEN);
            Param.Add("EPP_TIME", EPP_TIME);
            Param.Add("EPP_UNITTYPE", EPP_UNITTYPE);
            Param.Add("EPP_FREQUENCY", EPP_FREQUENCY);
            Param.Add("EPP_FREQUENCYTYPE", EPP_FREQUENCYTYPE);
            Param.Add("EPP_UNIT", EPP_UNIT);


            objDB.ExecuteNonQuery("HMS_SP_EMR_PharmacyAdd", Param);

        }


        public void PharmacyDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPP_BRANCH_ID", BRANCH_ID);
            Param.Add("EPP_ID", EPP_ID);
            Param.Add("EPP_PHY_CODE", EPP_PHY_CODE);

            objDB.ExecuteNonQuery("HMS_SP_EMR_PharmacyDelete", Param);

        }


        public DataSet  PharmacyTemplatGe(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMR_PharmacyTemplatGet", Param);

            return DS;


        }

        public void  PharmacyTemplateAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BRANCH_ID);
            Param.Add("patientmasterid", EPP_ID);
            Param.Add("code", TemplateCode);

            objDB.ExecuteNonQuery("HMS_SP_EMR_PharmacyTemplateAdd", Param);

        }


        public void  PharmacyTemplateDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BRANCH_ID);
            Param.Add("patientmasterid", EPP_ID);
            Param.Add("code", TemplateCode);

            objDB.ExecuteNonQuery("HMS_SP_EMR_PharmacyTemplateDelete", Param);

        }


        public void  PharmacyAddFromTemplate()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BRANCH_ID);
            Param.Add("patientmasterid", EPP_ID);
            Param.Add("code", TemplateCode);

            objDB.ExecuteNonQuery("HMS_SP_EMR_PharmacyAddFromTemplate", Param);

        }


        public void  PharmacyAddFromHistory()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BRANCH_ID);
            Param.Add("patientmasterid", EPP_ID);
            Param.Add("SelectedEMR_ID", SelectedEMR_ID);
            Param.Add("SelectedBranch_ID", SelectedBranch_ID);

            objDB.ExecuteNonQuery("HMS_SP_EMR_PharmacyAddFromHistory", Param);

        }

    }
}