﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
   public  class IP_PTProcedure
    {

        dbOperation objDB = new dbOperation();

        public string BRANCH_ID { set; get; }
        public string IPP_ID { set; get; }
        public string IPP_PRO_ID { set; get; }
        public string IPP_PRO_CODE { set; get; }
        public string IPP_PRO_NAME { set; get; }
        public string IPP_COST { set; get; }
        public string IPP_REMARKS { set; get; }


        public string IPP_QTY { set; get; }
        public string IPP_PRICE { set; get; }
        public string IPP_ORDERTYPE { set; get; }

        public string IPP_USEPRICE { set; get; }
        public string IPP_NOTES { set; get; }

        public string IPP_TEMPLATE_CODE { set; get; }
        public string IPP_SO_TRANS_ID { set; get; }

        public DataSet IPProceduresGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTProceduresGet", Param);

            return DS;


        }


        public void IPProceduresAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPP_BRANCH_ID", BRANCH_ID);
            Param.Add("IPP_ID", IPP_ID);
            Param.Add("IPP_PRO_ID", IPP_PRO_ID);
            Param.Add("IPP_PRO_CODE", IPP_PRO_CODE);
            Param.Add("IPP_PRO_NAME", IPP_PRO_NAME);
            Param.Add("IPP_COST", IPP_COST);
            Param.Add("IPP_REMARKS", IPP_REMARKS);
            Param.Add("IPP_QTY", IPP_QTY);
            Param.Add("IPP_PRICE", IPP_PRICE);
            Param.Add("IPP_ORDERTYPE", IPP_ORDERTYPE);
            Param.Add("IPP_USEPRICE", IPP_USEPRICE);
            Param.Add("IPP_NOTES", IPP_NOTES);
            Param.Add("IPP_TEMPLATE_CODE", IPP_TEMPLATE_CODE);
            Param.Add("IPP_SO_TRANS_ID", IPP_SO_TRANS_ID);

            objDB.ExecuteNonQuery("HMS_SP_IP_PTProceduresAdd", Param);

        }




        public void IPProceduresDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPP_BRANCH_ID", BRANCH_ID);
            Param.Add("IPP_ID", IPP_ID);
            Param.Add("IPP_PRO_ID", IPP_PRO_ID);
            Param.Add("IPP_SO_TRANS_ID", IPP_SO_TRANS_ID);

            objDB.ExecuteNonQuery("HMS_SP_IP_PTProceduresDelete", Param);

        }
    }
}
