﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;


namespace EMR_IP_BAL
{
    public class EMRSBAL
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStr"].ToString());

        public void conopen()
        {
            if (con.State == ConnectionState.Closed)
                con.Open();

        }

        public void conclose()
        {

            if (con.State == ConnectionState.Open)
                con.Close();



        }

        dbOperation objDB = new dbOperation();

        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string location { set; get; }
        public string quality { set; get; }

        public string severity { set; get; }
        public string signssymptoms { set; get; }
        public string chronicstatus { set; get; }
        public string timing { set; get; }


        public string modifyingfactor { set; get; }
        public string historyreason { set; get; }
        public string limitedhistory { set; get; }
        public string context { set; get; }
        public string duration { set; get; }

        public string examinationtype { set; get; }



        public string type { set; get; }
        public string subtype { set; get; }
        public string selected { set; get; }
        public string comments { set; get; }
        public string number { set; get; }
        public string points { set; get; }
        public string score { set; get; }
        public string fieldname { set; get; }


        public string FileNumber { set; get; }
        public string HPILevel { set; get; }
        public string ROS { set; get; }
        public string PFSH { set; get; }
        public string DocumentedHistory { set; get; }
        public string ProblemPointScored { set; get; }
        public string DataPointScored { set; get; }
        public string RiskManagement { set; get; }
        public string MDM { set; get; }
        public string CPTCode { set; get; }


        public DataSet WEMR_spS_GetEMRS()
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetEMRS", Param);



            return DS;

        }

        public DataSet WEMR_spS_GetEMRSDatas()
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetEMRSDatas", Param);



            return DS;

        }



        public void SaveEMRSHPI()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            Param.Add("location", location);
            Param.Add("quality", quality);
            Param.Add("severity", severity);
            Param.Add("signssymptoms", signssymptoms);
            Param.Add("chronicstatus", chronicstatus);
            Param.Add("timing", timing);
            Param.Add("modifyingfactor", modifyingfactor);
            Param.Add("historyreason", historyreason);
            Param.Add("limitedhistory", limitedhistory);
            Param.Add("context", context);
            Param.Add("duration", duration);

            objDB.ExecuteNonQuery("WEMR_spI_SaveEMRSHPI", Param);

        }


        public void SaveEMRS()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            Param.Add("examinationtype", examinationtype);
            Param.Add("pagefrom ", "PatientDashboard");

            objDB.ExecuteNonQuery("WEMR_spI_SaveEMRS", Param);

        }


        public void EMRSDataAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            Param.Add("type", type);
            Param.Add("subtype ", subtype);

            Param.Add("selected", selected);
            Param.Add("comments", comments);
            Param.Add("number", number);
            Param.Add("points ", points);
            Param.Add("score", score);
            Param.Add("fieldname ", fieldname);

            objDB.ExecuteNonQuery("HMS_SP_EMRSDataAdd", Param);

        }



        public void SaveEMResult()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();


            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            Param.Add("filenumber", FileNumber);
            Param.Add("hpilevel", HPILevel);
            Param.Add("ros", ROS);
            Param.Add("pfsh", PFSH);
            Param.Add("documentedhistory", DocumentedHistory);
            Param.Add("examinationtype", examinationtype);
            Param.Add("problempointscored", ProblemPointScored);
            Param.Add("datapointscored", DataPointScored);
            Param.Add("riskmanagement", RiskManagement);
            Param.Add("mdm", MDM);
            Param.Add("cptcode", CPTCode);


            objDB.ExecuteNonQuery("WEMR_spI_SaveEMResult", Param);

        }


        public void WEMR_spI_SaveEMRSDatas(DataTable EMRSDatas)
        {
            conopen();
            SqlCommand cmd1 = new SqlCommand();
            cmd1.Connection = con;
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 864000;
            cmd1.CommandText = "WEMR_spI_SaveEMRSDatas";
            cmd1.Parameters.Add(new SqlParameter("@emrsdatatable", SqlDbType.Structured)).Value = EMRSDatas;
            cmd1.ExecuteNonQuery();
            conclose();

        }


        public DataSet EMRS_PT_DETAILSGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT * FROM   EMRS_PT_DETAILS WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }

        public DataSet WEMR_spS_GetEMCPTInvoice(string BranchID, string EMRID, string cptcode)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            Param.Add("cptcode", cptcode);
            DS = objDB.ExecuteReader("WEMR_spS_GetEMCPTInvoice", Param);



            return DS;

        }


        public void EMRSDataDelete(string Criteria)
        {

            string strSQL = "";

            strSQL = "DELETE FROM EMRS_DATA  WHERE " + Criteria;

            objDB.ExecuteNonQuery(strSQL);


        }
    }
}
