﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace EMR_IP_BAL
{
   public  class EMR_PTLaboratory
    {


        dbOperation objDB = new dbOperation();

        public string BRANCH_ID { set; get; }
        public string EPL_ID { set; get; }
        public string EPL_LAB_CODE { set; get; }
        public string EPL_LAB_NAME { set; get; }
        public string EPL_COST { set; get; }
        public string EPL_REMARKS { set; get; }
        public string EPL_QTY { set; get; }
        public string EPL_TEMPLATE_CODE { set; get; }



        public DataSet LaboratoryGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRPTLaboratoryGet", Param);

            return DS;


        }


        public void LaboratoryAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPL_BRANCH_ID", BRANCH_ID);
            Param.Add("EPL_ID", EPL_ID);
            Param.Add("EPL_LAB_CODE", EPL_LAB_CODE);
            Param.Add("EPL_LAB_NAME", EPL_LAB_NAME);
            Param.Add("EPL_COST", EPL_COST);
            Param.Add("EPL_REMARKS", EPL_REMARKS);
            Param.Add("EPL_QTY", EPL_QTY);
            Param.Add("EPL_TEMPLATE_CODE", EPL_TEMPLATE_CODE);

            objDB.ExecuteNonQuery("HMS_SP_EMR_LaboratoryAdd", Param);

        }




        public void LaboratoryDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPL_BRANCH_ID", BRANCH_ID);
            Param.Add("EPL_ID", EPL_ID);
            Param.Add("EPL_LAB_CODE", EPL_LAB_CODE);

            objDB.ExecuteNonQuery("HMS_SP_EMR_LaboratoryDelete", Param);

        }

    }
}
