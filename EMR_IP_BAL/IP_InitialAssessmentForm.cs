﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_InitialAssessmentForm
    {

        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }

        public string IASD_TYPE { set; get; }
        public string IASD_FIELD_ID { set; get; }
        public string IASD_FIELD_NAME { set; get; }
        public string IASD_VALUE { set; get; }
        public string IASD_COMMENT { set; get; }

        public string IPE_AREA { set; get; }
        public string IPE_ARRIVAL_TIME { set; get; }
        public string IPE_VISIT_DATE { set; get; }
        public string IPE_DIAGNOSIS { set; get; }
        public string IPE_CONDITION { set; get; }
        public string IPE_CONDITION_OTH { set; get; }
        public string IPE_INFOBTAINDFROM { set; get; }
        public string IPE_INFOBTAINDFROM_OTH { set; get; }
        public string IPE_WEIGHT { set; get; }
        public string IPE_HEIGHT { set; get; }
        public string IPE_BMI { set; get; }
        public string IPE_TEMPERATURE_F { set; get; }
        public string IPE_TEMPERATURE_C { set; get; }
        public string IPE_PULSE { set; get; }
        public string IPE_RESPIRATION { set; get; }
        public string IPE_BP_SYSTOLIC { set; get; }
        public string IPE_BP_DIASTOLIC { set; get; }
        public string IPE_HPO2 { set; get; }
        public string IPE_EXPLI_GIVEN { set; get; }
        public string IPE_EXPLI_GIVEN_OTH { set; get; }
        public string IPE_ASSESSMENT_DONE_CODE { set; get; }
        public string IPE_ASSESSMENT_DONE_NAME { set; get; }


        public string IPE_PRESENT_COMPLAINT { set; get; }
        public string IPE_PAST_MEDICAL_HISTORY { set; get; }
        public string IPE_PAST_SURGICAL_HISTORY { set; get; }
        public string IPE_PSY_SOC_ECONOMIC_HISTORY { set; get; }
        public string IPE_FAMILY_HISTORY { set; get; }
        public string IPE_PHYSICAL_ASSESSMENT { set; get; }
        public string IPE_REVIEW_OF_SYSTEMS { set; get; }
        public string IPE_INVESTIGATIONS { set; get; }
        public string IPE_PRINCIPAL_DIAGNOSIS { set; get; }
        public string IPE_SECONDARY_DIAGNOSIS { set; get; }
        public string IPE_TREATMENT_PLAN { set; get; }
        public string IPE_PROCEDURE { set; get; }
        public string IPE_TREATMENT { set; get; }
        public string IPE_FOLLOWUPNOTES { set; get; }






        public string IPA_HEALTH_PRESPASTHIST { set; get; }
        public string IPA_HEALTH_PRESPAS_PROB { set; get; }
        public string IPA_HEALTH_PRESPAS_PREVPROB { set; get; }
        public string IPA_HEALTH_ALLERGIES_MEDIC { set; get; }
        public string IPA_HEALTH_ALLERGIES_FOOD { set; get; }
        public string IPA_HEALTH_ALLERGIES_OTHER { set; get; }
        public string IPA_HEALTH_MEDICHIST_MEDICINE { set; get; }
        public string IPA_HEALTH_MEDICHIST_SLEEP { set; get; }
        public string IPA_HEALTH_MEDICHIST_PSCHOYASS { set; get; }
        public string IPA_HEALTH_PAIN_EXPRESSES { set; get; }
        public string IPA_PHY_GAST_BOWELSOUNDS { set; get; }
        public string IPA_PHY_GAST_ELIMINATION { set; get; }
        public string IPA_PHY_GAST_GENERAL { set; get; }
        public string IPA_PHY_REPRO_MALE { set; get; }
        public string IPA_PHY_REPRO_FEMALE { set; get; }
        public string IPA_PHY_REPRO_BREASTS { set; get; }
        public string IPA_PHY_GENIT_GENERAL { set; get; }
        public string IPA_PHY_SKIN_COLOR { set; get; }
        public string IPA_PHY_SKIN_TEMPERATURE { set; get; }
        public string IPA_PHY_SKIN_LESIONS { set; get; }
        public string IPA_PHY_NEURO_GENERAL { set; get; }
        public string IPA_PHY_NEURO_CONSCIO { set; get; }
        public string IPA_PHY_NEURO_ORIENTED { set; get; }
        public string IPA_PHY_NEURO_TIMERESP { set; get; }
        public string IPA_PHY_CARDIO_GENERAL { set; get; }
        public string IPA_PHY_CARDIO_PULSE { set; get; }
        public string IPA_PHY_CARDIO_PEDALPULSE { set; get; }
        public string IPA_PHY_CARDIO_EDEMA { set; get; }
        public string IPA_PHY_CARDIO_NAILBEDS { set; get; }
        public string IPA_PHY_CARDIO_CAPILLARY { set; get; }
        public string IPA_PHY_ENT_EYES { set; get; }
        public string IPA_PHY_ENT_EARS { set; get; }
        public string IPA_PHY_ENT_NOSE { set; get; }
        public string IPA_PHY_ENT_THROAT { set; get; }
        public string IPA_PHY_RESP_CHEST { set; get; }
        public string IPA_PHY_RESP_BREATHPATT { set; get; }
        public string IPA_PHY_RESP_BREATHSOUND { set; get; }
        public string IPA_RESP_BREATHCOUGH { set; get; }
        public string IPA_PHY_NUTRI_DIET { set; get; }
        public string IPA_PHY_NUTRI_APPETITE { set; get; }
        public string IPA_PHY_NUTRI_NUTRISUPPORT { set; get; }
        public string IPA_PHY_NUTRI_FEEDINGDIF { set; get; }
        public string IPA_PHY_NUTRI_WEIGHTSTATUS { set; get; }
        public string IPA_PHY_NUTRI_DIAG { set; get; }
        public string IPA_RISK_SKINRISK_SENSORY { set; get; }
        public string IPA_RISK_SKINRISK_MOISTURE { set; get; }
        public string IPA_RISK_SKINRISK_ACTIVITY { set; get; }
        public string IPA_RISK_SKINRISK_MOBILITY { set; get; }
        public string IPA_RISK_SKINRISK_NUTRITION { set; get; }
        public string IPA_RISK_SKINRISK_FRICTION { set; get; }
        public string IPA_RISK_SOCIO_LIVING { set; get; }
        public string IPA_RISK_SAFETY_GENERAL { set; get; }
        public string IPA_RISK_FUN_SELFCARING { set; get; }
        public string IPA_RISK_FUN_MUSCULOS { set; get; }
        public string IPA_RISK_FUN_EQUIPMENT { set; get; }
        public string IPA_RISK_EDU_GENERAL { set; get; }

     

        public string UserID { set; get; }

        public DataSet PTEncounterGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTEncounterGet", Param);

            return DS;


        }

        public void PTEncounterAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPE_BRANCH_ID", BranchID);
            Param.Add("IPE_ID", EMRID);
            Param.Add("IPE_PT_ID", PTID);

            Param.Add("IPE_AREA", IPE_AREA);
            Param.Add("IPE_ARRIVAL_TIME", IPE_ARRIVAL_TIME);
            Param.Add("IPE_VISIT_DATE", IPE_VISIT_DATE);
            Param.Add("IPE_DIAGNOSIS", IPE_DIAGNOSIS);

            Param.Add("IPE_CONDITION", IPE_CONDITION);
            Param.Add("IPE_CONDITION_OTH", IPE_CONDITION_OTH);
            Param.Add("IPE_INFOBTAINDFROM", IPE_INFOBTAINDFROM);
            Param.Add("IPE_INFOBTAINDFROM_OTH", IPE_INFOBTAINDFROM_OTH);

            Param.Add("IPE_EXPLI_GIVEN", IPE_EXPLI_GIVEN);
            Param.Add("IPE_EXPLI_GIVEN_OTH", IPE_EXPLI_GIVEN_OTH);
            Param.Add("IPE_ASSESSMENT_DONE_CODE", IPE_ASSESSMENT_DONE_CODE);
            Param.Add("IPE_ASSESSMENT_DONE_NAME", IPE_ASSESSMENT_DONE_NAME);


            Param.Add("IPE_PRESENT_COMPLAINT", IPE_PRESENT_COMPLAINT);
            Param.Add("IPE_PAST_MEDICAL_HISTORY", IPE_PAST_MEDICAL_HISTORY);
            Param.Add("IPE_PAST_SURGICAL_HISTORY", IPE_PAST_SURGICAL_HISTORY);
            Param.Add("IPE_PSY_SOC_ECONOMIC_HISTORY", IPE_PSY_SOC_ECONOMIC_HISTORY);

            Param.Add("IPE_FAMILY_HISTORY", IPE_FAMILY_HISTORY);
            Param.Add("IPE_PHYSICAL_ASSESSMENT", IPE_PHYSICAL_ASSESSMENT);
            Param.Add("IPE_REVIEW_OF_SYSTEMS", IPE_REVIEW_OF_SYSTEMS);
            Param.Add("IPE_INVESTIGATIONS", IPE_INVESTIGATIONS);


            Param.Add("IPE_PRINCIPAL_DIAGNOSIS", IPE_PRINCIPAL_DIAGNOSIS);
            Param.Add("IPE_SECONDARY_DIAGNOSIS", IPE_SECONDARY_DIAGNOSIS);
            Param.Add("IPE_TREATMENT_PLAN", IPE_TREATMENT_PLAN);
            Param.Add("IPE_PROCEDURE", IPE_PROCEDURE);

            Param.Add("IPE_TREATMENT", IPE_TREATMENT);
            Param.Add("IPE_FOLLOWUPNOTES", IPE_FOLLOWUPNOTES);

            Param.Add("UserID", UserID);


            objDB.ExecuteNonQuery("HMS_SP_IP_PTEncounterAdd", Param);

        }


        public DataSet PTAssessmentGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTAssessmentGet", Param);

            return DS;


        }


        public void PTAssessmentAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPA_BRANCH_ID", BranchID);
            Param.Add("IPA_ID", EMRID);
            Param.Add("IPA_PT_ID", PTID);

            Param.Add("IPA_HEALTH_PRESPASTHIST", IPA_HEALTH_PRESPASTHIST);
            Param.Add("IPA_HEALTH_PRESPAS_PROB", IPA_HEALTH_PRESPAS_PROB);
            Param.Add("IPA_HEALTH_PRESPAS_PREVPROB", IPA_HEALTH_PRESPAS_PREVPROB);
            Param.Add("IPA_HEALTH_ALLERGIES_MEDIC", IPA_HEALTH_ALLERGIES_MEDIC);

            Param.Add("IPA_HEALTH_ALLERGIES_FOOD", IPA_HEALTH_ALLERGIES_FOOD);
            Param.Add("IPA_HEALTH_ALLERGIES_OTHER", IPA_HEALTH_ALLERGIES_OTHER);
            Param.Add("IPA_HEALTH_MEDICHIST_MEDICINE", IPA_HEALTH_MEDICHIST_MEDICINE);
            Param.Add("IPA_HEALTH_MEDICHIST_SLEEP", IPA_HEALTH_MEDICHIST_SLEEP);


            Param.Add("IPA_HEALTH_MEDICHIST_PSCHOYASS", IPA_HEALTH_MEDICHIST_PSCHOYASS);
            Param.Add("IPA_HEALTH_PAIN_EXPRESSES", IPA_HEALTH_PAIN_EXPRESSES);
            Param.Add("IPA_PHY_GAST_BOWELSOUNDS", IPA_PHY_GAST_BOWELSOUNDS);
            Param.Add("IPA_PHY_GAST_ELIMINATION", IPA_PHY_GAST_ELIMINATION);

            Param.Add("IPA_PHY_GAST_GENERAL", IPA_PHY_GAST_GENERAL);
            Param.Add("IPA_PHY_REPRO_MALE", IPA_PHY_REPRO_MALE);
            Param.Add("IPA_PHY_REPRO_FEMALE", IPA_PHY_REPRO_FEMALE);
            Param.Add("IPA_PHY_REPRO_BREASTS", IPA_PHY_REPRO_BREASTS);


            Param.Add("IPA_PHY_GENIT_GENERAL", IPA_PHY_GENIT_GENERAL);
            Param.Add("IPA_PHY_SKIN_COLOR", IPA_PHY_SKIN_COLOR);
            Param.Add("IPA_PHY_SKIN_TEMPERATURE", IPA_PHY_SKIN_TEMPERATURE);
            Param.Add("IPA_PHY_SKIN_LESIONS", IPA_PHY_SKIN_LESIONS);


            Param.Add("IPA_PHY_NEURO_GENERAL", IPA_PHY_NEURO_GENERAL);
            Param.Add("IPA_PHY_NEURO_CONSCIO", IPA_PHY_NEURO_CONSCIO);
            Param.Add("IPA_PHY_NEURO_ORIENTED", IPA_PHY_NEURO_ORIENTED);
            Param.Add("IPA_PHY_NEURO_TIMERESP", IPA_PHY_NEURO_TIMERESP);



            Param.Add("IPA_PHY_CARDIO_GENERAL", IPA_PHY_CARDIO_GENERAL);
            Param.Add("IPA_PHY_CARDIO_PULSE", IPA_PHY_CARDIO_PULSE);
            Param.Add("IPA_PHY_CARDIO_PEDALPULSE", IPA_PHY_CARDIO_PEDALPULSE);
            Param.Add("IPA_PHY_CARDIO_EDEMA", IPA_PHY_CARDIO_EDEMA);


            Param.Add("IPA_PHY_CARDIO_NAILBEDS", IPA_PHY_CARDIO_NAILBEDS);
            Param.Add("IPA_PHY_CARDIO_CAPILLARY", IPA_PHY_CARDIO_CAPILLARY);
            Param.Add("IPA_PHY_ENT_EYES", IPA_PHY_ENT_EYES);
            Param.Add("IPA_PHY_ENT_EARS", IPA_PHY_ENT_EARS);

            Param.Add("IPA_PHY_ENT_NOSE", IPA_PHY_ENT_NOSE);
            Param.Add("IPA_PHY_ENT_THROAT", IPA_PHY_ENT_THROAT);
            Param.Add("IPA_PHY_RESP_CHEST", IPA_PHY_RESP_CHEST);
            Param.Add("IPA_PHY_RESP_BREATHPATT", IPA_PHY_RESP_BREATHPATT);



            Param.Add("IPA_PHY_RESP_BREATHSOUND", IPA_PHY_RESP_BREATHSOUND);
            Param.Add("IPA_RESP_BREATHCOUGH", IPA_RESP_BREATHCOUGH);
            Param.Add("IPA_PHY_NUTRI_DIET", IPA_PHY_NUTRI_DIET);
            Param.Add("IPA_PHY_NUTRI_NUTRISUPPORT", IPA_PHY_NUTRI_NUTRISUPPORT);

            Param.Add("IPA_PHY_NUTRI_APPETITE", IPA_PHY_NUTRI_APPETITE);
            Param.Add("IPA_PHY_NUTRI_FEEDINGDIF", IPA_PHY_NUTRI_FEEDINGDIF);
            Param.Add("IPA_PHY_NUTRI_WEIGHTSTATUS", IPA_PHY_NUTRI_WEIGHTSTATUS);
            Param.Add("IPA_PHY_NUTRI_DIAG", IPA_PHY_NUTRI_DIAG);

            Param.Add("IPA_RISK_SKINRISK_SENSORY", IPA_RISK_SKINRISK_SENSORY);
            Param.Add("IPA_RISK_SKINRISK_MOISTURE", IPA_RISK_SKINRISK_MOISTURE);
            Param.Add("IPA_RISK_SKINRISK_ACTIVITY", IPA_RISK_SKINRISK_ACTIVITY);
            Param.Add("IPA_RISK_SKINRISK_MOBILITY", IPA_RISK_SKINRISK_MOBILITY);



            Param.Add("IPA_RISK_SKINRISK_NUTRITION", IPA_RISK_SKINRISK_NUTRITION);
            Param.Add("IPA_RISK_SKINRISK_FRICTION", IPA_RISK_SKINRISK_FRICTION);
            Param.Add("IPA_RISK_SOCIO_LIVING", IPA_RISK_SOCIO_LIVING);
            Param.Add("IPA_RISK_SAFETY_GENERAL", IPA_RISK_SAFETY_GENERAL);

            Param.Add("IPA_RISK_FUN_SELFCARING", IPA_RISK_FUN_SELFCARING);
            Param.Add("IPA_RISK_FUN_MUSCULOS", IPA_RISK_FUN_MUSCULOS);
            Param.Add("IPA_RISK_FUN_EQUIPMENT", IPA_RISK_FUN_EQUIPMENT);
            Param.Add("IPA_RISK_EDU_GENERAL", IPA_RISK_EDU_GENERAL);

            Param.Add("UserID", UserID);


            objDB.ExecuteNonQuery("HMS_SP_IP_PTAssessmentAdd", Param);

        }


        public DataSet  PTAssessmentSegmentGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTAssessmentSegmentGet", Param);

            return DS;


        }

        public DataSet PTAssessmentSegmentMasterGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTAssessmentSegmentMasterGet", Param);

            return DS;


        }

        public DataSet PTAssessmentSegmentDtlsGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTAssessmentSegmentDtlsGet", Param);

            return DS;


        }


        public void PTAssessmentSegmentDtlsAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IASD_BRANCH_ID", BranchID);
            Param.Add("IASD_ID", EMRID);
            Param.Add("IASD_PT_ID", PTID);
            Param.Add("IASD_TYPE", IASD_TYPE);
            Param.Add("IASD_FIELD_ID", IASD_FIELD_ID);
            Param.Add("IASD_FIELD_NAME", IASD_FIELD_NAME);
            Param.Add("IASD_VALUE", IASD_VALUE);
            Param.Add("IASD_COMMENT", IASD_COMMENT);

            objDB.ExecuteNonQuery("HMS_SP_IP_PTAssessmentSegmentDtlsAdd", Param);

        }

    }
}
