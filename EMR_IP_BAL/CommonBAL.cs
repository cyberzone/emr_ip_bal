﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;


using System.Net.Mail;

namespace EMR_IP_BAL
{
    public class CommonBAL
    {

        dbOperation objDB = new dbOperation();

        public string EMRID { set; get; }
        public string BranchID { set; get; }
        public string Code { set; get; }
        public string Description { set; get; }
        public string DR_ID { set; get; }
        public string DEP_ID { set; get; }
        public string Type { set; get; }
        public string Price { set; get; }
        public string TemplateData { set; get; }
        public string AllDr { set; get; }
        public string Comments { set; get; }
        public string EPC_DATE { set; get; }
        public string EPC_INSTRUCTION_ID { set; get; }
        public string NextDays { set; get; }
        public string EPC_TIME { set; get; }

        public string EPH_CC { set; get; }



        public string UserID { set; get; }


        public string TotalRoom { set; get; }
        public string Remarks { set; get; }
        public string Status { set; get; }

        
        public string WardID { set; get; }
        public string MultyBed { set; get; }
        public string Rent { set; get; }

        public string RoomID { set; get; }


        public DataSet ScreenCustomizationGet(string Criteria)
        {


            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_ScreenCustomizationGet", Param);
            return DS;
        }

        //public static string ConvertToTxt(string data)
        //{
        //    string result = data;
        //    try
        //    {
        //        using (System.Windows.Forms.RichTextBox rtfTemp = new System.Windows.Forms.RichTextBox())
        //        {
        //            rtfTemp.Rtf = data;
        //            result = rtfTemp.Text;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result = data;
        //    }


        //    return result;
        //}

        public DataSet BranchMasterGet()
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();

            DS = objDB.ExecuteReader("HMS_SP_BranchMasterGet", Param);
            return DS;
        }

        public DataSet DepMasterGet(string Criteria)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_DepMasterGet", Param);
            return DS;
        }

        public DataSet UserMasterGet(string Criteria)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_UserMasterGet", Param);
            return DS;

        }

        public DataSet SystemOptionGet(string Criteria)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_SystemOptionGet", Param);

            return DS;

        }

        public DataSet PatientMasterGet(string Criteria)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_PatientMasterGet", Param);
            return DS;

        }

        public DataSet PatientPhotoGet(string Criteria)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_PatientPhotoGet", Param);
            return DS;

        }

        public DataSet GetStaffMaster(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_StaffMasterGet", Param);

            return DS;


        }

        public DataSet StaffPhotoGet(string Criteria)
        {


            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_StaffPhotoGet", Param);

            return DS;
        }

        public DataSet RollTransGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_RollTransGet", Param);

            return DS;


        }

        public DataSet RefDoctorMasterGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_DoctorMasterGet", Param);

            return DS;


        }

        public DataSet PatientVisitGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            // DS = objDB.ExecuteReader("HMS_SP_PatientVisitGet", Param);
            DS = objDB.ExecuteReader("HMS_SP_EMRPatientVisitGet", Param);
            return DS;


        }


        public DataSet Patient_Master_VisitGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_Patient_Master_VisitGet", Param);

            return DS;


        }

        public DataSet ScanCardImageGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_ScanCardImageGet", Param);

            return DS;


        }

        public DataSet HaadServicessListGet(string ServiceType, string SearchFilter, string SearchFilter1, string department)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("servicetype", ServiceType);

            if (SearchFilter != "")
                Param.Add("searchfilter", SearchFilter);

            if (SearchFilter1 != "")
                Param.Add("searchfilter1", SearchFilter1);

            Param.Add("department", department);


            DS = objDB.ExecuteReader("HMS_SP_GetHAASServices", Param);

            return DS;


        }

        public DataSet FavoritesGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRFavoritesGet", Param);

            return DS;
        }

        public string InitialNoGet(string BranchId, string ScreenId)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BranchId", BranchId);
            Param.Add("ScreenId", ScreenId);
            DS = objDB.ExecuteReader("HMS_SP_InitialNoGet", Param);

            string fileNo = "0";
            if (DS.Tables[0].Rows.Count > 0)
            {

                fileNo = DS.Tables[0].Rows[0]["FileNo"].ToString();
            }

            return fileNo;


        }

        public DataSet AppointmentOutlookGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_AppointmentOutlookGet", Param);

            return DS;


        }



        public DataSet AppointmentStatusGet(string Criteria)
        {

            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "SELECT * FROM HMS_APPOINTMENTSTATUS where " + Criteria + "  ORDER BY HAS_ORDERNUMBER ASC";

            DS = objDB.ExecuteReader(strSQL);

            return DS;



        }

        public DataSet AppointmentServicesGet()
        {

            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "SELECT * FROM HMS_APPOINTMENTSERVICES ORDER BY HAS_ORDERNUMBER ASC";

            DS = objDB.ExecuteReader(strSQL);

            return DS;


        }

        public DataSet GetCompanyMaster(string Criteria)
        {


            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_CompanyGet", Param);
            return DS;



        }

        public DataSet AgrementServiceGet(string Criteria)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();


            Param.Add("Criteria", Criteria);

            DS = objDB.ExecuteReader("HMS_SP_AgrementServiceGet", Param);



            return DS;

        }

        public DataSet AgrementServiceGetTop1(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "Select Top 1 * from   HMS_AGRMT_SERVICE WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }

        public DataSet CompBenefitsGet(string Criteria)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();


            Param.Add("Criteria", Criteria);

            DS = objDB.ExecuteReader("HMS_SP_CompBenefitsGet", Param);



            return DS;

        }


        public DataSet ServiceMasterGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_ServiceMasterGet", Param);

            return DS;
        }

        public DataSet HMS_SP_ServiceMasterTopGet(string Criteria, string strTop)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();


            Param.Add("Criteria", Criteria);
            if (strTop != "")
            {

                Param.Add("Top", strTop);
            }
            else
            {
                Param.Add("Top", "");
            }

            DS = objDB.ExecuteReader("HMS_SP_ServiceMasterTopGet", Param);



            return DS;

        }


        public DataSet HaadServiceGet(string Criteria, string CategoryType, string strTop)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();


            Param.Add("Criteria", Criteria);
            if (CategoryType != "")
            {

                Param.Add("CategoryType", CategoryType);
            }
            else
            {
                Param.Add("CategoryType", "");
            }

            if (strTop != "")
            {

                Param.Add("Top", strTop);
            }
            else
            {
                Param.Add("Top", "");
            }

            DS = objDB.ExecuteReader("HMS_SP_HaadServiceGet", Param);



            return DS;

        }


        public DataSet DrICDMasterGet(string Criteria, string strTop)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();


            Param.Add("Criteria", Criteria);
            if (strTop != "")
            {

                Param.Add("Top", strTop);
            }
            else
            {
                Param.Add("Top", "");
            }

            DS = objDB.ExecuteReader("HMS_SP_DrICDMasterGet", Param);



            return DS;

        }

        public DataSet ServiceCategoryGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_ServiceCategoryGet", Param);

            return DS;
        }



        public void FavoritesAdd()
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("EDF_BRANCH_ID", BranchID);
            Param.Add("EDF_CODE", Code);
            Param.Add("EDF_NAME", Description);
            Param.Add("EDF_DR_ID", DR_ID);
            Param.Add("EDF_DEP_ID", DEP_ID);
            Param.Add("EDF_TYPE", Type);
            Param.Add("EDF_PRICE", Price);

            objDB.ExecuteNonQuery("HMS_SP_EMRFavoritesAdd", Param);


        }

        public void FavoritesDelete()
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("EDF_BRANCH_ID", BranchID);
            Param.Add("EDF_CODE", Code);
            Param.Add("EDF_DR_ID", DR_ID);
            Param.Add("EDF_DEP_ID", DEP_ID);
            Param.Add("EDF_TYPE", Type);

            objDB.ExecuteNonQuery("HMS_SP_EMRFavoritesDelete", Param);


        }

        public DataSet TemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRTemplatesGet", Param);

            return DS;
        }



        public string TemplatesAdd()
        {
            string TemplateCode = "";

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("ET_BRANCH_ID", BranchID);
            Param.Add("ET_TYPE", Type);
            Param.Add("ET_NAME", Description);
            Param.Add("ET_TEMPLATE", TemplateData);
            Param.Add("ET_APPLY ", AllDr);
            Param.Add("ET_DR_CODE", DR_ID);
            Param.Add("ET_DEP_ID ", DEP_ID);

            TemplateCode = objDB.ExecuteNonQueryReturn("HMS_SP_EMR_TemplatesAdd", Param);

            return TemplateCode;
        }


        public DataSet GetEMRMasters()
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("mastertype", Type);
            Param.Add("patientmasterid ", Code);
            DS = objDB.ExecuteReader("HMS_SP_GetEMRMasters", Param);

            return DS;
        }

        public DataSet EMRTimeGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT * FROM  EMR_PHY_TIME WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }

        public DataSet EMRTakenGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT * FROM  EMR_PHY_TAKEN WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }

        public DataSet EMRRouteGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT * FROM  EMR_PHY_ROUTE WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }


        public DataSet NursingOrderGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT *,convert(varchar,EPC_DATE,103)as EPC_DATEDesc,(SELECT top 1 HUM_USER_NAME FROM HMS_USER_MASTER WHERE HUM_USER_ID = EPC_USERNAME) AS UserName FROM  EMR_PT_INSTRUCTION WHERE " + Criteria + " order by EPC_DATE Desc";

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }


        public DataSet ScanCategoryGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "Select ESC_NAME,ESC_CODE from  EMEDICAL.DBO.EMR_SCAN_CATEGORY WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }

        public DataSet ScanFilesGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "SELECT CONVERT(VARCHAR,ESF_CREATED_DATE,103) AS ESF_CREATED_DATEDesc, * FROM  EMEDICAL.DBO.EMR_SCAN_FILE WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }
        public string NursingOrderAdd()
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPC_BRANCH_ID", BranchID);
            Param.Add("EPC_ID", EMRID);
            Param.Add("EPC_INSTRUCTION_ID", EPC_INSTRUCTION_ID);
            Param.Add("EPC_INSTTRUCTION", Description);
            Param.Add("EPC_TEMPLATE_CODE", "");
            Param.Add("EPR_TEMPLATE_CODE", "0");
            Param.Add("EPC_DATE", EPC_DATE);
            Param.Add("NextDays", NextDays);

            objDB.ExecuteNonQuery("HMS_SP_EMR_NursingOrderAdd", Param);

            return "";
        }

        public string NursingOrderUpdate()
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPC_BRANCH_ID", BranchID);
            Param.Add("EPC_ID", EMRID);
            Param.Add("EPC_INSTRUCTION_ID", EPC_INSTRUCTION_ID);
            Param.Add("EPC_COMMENT", Comments);
            Param.Add("EPC_USERNAME", UserID);
            Param.Add("EPC_TIME", EPC_TIME);
            objDB.ExecuteNonQuery("HMS_SP_EMR_NursingOrderUpdate", Param);

            return "";
        }



        public DataSet GetMenus()
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("menutype", Type);
            Param.Add("departmentid ", DEP_ID);
            DS = objDB.ExecuteReader("WEMR_spS_GetMenus", Param);

            return DS;
        }



        public DataSet LaboratoryResultGet(string BranchID, string FileNo)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("filenumber", FileNo);
            DS = objDB.ExecuteReader("WEMR_spS_GetLaboratoryResult", Param);



            return DS;

        }

        public DataSet GetLaboratoryResultGroupBy(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_GetLaboratoryResultGroupBy", Param);



            return DS;

        }

        //public DataSet RadiologyResultGet(string BranchID, string FileNo)
        //{
        //    DataSet DS = new DataSet();

        //    IDictionary<string, string> Param = new Dictionary<string, string>();
        //    Param.Add("branchid", BranchID);
        //    Param.Add("filenumber", FileNo);
        //    DS = objDB.ExecuteReader("WEMR_spS_GetRadiologyResult", Param);



        //    return DS;

        //}

        public DataSet RadiologyResultGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT top 50 *,convert(varchar,RTR_DATE,103)as RTR_DATEDesc FROM   RAD_TEST_REPORT WHERE " + Criteria + " ORDER BY RTR_DATE DESC ";

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }


        public DataSet PatientHistoryGet(string Criteria)
        {
            DataSet DS = new DataSet();

            dbOperation DBO = new dbOperation();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = DBO.ExecuteReader("HMS_SP_PatientHistoryGet", Param);
            return (DS);


        }

        public DataSet EMR_PTHistoryAdd()
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPH_BRANCH_ID", BranchID);
            Param.Add("EPH_ID", EMRID);
            Param.Add("EPH_CC", EPH_CC);
            objDB.ExecuteReader("HMS_SP_EMR_PTHistoryAdd", Param);



            return DS;

        }

        public DataSet EMR_DENTAL_TRANSGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " select (isnull( DDMR_TRNT_ID,'')+'-'+ isnull( DDMR_TRNT_DESCRIPTION,'')) as Treatment,(isnull( DDMR_TRNT_ICD,'')+'-'+ isnull( DDMR_TRNT_ICD_DESCRIPTION,'')) as ICDCodeDesc  , * from EMR_DENTAL_TRANS  WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }
        public DataSet EMR_DENTAL_ADLGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT * FROM   EMR_DENTAL_ADL WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }



        public DataSet EMR_EDUCATIONALFORMGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT * FROM   EMR_EDUCATIONALFORM WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }

        public DataSet WEMR_spS_GetEMRSHPI(string BranchID, string EMRID)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetEMRSHPI", Param);



            return DS;

        }

        public DataSet WEMR_spS_GetEMRSDatas(string BranchID, string EMRID)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetEMRSDatas", Param);



            return DS;

        }



        public DataSet EMR_DYNAMIC_CONTENT(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT * FROM   EMR_DYNAMIC_CONTENT WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }


        public DataSet fnGetFieldValue(string strSQL)
        {
            DataSet DS = new DataSet();

            DS = objDB.ExecuteReader(strSQL);

            return DS;
        }

        public DataSet fnGetFieldValue(string FieldNames, string TableName, string Criteria, string OrderBy)
        {
            DataSet DS = new DataSet();
            string strSQL;

            if (OrderBy != "")
            {
                strSQL = " SELECT " + FieldNames + "   FROM " + TableName + " WHERE " + Criteria + " ORDER BY " + OrderBy;
            }
            else
            {
                strSQL = " SELECT " + FieldNames + "   FROM " + TableName + " WHERE " + Criteria;
            }


            DS = objDB.ExecuteReader(strSQL);

            return DS;
        }

        public void fnInsertTableData(string FieldName, string Values, string TableName)
        {

            string strSQL;

            strSQL = " INSERT INTO " + TableName + "(" + FieldName + " ) VALUES( " + Values + ")";


            objDB.ExecuteNonQuery(strSQL);


        }


        public void fnUpdateTableData(string FieldNameWithValues, string TableName, string Criteria)
        {

            string strSQL;

            strSQL = " UPDATE   " + TableName + " SET " + FieldNameWithValues + " WHERE " + Criteria;


            objDB.ExecuteNonQuery(strSQL);


        }


        public void fnDeleteTableData(string TableName, string Criteria)
        {

            string strSQL;

            strSQL = " DELETE  FROM " + TableName + " WHERE " + Criteria;


            objDB.ExecuteNonQuery(strSQL);


        }

        public string fnGetDate(String formatstring)
        {
            DateTime dtDate;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT getdate() ";

            DS = objDB.ExecuteReader(strSQL);

            dtDate = Convert.ToDateTime(DS.Tables[0].Rows[0][0]);


            return Convert.ToString(dtDate.ToString(formatstring));
        }

        public Int32 fnGetDateDiff(String Date1, string Date2)
        {
            Int32 intDateDiff;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT dbo.GetDateDiff ('" + Date1 + "','" + Date2 + "')";

            DS = objDB.ExecuteReader(strSQL);

            intDateDiff = Convert.ToInt32(DS.Tables[0].Rows[0][0]);


            return intDateDiff;
        }

        public string fnGetDateddMMyyyy()
        {
            string strDate;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " select convert(varchar,getdate(),103) ";

            DS = objDB.ExecuteReader(strSQL);

            strDate = Convert.ToString(DS.Tables[0].Rows[0][0]);


            return strDate;
        }


        public Boolean fnCheckduplicate(string TableName, string Criteria)
        {
            DataSet DS = new DataSet();

            string strSQL;

            strSQL = " SELECT *  FROM " + TableName + " WHERE " + Criteria;
            DS = objDB.ExecuteReader(strSQL);

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }


        public void AudittrailAdd(string BranchId, string ScreenId, string Type, string Remarks, string UserID)
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HAT_BRANCH_ID", BranchId);
            Param.Add("HAT_SCRN_ID", ScreenId);
            Param.Add("HAT_TYPE", Type);
            Param.Add("HAT_REMARK", Remarks);
            Param.Add("HAT_CREATED_USER", UserID);

            objDB.ExecuteNonQuery("HMS_SP_AudittrailAdd", Param);

        }

        public int MailSend(string strToAddress, string strSubject, string strBody)
        {
            //create the mail message
            MailMessage mail = new MailMessage();
            string strFromEmail = GlobalValues.Hospital_FromMailID;
            string strMail_Server = GlobalValues.Hospital_MailServer;
            string Hospital_MailUesrName = GlobalValues.Hospital_MailUesrName;
            string Hospital_MailPassword = GlobalValues.Hospital_MailPassword;

            //set the addresses
            mail.From = new MailAddress(strFromEmail);
            mail.To.Add(strToAddress);

            //set the content
            mail.Subject = strSubject;
            mail.Body = strBody;
            mail.IsBodyHtml = true;
            //send the message
            SmtpClient smtp = new SmtpClient(strMail_Server);

            if (Hospital_MailUesrName != "")
            {
                //to authenticate we set the username and password properites on the SmtpClient
                smtp.Credentials = new System.Net.NetworkCredential(Hospital_MailUesrName, Hospital_MailPassword);
            }

            smtp.Send(mail);




            return 0;
        }



        public DataSet IPPatientVisitGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            // DS = objDB.ExecuteReader("HMS_SP_PatientVisitGet", Param);
            DS = objDB.ExecuteReader("HMS_SP_IPPatientVisitGet", Param);
            return DS;


        }

        public static string ConvertToJson(IDictionary<string, string> Param)
        {
            String strJsonValue = "{";

            foreach (KeyValuePair<string, string> _entry in Param)
            {

                strJsonValue += "\"" + _entry.Key + "\":" + "\"" + _entry.Value + "\",";
            }
            strJsonValue = strJsonValue.Substring(0, strJsonValue.Length - 1);
            strJsonValue += "}";



            return strJsonValue;
        }


        public static string ConvertToTxt(string data)
        {
            string result = data;
            try
            {
                using (System.Windows.Forms.RichTextBox rtfTemp = new System.Windows.Forms.RichTextBox())
                {
                    rtfTemp.Rtf = data;
                    result = rtfTemp.Text;
                }
            }
            catch (Exception ex)
            {
                result = data;
            }


            return result;
        }


        public DataSet EMR_MasterGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            // DS = objDB.ExecuteReader("HMS_SP_PatientVisitGet", Param);
            DS = objDB.ExecuteReader("HMS_SP_EMR_MasterGet", Param);
            return DS;


        }

        public void  WardMasterAdd()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("BranchID", BranchID);
            Param.Add("Code", Code);
            Param.Add("Name", Description);
            Param.Add("TotalRoom", TotalRoom);
            Param.Add("Remarks", Remarks);
            Param.Add("Status", Status);

            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_WardMasterAdd", Param);

        }

        public void RoomMasterAdd()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("BranchID", BranchID);
            Param.Add("WardID", WardID);
            Param.Add("Code", Code);
            Param.Add("Name", Description);
            Param.Add("MultyBed", MultyBed);
            Param.Add("Rent", Rent);
            Param.Add("Remarks", Remarks);
            Param.Add("Status", Status);

            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_RoomMasterAdd", Param);

        }


        public void BedMasterAdd()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("BranchID", BranchID);
            Param.Add("WardID", WardID);
            Param.Add("RoomID", RoomID);
            Param.Add("Code", Code);
            Param.Add("Rent", Rent);
            Param.Add("Remarks", Remarks);
            Param.Add("Status", Status);

            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_BedMasterAdd", Param);

        }


        public DataSet  HoursGet()
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            DS = objDB.ExecuteReader("HMS_SP_HoursGet", Param);
            return DS;
        }

        public DataSet MinutesGet(string Interval)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Interval", Interval);
            DS = objDB.ExecuteReader("HMS_SP_MinutesGet", Param);
            return DS;
        }


        static DataTable CharEncryptData;
        static DataTable UpperCharEncryptData;
        static DataTable DigitEncryptData;
        static DataTable SplCharEncryptData;

        void CharEncrypt()
        {
            // a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z
            // m	p	a	y	n	q	k	o	b	l	j	r	c	t	i	s	u	d	v	w	e	z	x	g	f	h

            DataTable DT = new DataTable();

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            DT.Columns.Add(Code);
            DT.Columns.Add(Name);

            DataRow objrow;

            objrow = DT.NewRow();
            objrow["Code"] = "a";
            objrow["Name"] = "m";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "b";
            objrow["Name"] = "p";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "c";
            objrow["Name"] = "a";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "d";
            objrow["Name"] = "y";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "e";
            objrow["Name"] = "n";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "f";
            objrow["Name"] = "q";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "g";
            objrow["Name"] = "k";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "h";
            objrow["Name"] = "o";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "i";
            objrow["Name"] = "b";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "j";
            objrow["Name"] = "l";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "k";
            objrow["Name"] = "j";
            DT.Rows.Add(objrow);



            objrow = DT.NewRow();
            objrow["Code"] = "l";
            objrow["Name"] = "r";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "m";
            objrow["Name"] = "c";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "n";
            objrow["Name"] = "t";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "o";
            objrow["Name"] = "i";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "p";
            objrow["Name"] = "s";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "q";
            objrow["Name"] = "u";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "r";
            objrow["Name"] = "d";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "s";
            objrow["Name"] = "v";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "t";
            objrow["Name"] = "w";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "u";
            objrow["Name"] = "e";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "v";
            objrow["Name"] = "z";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "w";
            objrow["Name"] = "x";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "x";
            objrow["Name"] = "g";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "y";
            objrow["Name"] = "f";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "z";
            objrow["Name"] = "h";
            DT.Rows.Add(objrow);



            CharEncryptData = (DataTable)DT;

        }

        void UpperCharEncrypt()
        {
            // A	B	C	D	E	F	G	H	I	J	K	L	M	N	O	P	Q	R	S	T	U	V	W	X	Y	Z
            // O	B	L	J	R	C	T	I	S	U	D	V	W	E	Z	X	G	F	H	M	P	A	Y	N	Q	K


            DataTable DT = new DataTable();

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            DT.Columns.Add(Code);
            DT.Columns.Add(Name);

            DataRow objrow;

            objrow = DT.NewRow();
            objrow["Code"] = "A";
            objrow["Name"] = "O";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "B";
            objrow["Name"] = "B";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "C";
            objrow["Name"] = "L";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "D";
            objrow["Name"] = "J";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "E";
            objrow["Name"] = "R";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "F";
            objrow["Name"] = "C";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "G";
            objrow["Name"] = "T";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "H";
            objrow["Name"] = "I";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "I";
            objrow["Name"] = "S";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "J";
            objrow["Name"] = "U";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "K";
            objrow["Name"] = "D";
            DT.Rows.Add(objrow);



            objrow = DT.NewRow();
            objrow["Code"] = "L";
            objrow["Name"] = "V";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "M";
            objrow["Name"] = "W";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "N";
            objrow["Name"] = "E";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "O";
            objrow["Name"] = "Z";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "P";
            objrow["Name"] = "X";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "Q";
            objrow["Name"] = "G";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "R";
            objrow["Name"] = "F";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "S";
            objrow["Name"] = "H";
            DT.Rows.Add(objrow);



            objrow = DT.NewRow();
            objrow["Code"] = "T";
            objrow["Name"] = "M";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "U";
            objrow["Name"] = "P";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "V";
            objrow["Name"] = "A";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "W";
            objrow["Name"] = "Y";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "X";
            objrow["Name"] = "N";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "Y";
            objrow["Name"] = "Q";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "Z";
            objrow["Name"] = "K";
            DT.Rows.Add(objrow);



            UpperCharEncryptData = (DataTable)DT;

        }

        void DigitEncrypt()
        {
            //0	1	2	3	4	5	6	7	8	9
            //6	4	7	5	0	8	2	3	9	1



            DataTable DT = new DataTable();

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            DT.Columns.Add(Code);
            DT.Columns.Add(Name);

            DataRow objrow;

            objrow = DT.NewRow();
            objrow["Code"] = "0";
            objrow["Name"] = "6";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "1";
            objrow["Name"] = "4";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "2";
            objrow["Name"] = "7";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "3";
            objrow["Name"] = "5";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "4";
            objrow["Name"] = "0";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "5";
            objrow["Name"] = "8";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "6";
            objrow["Name"] = "2";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "7";
            objrow["Name"] = "3";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "8";
            objrow["Name"] = "9";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "9";
            objrow["Name"] = "1";
            DT.Rows.Add(objrow);


            DigitEncryptData = (DataTable)DT;

        }

        void SplCharEncrypt()
        {
            //  !	@	#	$	%	^	&	*	(	)	_	-	+	.	,	\
            //  %	^	_	-	!	@	(	)	#	$	&	*	,	\	+	.




            DataTable DT = new DataTable();

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            DT.Columns.Add(Code);
            DT.Columns.Add(Name);

            DataRow objrow;

            objrow = DT.NewRow();
            objrow["Code"] = "!";
            objrow["Name"] = "%";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "@";
            objrow["Name"] = "^";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "#";
            objrow["Name"] = "_";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "$";
            objrow["Name"] = "-";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "%";
            objrow["Name"] = "!";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "^";
            objrow["Name"] = "@";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "&";
            objrow["Name"] = "(";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "*";
            objrow["Name"] = ")";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "(";
            objrow["Name"] = "#";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = ")";
            objrow["Name"] = "$";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "_";
            objrow["Name"] = "&";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "-";
            objrow["Name"] = "*";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "+";
            objrow["Name"] = ",";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = ".";
            objrow["Name"] = @"\";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = ",";
            objrow["Name"] = "+";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = @"\";
            objrow["Name"] = ".";
            DT.Rows.Add(objrow);




            SplCharEncryptData = (DataTable)DT;

        }

        bool CheckSplChar(string strData)
        {
            string strSplValue = @"!@#$%^&*()_-+.,\";

            if (strSplValue.IndexOf(strData) != -1)
            {
                return true;
            }

            return false;
        }

        public string SimpleEncrypt(string strData)
        {
            CharEncrypt();
            UpperCharEncrypt();
            DigitEncrypt();
            SplCharEncrypt();

            int key = 5;
            int v;
            Int64 c1;
            string z = "";
            string strValue = "";

            for (v = 0; v < strData.Length; v++)
            {
                strValue = strData.Substring(v, 1);

                DataTable DT;
                DataRow[] TempRow = null;
                if (strValue.Any(char.IsLower) == true)
                {
                    DT = (DataTable)CharEncryptData;
                    TempRow = DT.Select("Code='" + strValue + "'");
                    byte[] charByte = Encoding.ASCII.GetBytes(Convert.ToString(TempRow[0]["Name"]));
                    c1 = charByte[0];
                }
                else if (strValue.Any(char.IsUpper) == true)
                {
                    DT = (DataTable)UpperCharEncryptData;
                    TempRow = DT.Select("Code='" + strValue + "'");
                    byte[] charByte = Encoding.ASCII.GetBytes(Convert.ToString(TempRow[0]["Name"]));
                    c1 = charByte[0];
                }
                else if (strValue.Any(char.IsDigit) == true)
                {
                    DT = (DataTable)DigitEncryptData;
                    TempRow = DT.Select("Code='" + strValue + "'");
                    byte[] charByte = Encoding.ASCII.GetBytes(Convert.ToString(TempRow[0]["Name"]));
                    c1 = charByte[0];
                }
                else if (CheckSplChar(strValue) == true)
                {
                    DT = (DataTable)SplCharEncryptData;
                    TempRow = DT.Select("Code='" + strValue + "'");
                    byte[] charByte = Encoding.ASCII.GetBytes(Convert.ToString(TempRow[0]["Name"]));
                    c1 = charByte[0];
                }
                else
                {
                    byte[] charByte = Encoding.ASCII.GetBytes(strValue);
                    c1 = charByte[0];
                }


                char character;


                if (strValue == "{")
                {
                    z += "€";
                }
                else if (strValue == "}")
                {
                    z += ",";
                }
                else
                {
                    character = Convert.ToChar(c1 + key);
                    z += character.ToString();
                }


                // c1 = Asc(Mid(Name, v, 1))
                //c1 = Chr(c1 + Key)
                /// z = z & c1
                /// 
            }
            return z;
        }

        public string SimpleDecrypt(string strData)
        {
            CharEncrypt();
            UpperCharEncrypt();
            DigitEncrypt();
            SplCharEncrypt();

            int key = 5;
            int v;
            Int64 c1;
            string z = "";
            string strValue = "";
            for (v = 0; v < strData.Length; v++)
            {
                strValue = strData.Substring(v, 1);
                byte[] charByte = Encoding.ASCII.GetBytes(strData.Substring(v, 1));
                c1 = charByte[0];

                char character;
                if (strValue == "€")
                {
                    strValue = "{";
                }
                else if (strValue == ",")
                {
                    strValue = "}";
                }
                else
                {

                    strValue = Convert.ToChar(c1 - key).ToString();
                }



                DataTable DT;
                DataRow[] TempRow = null;
                if (strValue.Any(char.IsLower) == true)
                {
                    DT = (DataTable)CharEncryptData;
                    TempRow = DT.Select("Name='" + strValue + "'");
                    z += Convert.ToString(TempRow[0]["Code"]);// character.ToString();
                }
                else if (strValue.Any(char.IsUpper) == true)
                {
                    DT = (DataTable)UpperCharEncryptData;
                    TempRow = DT.Select("Name='" + strValue + "'");
                    z += Convert.ToString(TempRow[0]["Code"]);// character.ToString();
                }
                else if (strValue.Any(char.IsDigit) == true)
                {
                    DT = (DataTable)DigitEncryptData;
                    TempRow = DT.Select("Name='" + strValue + "'");
                    z += Convert.ToString(TempRow[0]["Code"]);// character.ToString();
                }
                else if (CheckSplChar(strValue) == true)
                {
                    DT = (DataTable)SplCharEncryptData;
                    TempRow = DT.Select("Name='" + strValue + "'");
                    z += Convert.ToString(TempRow[0]["Code"]);// character.ToString();
                }
                else
                {
                    z += strValue;// character.ToString();

                }



                // c1 = Asc(Mid(Name, v, 1))
                //c1 = Chr(c1 + Key)
                /// z = z & c1
            }
            return z;
        }
    }
}
