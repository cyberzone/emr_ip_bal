﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_CongenitaHeartDiseaseScreening
    {
        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }

        public string ICHDS_AGE { set; get; }

        public string ICHDS_INI_TIME { set; get; }
        public string ICHDS_INI_PULSE_OX_SATU_RHAND { set; get; }
        public string ICHDS_INI_PULSE_OX_SATU_FOOT { set; get; }
        public string ICHDS_INI_DIFF_RHAND_FOOT { set; get; }
        public string ICHDS_INI_DIFF_RESULT { set; get; }

        public string ICHDS_SEC_TIME { set; get; }
        public string ICHDS_SEC_PULSE_OX_SATU_RHAND { set; get; }
        public string ICHDS_SEC_PULSE_OX_SATU_FOOT { set; get; }
        public string ICHDS_SEC_DIFF_RHAND_FOOT { set; get; }
        public string ICHDS_SEC_DIFF_RESULT { set; get; }

        public string ICHDS_THI_TIME { set; get; }
        public string ICHDS_THI_PULSE_OX_SATU_RHAND { set; get; }
        public string ICHDS_THI_PULSE_OX_SATU_FOOT { set; get; }
        public string ICHDS_THI_DIFF_RHAND_FOOT { set; get; }
        public string ICHDS_THI_DIFF_RESULT { set; get; }

        public string UserID { set; get; }


        public DataSet CongHearDiseaseScreeGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_CongHearDiseaseScreeGet", Param);

            return DS;


        }


        public void CongHearDiseaseScreeAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BranchID", BranchID);
            Param.Add("EMRID", EMRID);
            Param.Add("PTID", PTID);
            Param.Add("ICHDS_AGE", ICHDS_AGE);
            Param.Add("ICHDS_INI_TIME", ICHDS_INI_TIME);
            Param.Add("ICHDS_INI_PULSE_OX_SATU_RHAND", ICHDS_INI_PULSE_OX_SATU_RHAND);
            Param.Add("ICHDS_INI_PULSE_OX_SATU_FOOT", ICHDS_INI_PULSE_OX_SATU_FOOT);
            Param.Add("ICHDS_INI_DIFF_RHAND_FOOT", ICHDS_INI_DIFF_RHAND_FOOT);
            Param.Add("ICHDS_INI_DIFF_RESULT", ICHDS_INI_DIFF_RESULT);

            Param.Add("ICHDS_SEC_TIME", ICHDS_SEC_TIME);
            Param.Add("ICHDS_SEC_PULSE_OX_SATU_RHAND", ICHDS_SEC_PULSE_OX_SATU_RHAND);
            Param.Add("ICHDS_SEC_PULSE_OX_SATU_FOOT", ICHDS_SEC_PULSE_OX_SATU_FOOT);
            Param.Add("ICHDS_SEC_DIFF_RHAND_FOOT", ICHDS_SEC_DIFF_RHAND_FOOT);
            Param.Add("ICHDS_SEC_DIFF_RESULT", ICHDS_SEC_DIFF_RESULT);

            Param.Add("ICHDS_THI_TIME", ICHDS_THI_TIME);
            Param.Add("ICHDS_THI_PULSE_OX_SATU_RHAND", ICHDS_THI_PULSE_OX_SATU_RHAND);
            Param.Add("ICHDS_THI_PULSE_OX_SATU_FOOT", ICHDS_THI_PULSE_OX_SATU_FOOT);
            Param.Add("ICHDS_THI_DIFF_RHAND_FOOT", ICHDS_THI_DIFF_RHAND_FOOT);
            Param.Add("ICHDS_THI_DIFF_RESULT", ICHDS_THI_DIFF_RESULT);


            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_IP_CongHearDiseaseScreeAdd", Param);

        }

    }
}
