﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class EMR_PTRadiology
    {


        dbOperation objDB = new dbOperation();

        public string BRANCH_ID { set; get; }
        public string EPR_ID { set; get; }
        public string EPR_RAD_CODE { set; get; }
        public string EPR_RAD_NAME { set; get; }
        public string EPR_REMARKS { set; get; }
        public string EPR_QTY { set; get; }
        public string EPR_TEMPLATE_CODE { set; get; }


        public DataSet RadiologyGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRPTRadiologyGet", Param);

            return DS;


        }


        public void RadiologyAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPR_BRANCH_ID", BRANCH_ID);
            Param.Add("EPR_ID", EPR_ID);
            Param.Add("EPR_RAD_CODE", EPR_RAD_CODE);
            Param.Add("EPR_RAD_NAME", EPR_RAD_NAME);
            Param.Add("EPR_REMARKS", EPR_REMARKS);

            Param.Add("EPR_TEMPLATE_CODE", EPR_TEMPLATE_CODE);

            objDB.ExecuteNonQuery("HMS_SP_EMR_RadiologyAdd", Param);

        }




        public void RadiologyDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPR_BRANCH_ID", BRANCH_ID);
            Param.Add("EPR_ID", EPR_ID);
            Param.Add("EPR_RAD_CODE", EPR_RAD_CODE);

            objDB.ExecuteNonQuery("HMS_SP_EMR_RadiologyDelete", Param);

        }
    }
}
