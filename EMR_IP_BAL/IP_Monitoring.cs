﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_Monitoring
    {
        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string IPN_MON_ID { set; get; }
        public string IPN_NOTES { set; get; }
        public string IPN_TYPE { set; get; }

        public string IPN_STATUS { set; get; }
        public string IPN_MONITORING_DATE { set; get; }
        public string IPN_STAFF_ID { set; get; }
        public string IPN_STAFF_NAME { set; get; }

        public string UserID { set; get; }


        public DataSet PTMonitoringGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTMonitoringGet", Param);

            return DS;


        }


        public void PTMonitoringAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPN_BRANCH_ID", BranchID);
            Param.Add("IPN_ID", EMRID);
            Param.Add("IPN_PT_ID", PTID);
            Param.Add("IPN_MON_ID", IPN_MON_ID);

            Param.Add("IPN_NOTES", IPN_NOTES);
            Param.Add("IPN_TYPE", IPN_TYPE);
            Param.Add("IPN_STATUS", IPN_STATUS);
            Param.Add("IPN_MONITORING_DATE", IPN_MONITORING_DATE);
            Param.Add("IPN_STAFF_ID", IPN_STAFF_ID);
            Param.Add("IPN_STAFF_NAME", IPN_STAFF_NAME);
            Param.Add("UserID", UserID);


            objDB.ExecuteNonQuery("HMS_SP_IP_PTMonitoringAdd", Param);

        }
    }
}
