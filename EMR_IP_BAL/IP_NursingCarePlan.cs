﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
    public class IP_NursingCarePlan
    {

        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string INCP_NURC_ID { set; get; }
        public string INCP_DATE { set; get; }
        public string INCP_ASSESSMENT { set; get; }
        public string INCP_DIAGNOSIS { set; get; }
        public string INCP_IMPLEMENTATION { set; get; }
        public string INCP_EVALUATION { set; get; }


        public string UserID { set; get; }


        public DataSet NurseCarePlanGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPNurseCarePlanGet", Param);

            return DS;


        }


        public void  NurseCarePlanAdd()
        {
 

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("INCP_BRANCH_ID", BranchID);
            Param.Add("INCP_ID", EMRID);
            Param.Add("INCP_PT_ID", PTID);
            Param.Add("INCP_NURC_ID", INCP_NURC_ID);

            Param.Add("INCP_DATE", INCP_DATE);
            Param.Add("INCP_ASSESSMENT", INCP_ASSESSMENT);
            Param.Add("INCP_DIAGNOSIS", INCP_DIAGNOSIS);
            Param.Add("INCP_IMPLEMENTATION", INCP_IMPLEMENTATION);
            Param.Add("INCP_EVALUATION", INCP_EVALUATION);
         

        
            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_IPNurseCarePlanAdd", Param);

        }
    }
}
