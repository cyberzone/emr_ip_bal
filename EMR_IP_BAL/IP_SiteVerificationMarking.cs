﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_SiteVerificationMarking
    {









        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }
        public string ISVM_NAME_VERBALIZED_PT_CHART { set; get; }
        public string ISVM_BDATE_VERBALIZED_PT_CHART { set; get; }
        public string ISVM_IDBAND_CHART { set; get; }
        public string ISVM_PT_IDEN_FAMILY_MEMBER { set; get; }
        public string ISVM_PT_IDEN_UNIT { set; get; }
        public string ISVM_PT_IDEN_OTHER { set; get; }


        public string ISVM_PROCEDURE { set; get; }
        public string ISVM_MARKING { set; get; }
        public string ISVM_MARK_REFUSE { set; get; }
        public string ISVM_MARK_OTHER { set; get; }
        public string ISVM_MARK_FAMILY_PARTICI { set; get; }
        public string ISVM_MARK_FAMILY_PARTICI_DESC { set; get; }
        public string ISVM_MARK_LOCATION { set; get; }
        public string ISVM_MARK_LOCATION_DESC { set; get; }
        public string ISVM_SITE_MARKED_BY { set; get; }
        public string ISVM_SITE_CONFIRM_BY { set; get; }
        public string ISVM_SITE_MARK_DATE { set; get; }
        public string ISVM_VERIFI_SIDE { set; get; }
        public string ISVM_PT_CAME_FROM { set; get; }

        public string ISVM_CONSISTENT_CONSENT { set; get; }
        public string ISVM_CONSISTENT_HIS_PHY { set; get; }
        public string ISVM_CONSISTENT_XRAY_RESULT { set; get; }
        public string ISVM_CONSISTENT_SITE_MARK { set; get; }


        public string ISVM_DISCRIDENTIFIED { set; get; }
        public string ISVM_DISCRIDENTIFIED_EXP { set; get; }
        public string ISVM_RESOLUTION { set; get; }
        public string ISVM_RESOLUTION_DESC { set; get; }




        public string ISVSD_TYPE { set; get; }
        public string ISVSD_FIELD_ID { set; get; }
        public string ISVSD_FIELD_NAME { set; get; }
        public string ISVSD_VALUE { set; get; }
        public string ISVSD_COMMENT { set; get; }


        public string UserID { set; get; }


        public DataSet SiteVerificationMarkingGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_SiteVerificationMarkingGet", Param);

            return DS;


        }


        public void SiteVerificationMarkingAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BranchID", BranchID);
            Param.Add("EMRID", EMRID);
            Param.Add("PTID", PTID);

            Param.Add("ISVM_NAME_VERBALIZED_PT_CHART", ISVM_NAME_VERBALIZED_PT_CHART);
            Param.Add("ISVM_BDATE_VERBALIZED_PT_CHART", ISVM_BDATE_VERBALIZED_PT_CHART);
            Param.Add("ISVM_IDBAND_CHART", ISVM_IDBAND_CHART);
            Param.Add("ISVM_PT_IDEN_FAMILY_MEMBER", ISVM_PT_IDEN_FAMILY_MEMBER);

            Param.Add("ISVM_PT_IDEN_UNIT", ISVM_PT_IDEN_UNIT);
            Param.Add("ISVM_PT_IDEN_OTHER", ISVM_PT_IDEN_OTHER);



            Param.Add("ISVM_PROCEDURE", ISVM_PROCEDURE);
            Param.Add("ISVM_MARKING", ISVM_MARKING);
            Param.Add("ISVM_MARK_REFUSE", ISVM_MARK_REFUSE);
            Param.Add("ISVM_MARK_OTHER", ISVM_MARK_OTHER);
            Param.Add("ISVM_MARK_FAMILY_PARTICI", ISVM_MARK_FAMILY_PARTICI);
            Param.Add("ISVM_MARK_FAMILY_PARTICI_DESC", ISVM_MARK_FAMILY_PARTICI_DESC);
            Param.Add("ISVM_MARK_LOCATION", ISVM_MARK_LOCATION);
            Param.Add("ISVM_MARK_LOCATION_DESC", ISVM_MARK_LOCATION_DESC);
            Param.Add("ISVM_SITE_MARKED_BY", ISVM_SITE_MARKED_BY);
            Param.Add("ISVM_SITE_CONFIRM_BY", ISVM_SITE_CONFIRM_BY);
            Param.Add("ISVM_SITE_MARK_DATE", ISVM_SITE_MARK_DATE);
            Param.Add("ISVM_VERIFI_SIDE", ISVM_VERIFI_SIDE);
            Param.Add("ISVM_PT_CAME_FROM", ISVM_PT_CAME_FROM);
           
            Param.Add("ISVM_CONSISTENT_CONSENT", ISVM_CONSISTENT_CONSENT);
            Param.Add("ISVM_CONSISTENT_HIS_PHY", ISVM_CONSISTENT_HIS_PHY);
            Param.Add("ISVM_CONSISTENT_XRAY_RESULT", ISVM_CONSISTENT_XRAY_RESULT);
            Param.Add("ISVM_CONSISTENT_SITE_MARK", ISVM_CONSISTENT_SITE_MARK);

            Param.Add("ISVM_DISCRIDENTIFIED", ISVM_DISCRIDENTIFIED);
            Param.Add("ISVM_DISCRIDENTIFIED_EXP", ISVM_DISCRIDENTIFIED_EXP);
            Param.Add("ISVM_RESOLUTION", ISVM_RESOLUTION);
            Param.Add("ISVM_RESOLUTION_DESC", ISVM_RESOLUTION_DESC);

            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_IP_SiteVerificationMarkingAdd", Param);

        }


        public DataSet  SiteVerificationDtlsGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_SiteVerificationDtlsGet", Param);

            return DS;


        }


        public void SiteVerificationSegmentDtlsAdd()
        {


            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("ISVSD_BRANCH_ID", BranchID);
            Param.Add("ISVSD_ID", EMRID);
            Param.Add("ISVSD_PT_ID", PTID);

            Param.Add("ISVSD_TYPE", ISVSD_TYPE);
            Param.Add("ISVSD_FIELD_ID", ISVSD_FIELD_ID);
            Param.Add("ISVSD_FIELD_NAME", ISVSD_FIELD_NAME);
            Param.Add("ISVSD_VALUE", ISVSD_VALUE);
            Param.Add("ISVSD_COMMENT", ISVSD_COMMENT);



            objDB.ExecuteNonQuery("HMS_SP_IP_SiteVerificationDtlsAdd", Param);

        }
    }
}
