﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
    public class HL7MessageGenerator
    {
        dbOperation objDB = new dbOperation();
        public string BRANCH_ID { set; get; }

        public string PT_ID { set; get; }
        public string VISIT_ID { set; get; }
        public string EMR_ID { set; get; }
        public string MessageID { set; get; }
        public string HL7FileName { set; get; }
        public string MessageCode { set; get; }
        public string MessageDesc { set; get; }
        public string MESSAGE_TYPE { set; get; }
        public string RETURN_MESSAGE { set; get; }
        public string UPLOAD_STATUS { set; get; }
        public string SERV_ID { set; get; }

        public string BRANCH_NAME { set; get; }
        public string PROVIDER_ID { set; get; }
        public string FileDescription { set; get; }

        public string IP_ADMISSION_NO { set; get; }

        public string UserID { set; get; }


        public void MalaffiHL7MessageMasterAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BRANCH_ID", BRANCH_ID);
            Param.Add("PT_ID", PT_ID);
            Param.Add("VISIT_ID", VISIT_ID);
            Param.Add("EMR_ID", EMR_ID);
            Param.Add("MessageID", MessageID);
            Param.Add("FileName", HL7FileName);
            Param.Add("MessageCode", MessageCode);
            Param.Add("MessageDesc", MessageDesc);
            Param.Add("MESSAGE_TYPE", MESSAGE_TYPE);
            Param.Add("RETURN_MESSAGE", RETURN_MESSAGE);
            Param.Add("UPLOAD_STATUS", UPLOAD_STATUS);
            Param.Add("SERV_ID", SERV_ID);
            Param.Add("IP_ADMISSION_NO", IP_ADMISSION_NO);

            Param.Add("UserID", UserID);


            objDB.ExecuteNonQuery("HMS_SP_MalaffiHL7MessageMasterAdd", Param);

        }
    }
}
