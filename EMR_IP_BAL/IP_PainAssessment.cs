﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_PainAssessment
    {
        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string IPA_PAIN_RADIATION { set; get; }
        public string IPA_PAIN_PATTERN { set; get; }
        public string IPA_PAIN_ONSET { set; get; }
        public string IPA_PAIN_CAUSES { set; get; }
        public string IPA_PAIN_RELIEVES { set; get; }
        public string IPA_EFFECTS_ONACTIVITIES { set; get; }
        public string IPA_MEDICATION { set; get; }
        public string IPA_NONMEDICATION { set; get; }
        public string IPA_OTHER_INTERVENTIONS { set; get; }
        public string IPA_INTERVENTION_TIME { set; get; }
        public string IPA_PAIN_ASSESS_SCALE { set; get; }



        public string IPAD_ASS_ID { set; get; }
        public string IPAD_DATE { set; get; }
        public string IPAD_LOCATION { set; get; }
        public string IPAD_PAIN_INTENSITY { set; get; }
        public string IPAD_CHARACTER_CODE { set; get; }
        public string IPAD_FREQUENCY { set; get; }
        public string IPAD_DURATION { set; get; }
        public string IPAD_CONT_CURR_MED { set; get; }
        public string IPAD_MEDICATION { set; get; }
        public string IPAD_NONMEDICATION { set; get; }
        public string IPAD_OTHER_INTERVENTIONS { set; get; }





        public string IPNAD_ASS_ID { set; get; }
        public string IPNAD_DATE { set; get; }
        public string IPNAD_PAIN_INTENSITY { set; get; }
        public string IPNAD_PAIN_CHANGES { set; get; }
        public string IPNAD_INTERVENTIONS { set; get; }



        public string UserID { set; get; }


        public DataSet  PainAssessmentGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPPainAssessmentGet", Param);

            return DS;


        }

        public DataSet PainAssessmentDtlsGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPPainAssessmentDtlsGet", Param);

            return DS;


        }

        public DataSet  PainReAssessmentDtlsGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPPainReAssessmentDtlsGet", Param);

            return DS;


        }

        public void PainAssessmentAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPA_BRANCH_ID", BranchID);
            Param.Add("IPA_ID", EMRID);
            Param.Add("IPA_PT_ID", PTID);

            Param.Add("IPA_PAIN_RADIATION", IPA_PAIN_RADIATION);
            Param.Add("IPA_PAIN_PATTERN", IPA_PAIN_PATTERN);
            Param.Add("IPA_PAIN_ONSET", IPA_PAIN_ONSET);
            Param.Add("IPA_PAIN_CAUSES", IPA_PAIN_CAUSES);
            Param.Add("IPA_PAIN_RELIEVES", IPA_PAIN_RELIEVES);
            Param.Add("IPA_EFFECTS_ONACTIVITIES", IPA_EFFECTS_ONACTIVITIES);

            Param.Add("IPA_MEDICATION", IPA_MEDICATION);
            Param.Add("IPA_NONMEDICATION", IPA_NONMEDICATION);
            Param.Add("IPA_OTHER_INTERVENTIONS", IPA_OTHER_INTERVENTIONS);
            Param.Add("IPA_INTERVENTION_TIME", IPA_INTERVENTION_TIME);
            Param.Add("IPA_PAIN_ASSESS_SCALE", IPA_PAIN_ASSESS_SCALE);
            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_IPPainAssessmentAdd", Param);

        }

        public void PainAssessmentDtlsAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPAD_BRANCH_ID", BranchID);
            Param.Add("IPAD_ID", EMRID);
            Param.Add("IPAD_PT_ID", PTID);

            Param.Add("IPAD_ASS_ID", IPAD_ASS_ID);
            Param.Add("IPAD_DATE", IPAD_DATE);
            Param.Add("IPAD_LOCATION", IPAD_LOCATION);
            Param.Add("IPAD_PAIN_INTENSITY", IPAD_PAIN_INTENSITY);
            Param.Add("IPAD_CHARACTER_CODE", IPAD_CHARACTER_CODE);
            Param.Add("IPAD_FREQUENCY", IPAD_FREQUENCY);
            Param.Add("IPAD_DURATION", IPAD_DURATION);

            Param.Add("IPAD_CONT_CURR_MED", IPAD_CONT_CURR_MED);
            Param.Add("IPAD_MEDICATION", IPAD_MEDICATION);
            Param.Add("IPAD_NONMEDICATION", IPAD_NONMEDICATION);
            Param.Add("IPAD_OTHER_INTERVENTIONS", IPAD_OTHER_INTERVENTIONS);


            objDB.ExecuteNonQuery("HMS_SP_IPPainAssessmentDtlsAdd", Param);

        }

        public void PainReAssessmentDtlsAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPNAD_BRANCH_ID", BranchID);
            Param.Add("IPNAD_ID", EMRID);
            Param.Add("IPNAD_PT_ID", PTID);

            Param.Add("IPNAD_ASS_ID", IPNAD_ASS_ID);
            Param.Add("IPNAD_DATE", IPNAD_DATE);
            Param.Add("IPNAD_PAIN_INTENSITY", IPNAD_PAIN_INTENSITY);
            Param.Add("IPNAD_PAIN_CHANGES", IPNAD_PAIN_CHANGES);
            Param.Add("IPNAD_INTERVENTIONS", IPNAD_INTERVENTIONS);




            objDB.ExecuteNonQuery("HMS_SP_IPPainReAssessmentDtlsAdd", Param);

        }

    }
}
