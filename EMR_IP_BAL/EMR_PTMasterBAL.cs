﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
    public class EMR_PTMasterBAL
    {
        dbOperation objDB = new dbOperation();

        public string EPM_ID { set; get; }
        public string EPM_BRANCH_ID { set; get; }
        public string EPM_DATE { set; get; }
        public string EPM_PT_ID { set; get; }
        public string EPM_PT_NAME { set; get; }
        public string EPM_AGE { set; get; }
        public string EPM_AGE_TYPE { set; get; }
        public string EPM_DR_CODE { set; get; }
        public string EPM_DR_NAME { set; get; }
        public string EPM_INS_CODE { set; get; }
        public string EPM_INS_NAME { set; get; }
        public string EPM_START_DATE { set; get; }
        public string EPM_END_DATE { set; get; }
        public string EPM_DEP_ID { set; get; }
        public string EPM_DEP_NAME { set; get; }
        public string EMR_PT_TYPE { set; get; }
        public string EPM_TYPE { set; get; }
        public string EPM_REF_DR_CODE { set; get; }
        public string UserId { set; get; }
        public string StartProcess { set; get; }
        public string VisitDate { set; get; }



        public string EPM_PAINSCORE { set; get; }
        public string EPM_PAINSCORE_LOCATION { set; get; }
        public string EPM_PAINSCORE_INTENSITY { set; get; }
        public string EPM_PAINSCORE_CHARACTER { set; get; }
        public string EPM_PAINSCORE_DURATION { set; get; }
        public string EPM_PAINSCORE_OTHERS { set; get; }

        public string EPM_TREATMENT_PLAN { set; get; }
        public string EPM_FOLLOWUP_NOTES { set; get; }

        public string EPM_CRITICAL_NOTES { set; get; }

        public string ProcessCompleted { set; get; }
        public string EPM_VIP { set; get; }
        public string EPM_ROYAL { set; get; }


        public DataSet GetEMR_PTMaster(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRPTMasterGet", Param);



            return DS;

        }

        public DataSet GetHistory()
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("filenumber", EPM_PT_ID);
            DS = objDB.ExecuteReader("WEMR_spS_GetHistory", Param);

            return DS;
        }



        public void AddEMR_PTMaster(out string EMR_ID)
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPM_BRANCH_ID", EPM_BRANCH_ID);
            Param.Add("EPM_ID", EPM_ID);
            Param.Add("EPM_DATE", EPM_DATE);
            Param.Add("EPM_PT_ID", EPM_PT_ID);
            Param.Add("EPM_PT_NAME", EPM_PT_NAME);
            Param.Add("EPM_AGE", EPM_AGE);

            Param.Add("EPM_AGE_TYPE", EPM_AGE_TYPE);
            Param.Add("EPM_DR_CODE", EPM_DR_CODE);
            Param.Add("EPM_DR_NAME", EPM_DR_NAME);
            Param.Add("EPM_INS_CODE", EPM_INS_CODE);
            Param.Add("EPM_INS_NAME", EPM_INS_NAME);

            Param.Add("EPM_START_DATE", EPM_START_DATE);
            Param.Add("EPM_END_DATE", EPM_END_DATE);
            Param.Add("EPM_DEP_ID", EPM_DEP_ID);
            Param.Add("EPM_DEP_NAME", EPM_DEP_NAME);
            Param.Add("EMR_PT_TYPE", EMR_PT_TYPE);
            Param.Add("EPM_TYPE", EPM_TYPE);
            Param.Add("EPM_REF_DR_CODE", EPM_REF_DR_CODE);
            Param.Add("VisitDate", VisitDate);
            Param.Add("StartProcess", StartProcess);
            Param.Add("UserId", UserId);

            EMR_ID = objDB.ExecuteNonQueryReturn("HMS_SP_EMR_PT_Add", Param);

        }

        public void EMRPTStartProcess()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPM_BRANCH_ID", EPM_BRANCH_ID);
            Param.Add("EPM_ID", EPM_ID);
            Param.Add("EPM_PT_ID", EPM_PT_ID);
            Param.Add("EPM_DR_CODE", EPM_DR_CODE);
            Param.Add("VisitDate", VisitDate);
            Param.Add("UserId", UserId);
            objDB.ExecuteNonQuery("HMS_SP_EMRStartProcess", Param);

        }


        public void UpdateEMR_PTMasterPainScore(string Criteria)
        {

            string strSQL = "";

            strSQL = "UPDATE EMR_PT_MASTER SET "
            + "   EPM_PAINSCORE='" + EPM_PAINSCORE + "' , EPM_PAINSCORE_LOCATION='" + EPM_PAINSCORE_LOCATION + "'"
            + " , EPM_PAINSCORE_INTENSITY='" + EPM_PAINSCORE_INTENSITY + "' , EPM_PAINSCORE_CHARACTER='" + EPM_PAINSCORE_CHARACTER + "'"
            + " , EPM_PAINSCORE_DURATION='" + EPM_PAINSCORE_DURATION + "' , EPM_PAINSCORE_OTHERS='" + EPM_PAINSCORE_OTHERS + "'"
            + " WHERE " + Criteria;

            objDB.ExecuteNonQuery(strSQL);


        }


        public void UpdateEMR_PTMasterOthers(string Criteria)
        {

            string strSQL = "";

            strSQL = "UPDATE EMR_PT_MASTER SET "
            + "  EPM_TREATMENT_PLAN='" + EPM_TREATMENT_PLAN + "',EPM_FOLLOWUP_NOTES='" + EPM_FOLLOWUP_NOTES + "'"
            + " WHERE " + Criteria;

            objDB.ExecuteNonQuery(strSQL);

        }

        public void UpdateEMR_PTMasterCriticalNotes(string Criteria)
        {

            string strSQL = "";

            strSQL = "UPDATE EMR_PT_MASTER SET "
            + "  EPM_CRITICAL_NOTES='" + EPM_CRITICAL_NOTES + "'"
            + " WHERE " + Criteria;

            objDB.ExecuteNonQuery(strSQL);

        }

        public void AddPatientProcess()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", EPM_BRANCH_ID);
            Param.Add("patientmasterid", EPM_ID);

            Param.Add("criticalnotes", EPM_CRITICAL_NOTES);
            Param.Add("processcompleted", ProcessCompleted);

            Param.Add("vip", EPM_VIP);
            Param.Add("royal", EPM_ROYAL);


            Param.Add("startdate", EPM_START_DATE);
            Param.Add("enddate", EPM_END_DATE);

            Param.Add("date", EPM_DATE);
            Param.Add("doctorid", EPM_DR_CODE);

            objDB.ExecuteNonQuery("WEMR_spI_SavePatientProcess", Param);

        }



    }
}
