﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
    public class IP_PTPharmacy
    {
        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string IPP_PHY_ID { set; get; }

        public string IPP_PHY_CODE { set; get; }
        public string IPP_PHY_NAME { set; get; }
        public string IPP_DOSAGE { set; get; }
        public string IPP_ROUTE { set; get; }
        public string IPP_REFILL { set; get; }
        public string IPP_DURATION { set; get; }
        public string IPP_DURATION_TYPE { set; get; }
        public string IPP_START_DATE { set; get; }
        public string IPP_END_DATE { set; get; }
        public string IPP_INTERVAL { set; get; }
        public string IPP_REMARKS { set; get; }
        public string IPP_QTY { set; get; }
        public string IPP_DOSAGE1 { set; get; }
        public string IPP_TAKEN { set; get; }
        public string IPP_STATUS { set; get; }

        public string IPP_STOP { set; get; }
        public string IPP_STOP_REASON { set; get; }


        public string IPPS_DATE { set; get; }
        public string IPPS_GIVENBY { set; get; }
        public string IPPS_REMARKS { set; get; }

        public DataSet IPPharmacyGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTPharmacyGet", Param);

            return DS;


        }


        public void IPPharmacyAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPP_BRANCH_ID", BranchID);
            Param.Add("IPP_ID", EMRID);
            Param.Add("IPP_PT_ID", PTID);
            Param.Add("IPP_PHY_ID", IPP_PHY_ID);

            Param.Add("IPP_PHY_CODE", IPP_PHY_CODE);
            Param.Add("IPP_PHY_NAME", IPP_PHY_NAME);
            Param.Add("IPP_DOSAGE", IPP_DOSAGE);
            Param.Add("IPP_ROUTE", IPP_ROUTE);
            Param.Add("IPP_REFILL", IPP_REFILL);

            Param.Add("IPP_DURATION", IPP_DURATION);
            Param.Add("IPP_DURATION_TYPE", IPP_DURATION_TYPE);
            Param.Add("IPP_START_DATE", IPP_START_DATE);
            Param.Add("IPP_END_DATE", IPP_END_DATE);
            Param.Add("IPP_INTERVAL", IPP_INTERVAL);
            Param.Add("IPP_REMARKS", IPP_REMARKS);
            Param.Add("IPP_QTY", IPP_QTY);
            Param.Add("IPP_DOSAGE1", IPP_DOSAGE1);
            Param.Add("IPP_TAKEN", IPP_TAKEN);
            Param.Add("IPP_STATUS", IPP_STATUS);
            Param.Add("IPP_STOP", IPP_STOP);
            Param.Add("IPP_STOP_REASON", IPP_STOP_REASON);

            objDB.ExecuteNonQuery("HMS_SP_IP_PTPharmacyAdd", Param);

        }

        public void IPPharmacyDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPP_BRANCH_ID", BranchID);
            Param.Add("IPP_ID", EMRID);
            Param.Add("IPP_PHY_ID", IPP_PHY_ID);


            objDB.ExecuteNonQuery("HMS_SP_IP_PTPharmacyDelete", Param);

        }

        public DataSet IPPharmacySheduleGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PharmacySheduleGet", Param);

            return DS;


        }
        public void IPPharmacySheduleAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPPS_BRANCH_ID", BranchID);
            Param.Add("IPPS_ID", EMRID);

            Param.Add("IPPS_PHY_ID", IPP_PHY_ID);

            Param.Add("IPPS_DATE", IPPS_DATE);
            Param.Add("IPPS_GIVENBY", IPPS_GIVENBY);
            Param.Add("IPPS_REMARKS", IPPS_REMARKS);


            objDB.ExecuteNonQuery("HMS_SP_IP_PharmacySheduleAdd", Param);

        }
    }
}
