﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_PTDiagnosis
    {

        dbOperation objDB = new dbOperation();

        public string BRANCH_ID { set; get; }
        public string IPD_ID { set; get; }
        public string IPD_DIAG_ID { set; get; }
        public string IPD_DIAG_CODE { set; get; }
        public string IPD_DIAG_NAME { set; get; }

        public string IPD_REMARKS { set; get; }
        public string IPD_TEMPLATE_CODE { set; get; }
        public string IPD_PROBLEM_TYPE { set; get; }
        public string IPD_POSITION { set; get; }
        public string IPD_TYPE { set; get; }

        public DataSet IPDiagnosisGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTDiagnosisGet", Param);

            return DS;


        }


        public void IPDiagnosisAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPD_BRANCH_ID", BRANCH_ID);
            Param.Add("IPD_ID", IPD_ID);
            Param.Add("IPD_DIAG_ID", IPD_DIAG_ID);
            Param.Add("IPD_DIAG_CODE", IPD_DIAG_CODE);
            Param.Add("IPD_DIAG_NAME", IPD_DIAG_NAME);
            Param.Add("IPD_REMARKS", IPD_REMARKS);
            Param.Add("IPD_TEMPLATE_CODE", IPD_TEMPLATE_CODE);
            Param.Add("IPD_PROBLEM_TYPE", IPD_PROBLEM_TYPE);
            Param.Add("IPD_TYPE", IPD_TYPE);
            Param.Add("IPD_POSITION", IPD_POSITION);


            objDB.ExecuteNonQuery("HMS_SP_IP_PTDiagnosisAdd", Param);

        }


        public void IPDiagnosisDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPD_BRANCH_ID", BRANCH_ID);
            Param.Add("IPD_ID", IPD_ID);
            Param.Add("IPD_DIAG_ID", IPD_DIAG_ID);

            objDB.ExecuteNonQuery("HMS_SP_IP_PTDiagnosisDelete", Param);

        }
        public DataSet IPDiscDiagnosisGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTDiscDiagnosisGet", Param);

            return DS;


        }

        public void IPDiscDiagnosisAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPD_BRANCH_ID", BRANCH_ID);
            Param.Add("IPD_ID", IPD_ID);
            Param.Add("IPD_DIAG_ID", IPD_DIAG_ID);
            Param.Add("IPD_DIAG_CODE", IPD_DIAG_CODE);
            Param.Add("IPD_DIAG_NAME", IPD_DIAG_NAME);
            Param.Add("IPD_TYPE", IPD_TYPE);
            Param.Add("IPD_POSITION", IPD_POSITION);


            objDB.ExecuteNonQuery("HMS_SP_IP_PTDiscDiagnosisAdd", Param);

        }


        public void IPDiscDiagnosisDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPD_BRANCH_ID", BRANCH_ID);
            Param.Add("IPD_ID", IPD_ID);
            Param.Add("IPD_DIAG_ID", IPD_DIAG_ID);

            objDB.ExecuteNonQuery("HMS_SP_IP_PTDiscDiagnosisDelete", Param);

        }


    }
}
