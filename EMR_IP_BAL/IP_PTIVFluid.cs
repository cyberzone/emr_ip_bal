﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
  public  class IP_PTIVFluid
    {
        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string IPI_IV_ID { set; get; }

        public string IPI_IV_CODE { set; get; }
        public string IPI_IV_NAME { set; get; }
        public string IPI_DOSAGE { set; get; }
        public string IPI_ROUTE { set; get; }
        public string IPI_REFILL { set; get; }
        public string IPI_DURATION { set; get; }
        public string IPI_DURATION_TYPE { set; get; }
        public string IPI_START_DATE { set; get; }
        public string IPI_END_DATE { set; get; }
        public string IPI_INTERVAL { set; get; }
        public string IPI_REMARKS { set; get; }
        public string IPI_QTY { set; get; }
        public string IPI_DOSAGE1 { set; get; }
        public string IPI_TAKEN { set; get; }
        public string IPI_STATUS { set; get; }


        public string IPIS_DATE { set; get; }
        public string IPIS_GIVENBY { set; get; }
        public string IPIS_REMARKS { set; get; }
        public string IPIS_ENDTIME { set; get; }


        public DataSet IPPTIVFluidGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTIVFluidGet", Param);

            return DS;


        }


        public void IPPTIVFluidAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPI_BRANCH_ID", BranchID);
            Param.Add("IPI_ID", EMRID);
            Param.Add("IPI_PT_ID", PTID);
            Param.Add("IPI_IV_ID", IPI_IV_ID);

            Param.Add("IPI_IV_CODE", IPI_IV_CODE);
            Param.Add("IPI_IV_NAME", IPI_IV_NAME);
            Param.Add("IPI_DOSAGE", IPI_DOSAGE);
            Param.Add("IPI_ROUTE", IPI_ROUTE);
            Param.Add("IPI_REFILL", IPI_REFILL);

            Param.Add("IPI_DURATION", IPI_DURATION);
            Param.Add("IPI_DURATION_TYPE", IPI_DURATION_TYPE);
            Param.Add("IPI_START_DATE", IPI_START_DATE);
            Param.Add("IPI_END_DATE", IPI_END_DATE);
            Param.Add("IPI_INTERVAL", IPI_INTERVAL);
            Param.Add("IPI_REMARKS", IPI_REMARKS);
            Param.Add("IPI_QTY", IPI_QTY);
            Param.Add("IPI_DOSAGE1", IPI_DOSAGE1);
            Param.Add("IPI_TAKEN", IPI_TAKEN);
            Param.Add("IPI_STATUS", IPI_STATUS);

            objDB.ExecuteNonQuery("HMS_SP_IP_PTIVFluidAdd", Param);

        }

        public void IPPTIVFluidDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPI_BRANCH_ID", BranchID);
            Param.Add("IPI_ID", EMRID);
            Param.Add("IPI_IV_ID", IPI_IV_ID);


            objDB.ExecuteNonQuery("HMS_SP_IP_PTIVFluidDelete", Param);

        }

        public DataSet IPIVFluidSheduleGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_IVFluidSheduleGet", Param);

            return DS;


        }
        public void IPIVFluidSheduleAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPIS_BRANCH_ID", BranchID);
            Param.Add("IPIS_ID", EMRID);

            Param.Add("IPIS_IV_ID", IPI_IV_ID);

            Param.Add("IPIS_DATE", IPIS_DATE);
            Param.Add("IPIS_ENDTIME", IPIS_ENDTIME);
            Param.Add("IPIS_GIVENBY", IPIS_GIVENBY);
            Param.Add("IPIS_REMARKS", IPIS_REMARKS);


            objDB.ExecuteNonQuery("HMS_SP_IP_IVFluidSheduleAdd", Param);

        }
    }
}
