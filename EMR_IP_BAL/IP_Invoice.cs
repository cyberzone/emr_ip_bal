﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_Invoice
    {


        dbOperation objDB = new dbOperation();


        public string HIM_BRANCH_ID { set; get; }

        public string HIM_INVOICE_ID { set; get; }
        public string HIM_INVOICE_TYPE { set; get; }
        public string HIM_DATE { set; get; }
        public string HIM_STATUS { set; get; }
        public string HIM_TOKEN_NO { set; get; }
        public string HIM_PT_ID { set; get; }
        public string HIM_PT_NAME { set; get; }
        public string HIM_BILLTOCODE { set; get; }
        public string HIM_SUB_INS_CODE { set; get; }
        public string HIM_INS_NAME { set; get; }
        public string HIM_SUB_INS_NAME { set; get; }
        public string HIM_PT_COMP { set; get; }
        public string HIM_POLICY_NO { set; get; }
        public string HIM_ID_NO { set; get; }
        public string HIM_POLICY_EXP { set; get; }
        public string HIM_INS_TRTNT_TYPE { set; get; }
        public string HIM_DR_CODE { set; get; }
        public string HIM_DR_NAME { set; get; }
        public string HIM_ORDERING_DR_CODE { set; get; }
        public string HIM_ORDERING_DR_NAME { set; get; }
        public string HIM_TRTNT_TYPE { set; get; }
        public string HIM_REF_DR_CODE { set; get; }
        public string HIM_REF_DR_NAME { set; get; }
        public string HIM_GROSS_TOTAL { set; get; }
        public string HIM_HOSP_DISC_TYPE { set; get; }
        public string HIM_HOSP_DISC { set; get; }
        public string HIM_HOSP_DISC_AMT { set; get; }
        public string HIM_CO_INS_TYPE { set; get; }
        public string HIM_CO_INS_AMOUNT { set; get; }
        public string HIM_DEDUCTIBLE { set; get; }
        public string HIM_NET_AMOUNT { set; get; }
        public string HIM_CLAIM_AMOUNT { set; get; }
        public string HIM_CLAIM_AMOUNT_PAID { set; get; }
        public string HIM_CLAIM_REJECTED { set; get; }
        public string HIM_PT_AMOUNT { set; get; }
        public string HIM_PT_CREDIT { set; get; }
        public string HIM_PT_CREDIT_PAID { set; get; }
        public string HIM_PT_REJECTED { set; get; }
        public string HIM_SPL_DISC { set; get; }
        public string HIM_PAID_AMOUNT { set; get; }
        public string HIM_PAYMENT_TYPE { set; get; }
        public string HIM_CC_TYPE { set; get; }
        public string HIM_CC_NAME { set; get; }
        public string HIM_CC_NO { set; get; }
        public string HIM_REF_NO { set; get; }
        public string HIM_CHEQ_NO { set; get; }
        public string HIM_BANK_NAME { set; get; }
        public string HIM_CHEQ_DATE { set; get; }
        public string HIM_CUR_TYP { set; get; }
        public string HIM_CUR_RATE { set; get; }
        public string HIM_CUR_VALUE { set; get; }
        public string HIM_RECEIPT_NO { set; get; }
        public string HIM_COMMIT_FLAG { set; get; }

        public string HIM_INV_REFNO { set; get; }
        public string HIM_INV_REFDATE { set; get; }
        public string HIM_RETURN_AMT { set; get; }
        public string HIM_CLAIM_RETURN_AMT { set; get; }
        public string HIM_VOUCHER_NO { set; get; }
        public string HIM_CASH_AMT { set; get; }
        public string HIM_CC_AMT { set; get; }
        public string HIM_CHEQUE_AMT { set; get; }
        public string HIM_CUR_RCVD_AMT { set; get; }
        public string HIM_POLICY_TYPE { set; get; }
        public string HIM_REMARKS { set; get; }
        public string HIM_TYPE { set; get; }
        public string HIM_COMMISSION_TYPE { set; get; }
        public string HIM_LAB_DONE { set; get; }
        public string HIM_RAD_DONE { set; get; }
        public string HIM_JOUR_NO { set; get; }
        public string HIM_CMP_INVOICE_NO { set; get; }
        public string HIM_MERGEINVOICES { set; get; }
        public string HIM_MERGEREMARKS { set; get; }
        public string HIM_ADV_AMT { set; get; }
        public string HPV_BUSINESSUNITCODE { set; get; }
        public string HPV_JOBNUMBER { set; get; }
        public string HIM_UPLOADED { set; get; }
        public string HIM_ADMN_NO { set; get; }
        public string HIM_INVOICE_ORDER { set; get; }
        public string HIM_MASTER_INVOICE_NUMBER { set; get; }
        public string HIM_ADVANCEUTILISED { set; get; }
        public string PHASECODE { set; get; }
        public string HIM_TRTNT_TYPE_01 { set; get; }
        public string HIM_TRTNT_Type_code { set; get; }
        public string HIM_SERVCOST_TOTAL { set; get; }
        public string HIM_READYFORECLAIM { set; get; }
        public string HIM_EMR_ID { set; get; }
        public string HIM_AUTHID { set; get; }
        public string HIM_CC { set; get; }
        public string HIM_RCPT_REMARK { set; get; }
        public string HIM_TOPUP { set; get; }
        public string HIM_TOPUP_AMT { set; get; }
        public string HIM_ADNL_REMARK { set; get; }

        public string NewFlag { set; get; }
        public string IsTopupInv { set; get; }
        public string UserID { set; get; }

        public string HIT_BRANCH_ID { set; get; }
        public string HIT_INVOICE_ID { set; get; }
        public string HIT_CAT_ID { set; get; }
        public string HIT_SERV_CODE { set; get; }
        public string HIT_DESCRIPTION { set; get; }
        public string HIT_FEE { set; get; }
        public string HIT_DISC_TYPE { set; get; }
        public string HIT_DISC_AMT { set; get; }
        public string HIT_QTY { set; get; }
        public string HIT_AMOUNT { set; get; }
        public string HIT_DEDUCT { set; get; }
        public string HIT_CO_INS_TYPE { set; get; }
        public string HIT_CO_INS_AMT { set; get; }
        public string HIT_CO_TOTAL { set; get; }
        public string HIT_COMP_TYPE { set; get; }
        public string HIT_COMP_AMT { set; get; }
        public string HIT_COMP_TOTAL { set; get; }
        public string HIT_PT_AMOUNT { set; get; }
        public string HIT_DISC_AMOUNT { set; get; }
        public string HIT_NET_AMOUNT { set; get; }
        public string HIT_DR_CODE { set; get; }
        public string HIT_DR_NAME { set; get; }
        public string HIT_SERV_TYPE { set; get; }
        public string HIT_COVERED { set; get; }
        public string HIT_SLNO { set; get; }
        public string HIT_REFNO { set; get; }
        public string HIT_TEETHNO { set; get; }
        public string HIT_HAAD_CODE { set; get; }
        public string HIT_TYPE_VALUE { set; get; }
        public string HIT_COMMISSION_TYPE { set; get; }
        public string HIT_COSTPRICE { set; get; }
        public string HIT_AUTHORIZATIONID { set; get; }
        public string HIT_STARTDATE { set; get; }
        public string HIT_PHARMACY_CODE { set; get; }
        public string HIT_TREATMENT_ID { set; get; }
        public string HIT_RAD_NUMBER { set; get; }
        public string UTIL_EMNUPDATE { set; get; }
        public string HIT_addSrno { set; get; }
        public string HIT_SERVCOST { set; get; }
        public string HIT_ADJ_DEDUCT_AMOUNT { set; get; }
        public string HIT_TOPUP_AMT { set; get; }
        public string HIT_CLAIM_AMOUNT_PAID { set; get; }
        public string HIT_CLAIM_REJECTED { set; get; }
        public string HIT_CLAIM_BALANCE { set; get; }
        public string HIT_DENIAL_CODE { set; get; }
        public string HIT_DENIAL_DESC { set; get; }
        public string HIT_TRANS_DATE { set; get; }


        public string HIO_BRANCH_ID { set; get; }
        public string HIO_INVOICE_ID { set; get; }
        public string HIO_SERV_CODE { set; get; }
        public string HIO_TYPE { set; get; }
        public string HIO_CODE { set; get; }
        public string HIO_SERV_SLNO { set; get; }
        public string HIO_DESCRIPTION { set; get; }
        public string HIO_VALUE { set; get; }
        public string HIO_VALUETYPE { set; get; }


        public string HPB_BRANCH_ID { set; get; }
        public string HPB_TRANS_ID { set; get; }
        public string HPB_DATE { set; get; }
        public string HPB_PT_ID { set; get; }
        public string HPB_PATIENT_TYPE { set; get; }
        public string HPB_INV_AMOUNT { set; get; }
        public string HPB_PAID_AMT { set; get; }
        public string HPB_REJECTED_AMT { set; get; }
        public string HPB_BALANCE_AMT { set; get; }



        public string HCB_BRANCH_ID { set; get; }
        public string HCB_COMP_TYPE { set; get; }
        public string HCB_COMP_ID { set; get; }
        public string HCB_TRANS_ID { set; get; }
        public string HCB_TRANS_TYPE { set; get; }
        public string HCB_DATE { set; get; }
        public string HCB_AMT { set; get; }
        public string HCB_PAID_AMT { set; get; }
        public string HCB_REJECT_AMT { set; get; }
        public string HCB_BALANCE_AMT { set; get; }

    

        public DataSet DrSalesOrderTransGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_DrSalesOrderTransGet", Param);
            return DS;

        }


        public DataSet CCTypeGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT * FROM  HMS_CC_TYPE WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }


        public DataSet InvoiceMasterGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_InvoiceMasterGet", Param);
            return DS;

        }


        public DataSet IPInvoiceTransGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_InvoiceTransGet", Param);
            return DS;

        }

        public string IPInvoiceMasterAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("HIM_BRANCH_ID", HIM_BRANCH_ID);
            Param.Add("HIM_INVOICE_ID", HIM_INVOICE_ID);
            Param.Add("HIM_INVOICE_TYPE", HIM_INVOICE_TYPE);
            Param.Add("HIM_DATE", HIM_DATE);
            Param.Add("HIM_STATUS", HIM_STATUS);

            Param.Add("HIM_TOKEN_NO", HIM_TOKEN_NO);
            Param.Add("HIM_PT_ID", HIM_PT_ID);
            Param.Add("HIM_PT_NAME", HIM_PT_NAME);
            Param.Add("HIM_BILLTOCODE", HIM_BILLTOCODE);
            Param.Add("HIM_SUB_INS_CODE", HIM_SUB_INS_CODE);
            Param.Add("HIM_INS_NAME", HIM_INS_NAME);
            Param.Add("HIM_SUB_INS_NAME", HIM_SUB_INS_NAME);
            Param.Add("HIM_PT_COMP", HIM_PT_COMP);
            Param.Add("HIM_POLICY_NO", HIM_POLICY_NO);
            Param.Add("HIM_ID_NO", HIM_ID_NO);
            Param.Add("HIM_POLICY_EXP", HIM_POLICY_EXP);
            Param.Add("HIM_INS_TRTNT_TYPE", HIM_INS_TRTNT_TYPE);
            Param.Add("HIM_DR_CODE", HIM_DR_CODE);
            Param.Add("HIM_DR_NAME", HIM_DR_NAME);

            Param.Add("HIM_ORDERING_DR_CODE", HIM_ORDERING_DR_CODE);
            Param.Add("HIM_ORDERING_DR_NAME", HIM_ORDERING_DR_NAME);

            Param.Add("HIM_TRTNT_TYPE", HIM_TRTNT_TYPE);
            Param.Add("HIM_REF_DR_CODE", HIM_REF_DR_CODE);
            Param.Add("HIM_REF_DR_NAME", HIM_REF_DR_NAME);
            Param.Add("HIM_GROSS_TOTAL", HIM_GROSS_TOTAL);
            Param.Add("HIM_HOSP_DISC_TYPE", HIM_HOSP_DISC_TYPE);
            Param.Add("HIM_HOSP_DISC", HIM_HOSP_DISC);
            Param.Add("HIM_HOSP_DISC_AMT", HIM_HOSP_DISC_AMT);
            Param.Add("HIM_CO_INS_TYPE", HIM_CO_INS_TYPE);
            Param.Add("HIM_CO_INS_AMOUNT", HIM_CO_INS_AMOUNT);
            Param.Add("HIM_DEDUCTIBLE", HIM_DEDUCTIBLE);
            Param.Add("HIM_NET_AMOUNT", HIM_NET_AMOUNT);
            Param.Add("HIM_CLAIM_AMOUNT", HIM_CLAIM_AMOUNT);
            Param.Add("HIM_CLAIM_AMOUNT_PAID", HIM_CLAIM_AMOUNT_PAID);
            Param.Add("HIM_CLAIM_REJECTED", HIM_CLAIM_REJECTED);
            Param.Add("HIM_PT_AMOUNT", HIM_PT_AMOUNT);
            Param.Add("HIM_PT_CREDIT", HIM_PT_CREDIT);
            Param.Add("HIM_PT_CREDIT_PAID", HIM_PT_CREDIT_PAID);
            Param.Add("HIM_SPL_DISC", HIM_SPL_DISC);
            Param.Add("HIM_PAID_AMOUNT", HIM_PAID_AMOUNT);
            Param.Add("HIM_PAYMENT_TYPE", HIM_PAYMENT_TYPE);
            Param.Add("HIM_CC_TYPE", HIM_CC_TYPE);
            Param.Add("HIM_CC_NAME", HIM_CC_NAME);
            Param.Add("HIM_CC_NO", HIM_CC_NO);
            Param.Add("HIM_REF_NO", HIM_REF_NO);
            Param.Add("HIM_CHEQ_NO", HIM_CHEQ_NO);
            Param.Add("HIM_BANK_NAME", HIM_BANK_NAME);
            Param.Add("HIM_CHEQ_DATE", HIM_CHEQ_DATE);
            Param.Add("HIM_CUR_TYP", HIM_CUR_TYP);
            Param.Add("HIM_CUR_RATE", HIM_CUR_RATE);
            Param.Add("HIM_CUR_VALUE", HIM_CUR_VALUE);
            Param.Add("HIM_RECEIPT_NO", HIM_RECEIPT_NO);
            Param.Add("HIM_COMMIT_FLAG", HIM_COMMIT_FLAG);




            Param.Add("HIM_VOUCHER_NO", HIM_VOUCHER_NO);
            Param.Add("HIM_CASH_AMT", HIM_CASH_AMT);
            Param.Add("HIM_CC_AMT", HIM_CC_AMT);
            Param.Add("HIM_CHEQUE_AMT", HIM_CHEQUE_AMT);
            Param.Add("HIM_CUR_RCVD_AMT", HIM_CUR_RCVD_AMT);
            Param.Add("HIM_POLICY_TYPE", HIM_POLICY_TYPE);
            Param.Add("HIM_REMARKS", HIM_REMARKS);
            Param.Add("HIM_TYPE", HIM_TYPE);
            Param.Add("HIM_COMMISSION_TYPE", HIM_COMMISSION_TYPE);
            Param.Add("HIM_LAB_DONE", HIM_LAB_DONE);
            Param.Add("HIM_RAD_DONE", HIM_RAD_DONE);
            Param.Add("HIM_JOUR_NO", HIM_JOUR_NO);
            Param.Add("HIM_CMP_INVOICE_NO", HIM_CMP_INVOICE_NO);
            Param.Add("HIM_MERGEINVOICES", HIM_MERGEINVOICES);
            Param.Add("HIM_MERGEREMARKS", HIM_MERGEREMARKS);
            Param.Add("HIM_ADV_AMT", HIM_ADV_AMT);
            Param.Add("HPV_BUSINESSUNITCODE", HPV_BUSINESSUNITCODE);
            Param.Add("HPV_JOBNUMBER", HPV_JOBNUMBER);
            Param.Add("HIM_ADMN_NO", HIM_ADMN_NO);
            Param.Add("HIM_INVOICE_ORDER", HIM_INVOICE_ORDER);
            Param.Add("HIM_MASTER_INVOICE_NUMBER", HIM_MASTER_INVOICE_NUMBER);
            Param.Add("HIM_ADVANCEUTILISED", HIM_ADVANCEUTILISED);
            Param.Add("PHASECODE", PHASECODE);
            Param.Add("HIM_TRTNT_TYPE_01", HIM_TRTNT_TYPE_01);
            Param.Add("HIM_TRTNT_Type_code", HIM_TRTNT_Type_code);
            Param.Add("HIM_SERVCOST_TOTAL", HIM_SERVCOST_TOTAL);
            Param.Add("HIM_READYFORECLAIM", HIM_READYFORECLAIM);
            Param.Add("HIM_EMR_ID", HIM_EMR_ID);
            Param.Add("HIM_AUTHID", HIM_AUTHID);
            Param.Add("HIM_CC", HIM_CC);
            Param.Add("HIM_RCPT_REMARK", HIM_RCPT_REMARK);
            Param.Add("HIM_TOPUP", HIM_TOPUP);
            Param.Add("HIM_TOPUP_AMT", HIM_TOPUP_AMT);
            Param.Add("HIM_ADNL_REMARK", HIM_ADNL_REMARK);
            Param.Add("NewFlag", NewFlag);
            Param.Add("IsTopupInv", IsTopupInv);
            Param.Add("UserId", UserID);

            string strInvoiceID;
            strInvoiceID = objDB.ExecuteNonQueryReturn("HMS_SP_IP_InvoiceMasterAdd", Param);
            return strInvoiceID;

        }


        public void InvoiceMasterDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HIM_BRANCH_ID", HIM_BRANCH_ID);
            Param.Add("HIM_INVOICE_ID", HIM_INVOICE_ID);
            Param.Add("HIM_RECEIPT_NO", HIM_RECEIPT_NO);

            objDB.ExecuteNonQuery("HMS_SP_InvoiceMasterDelete", Param);

        }
        public void IPInvoiceTransAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HIT_BRANCH_ID", HIT_BRANCH_ID);
            Param.Add("HIT_INVOICE_ID", HIT_INVOICE_ID);
            Param.Add("HIT_CAT_ID", HIT_CAT_ID);
            Param.Add("HIT_SERV_CODE", HIT_SERV_CODE);
            Param.Add("HIT_DESCRIPTION", HIT_DESCRIPTION);
            Param.Add("HIT_FEE", HIT_FEE);
            Param.Add("HIT_DISC_TYPE", HIT_DISC_TYPE);
            Param.Add("HIT_DISC_AMT", HIT_DISC_AMT);
            Param.Add("HIT_QTY", HIT_QTY);
            Param.Add("HIT_AMOUNT", HIT_AMOUNT);
            Param.Add("HIT_DEDUCT", HIT_DEDUCT);
            Param.Add("HIT_CO_INS_TYPE", HIT_CO_INS_TYPE);
            Param.Add("HIT_CO_INS_AMT", HIT_CO_INS_AMT);
            Param.Add("HIT_CO_TOTAL", HIT_CO_TOTAL);
            Param.Add("HIT_COMP_TYPE", HIT_COMP_TYPE);
            Param.Add("HIT_COMP_AMT", HIT_COMP_AMT);
            Param.Add("HIT_COMP_TOTAL", HIT_COMP_TOTAL);

            Param.Add("HIT_DR_CODE", HIT_DR_CODE);
            Param.Add("HIT_DR_NAME", HIT_DR_NAME);
            Param.Add("HIT_SERV_TYPE", HIT_SERV_TYPE);
            Param.Add("HIT_COVERED", HIT_COVERED);
            Param.Add("HIT_SLNO", HIT_SLNO);
            Param.Add("HIT_REFNO", HIT_REFNO);
            Param.Add("HIT_TEETHNO", HIT_TEETHNO);
            Param.Add("HIT_HAAD_CODE", HIT_HAAD_CODE);
            Param.Add("HIT_TYPE_VALUE", HIT_TYPE_VALUE);
            Param.Add("HIT_COMMISSION_TYPE", HIT_COMMISSION_TYPE);
            Param.Add("HIT_COSTPRICE", HIT_COSTPRICE);
            Param.Add("HIT_AUTHORIZATIONID", HIT_AUTHORIZATIONID);
            Param.Add("HIT_STARTDATE", HIT_STARTDATE);

            Param.Add("HIT_SERVCOST", HIT_SERVCOST);
            Param.Add("HIT_ADJ_DEDUCT_AMOUNT", HIT_ADJ_DEDUCT_AMOUNT);
            Param.Add("HIT_TOPUP_AMT", HIT_TOPUP_AMT);
            Param.Add("HIT_CLAIM_BALANCE", HIT_CLAIM_BALANCE);
            Param.Add("HIT_TRANS_DATE", HIT_TRANS_DATE);

            objDB.ExecuteNonQuery("HMS_SP_IP_InvoiceTransAdd", Param);

        }

        public void InvoiceObserAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HIO_BRANCH_ID", HIO_BRANCH_ID);
            Param.Add("HIO_INVOICE_ID", HIO_INVOICE_ID);
            Param.Add("HIO_SERV_CODE", HIO_SERV_CODE);
            Param.Add("HIO_TYPE", HIO_TYPE);
            Param.Add("HIO_CODE", HIO_CODE);
            Param.Add("HIO_SERV_SLNO", HIO_SERV_SLNO);
            Param.Add("HIO_DESCRIPTION", HIO_DESCRIPTION);
            Param.Add("HIO_VALUE", HIO_VALUE);
            Param.Add("HIO_VALUETYPE", HIO_VALUETYPE);


            objDB.ExecuteNonQuery("HMS_SP_InvoiceObserAdd", Param);

        }

        public void PatientBalHisAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HPB_BRANCH_ID", HPB_BRANCH_ID);
            Param.Add("HPB_TRANS_ID", HPB_TRANS_ID);
            Param.Add("HPB_DATE", HPB_DATE);
            Param.Add("HPB_PT_ID", HPB_PT_ID);
            Param.Add("HPB_PATIENT_TYPE", HPB_PATIENT_TYPE);
            Param.Add("HPB_INV_AMOUNT", HPB_INV_AMOUNT);
            Param.Add("HPB_PAID_AMT", HPB_PAID_AMT);
            Param.Add("HPB_REJECTED_AMT", HPB_REJECTED_AMT);
            Param.Add("HPB_BALANCE_AMT", HPB_BALANCE_AMT);
            Param.Add("UserID", UserID);


            objDB.ExecuteNonQuery("HMS_SP_PatientBalHisAdd", Param);

        }

        public void CompanytBalHisAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HCB_BRANCH_ID", HCB_BRANCH_ID);
            Param.Add("HCB_COMP_TYPE", HCB_COMP_TYPE);
            Param.Add("HCB_COMP_ID", HCB_COMP_ID);
            Param.Add("HCB_TRANS_ID", HCB_TRANS_ID);
            Param.Add("HCB_DATE", HCB_DATE);
            Param.Add("HCB_AMT", HCB_AMT);
            Param.Add("HCB_PAID_AMT", HCB_PAID_AMT);
            Param.Add("HCB_REJECT_AMT", HCB_REJECT_AMT);
            Param.Add("HCB_BALANCE_AMT", HCB_BALANCE_AMT);
            Param.Add("UserID", UserID);


            objDB.ExecuteNonQuery("HMS_SP_CompanytBalHisAdd", Param);

        }

        public void InvoiceMasterEditAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HIM_BRANCH_ID", HIM_BRANCH_ID);
            Param.Add("HIM_INVOICE_ID", HIM_INVOICE_ID);

            objDB.ExecuteNonQuery("HMS_SP_InvoiceMasterEditAdd", Param);

        }

        public void InvoiceTransEditAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HIT_BRANCH_ID", HIT_BRANCH_ID);
            Param.Add("HIT_INVOICE_ID", HIT_INVOICE_ID);

            objDB.ExecuteNonQuery("HMS_SP_InvoiceTransEditAdd", Param);

        }


        public DataSet CalculatePTAmount(string AmountType, string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL = "";

            switch (AmountType)
            {
                case "INVOICE":
                    strSQL = "SELECT SUM(HPB_INV_AMOUNT) AS PTAmount FROM HMS_PATIENT_BALANCE_HISTORY WHERE " + Criteria + " AND HPB_TRANS_TYPE='INVOICE'";
                    break;
                case "RETURN":
                    strSQL = "SELECT SUM(HPB_INV_AMOUNT)AS PTAmount FROM HMS_PATIENT_BALANCE_HISTORY WHERE " + Criteria + " AND HPB_TRANS_TYPE='INV_RTN'";
                    break;
                case "PAIDAMT":
                    strSQL = "SELECT SUM(HPB_PAID_AMT)AS PTAmount FROM HMS_PATIENT_BALANCE_HISTORY WHERE " + Criteria + " AND HPB_TRANS_TYPE='INVOICE'";
                    break;
                case "RETURNPAIDAMT":
                    strSQL = "SELECT SUM(HPB_PAID_AMT)AS PTAmount FROM HMS_PATIENT_BALANCE_HISTORY WHERE " + Criteria + " AND HPB_TRANS_TYPE='INV_RTN'";
                    break;
                case "REJECTED":
                    strSQL = "SELECT SUM(HPB_REJECTED_AMT)AS PTAmount FROM HMS_PATIENT_BALANCE_HISTORY WHERE " + Criteria + " AND HPB_TRANS_TYPE='INVOICE'";
                    break;
                case "ADVANCE":
                    strSQL = "SELECT SUM(HPB_PAID_AMT)AS PTAmount FROM HMS_PATIENT_BALANCE_HISTORY WHERE " + Criteria + " AND HPB_TRANS_TYPE='ADVANCE'";
                    break;
                case "NPPAID":
                    strSQL = "SELECT SUM(HIT_AMOUNT)AS PTAmount  FROM HMS_INVOICE_MASTER INNER JOIN HMS_INVOICE_TRANSACTION ON HIM_INVOICE_ID=HIT_INVOICE_ID WHERE " + Criteria + " AND hit_serv_code='2'";
                    break;
                default:
                    break;
            }

            DS = objDB.ExecuteReader(strSQL);

            return DS;
        }


        public DataSet AdvanceDtlsGet(string BranchID, string FileNo)
        {
            DataSet DS = new DataSet();
            string strSQL = "";


            strSQL = "SELECT HIO_REMARKS,ISNULL((SELECT SUM(HIO_ADVANCE_AMT-HIO_ADV_USED) ADVANCEAMOUNT FROM HMS_INVOICE_OTHERS WHERE HIO_BRANCH_ID ='" + BranchID + "'  AND hio_invoice_id=a.hio_adv_invoice_id),0) as Used,isnull(hio_advance_amt,0) as AdvAmt from hms_invoice_others a where hio_branch_id='" + BranchID + "' and hio_invoice_id='" + FileNo + "' AND (hio_type='Invoice' or hio_type='Refund')";


            DS = objDB.ExecuteReader(strSQL);

            return DS;
        }

        public DataSet InvoiceAdvanceDtlsGet(string BranchID, string FileNo, string InvoiceID)
        {
            DataSet DS = new DataSet();
            string strSQL = "";


            strSQL = "select HIM_INVOICE_ID,convert(varchar(10),him_date,103) as him_date,hio_advance_amt-hio_adv_used as AdvAmt from   HMS_INVOICE_MASTER inner join hms_invoice_others on him_invoice_id=hio_invoice_id where him_pt_id='" + FileNo + "' and (hio_type='Advance' or hio_type='Refund') and hio_advance_amt-hio_adv_used>0 and hio_invoice_id not in (select hio_adv_invoice_id from hms_invoice_others where hio_branch_id='" + BranchID + "' and hio_invoice_id='" + InvoiceID + "') order by him_date";


            DS = objDB.ExecuteReader(strSQL);

            return DS;
        }


        public DataSet GetLastCompanyInvoiceNo(string branchid, string compid)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", branchid);
            Param.Add("compid", compid);

            DS = objDB.ExecuteReader("sp_GetLastCompanyInvoiceNo", Param);
            return DS;

        }


        public DataSet InvoiceOtherGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL = "";


            strSQL = "SELECT * FROM HMS_INVOICE_OTHERS WHERE  " + Criteria;


            DS = objDB.ExecuteReader(strSQL);

            return DS;
        }

        public void InvoiceOtherUpdateAdvAmt(decimal AdvanceAmt, string Criteria)
        {

            string strSQL = "";

            strSQL = "UPDATE HMS_INVOICE_OTHERS SET HIO_ADV_USED=HIO_ADV_USED- " + AdvanceAmt + " where " + Criteria;

            objDB.ExecuteNonQuery(strSQL);


        }


        public void PatientVIsitUpdateInvoiceAmt(string Criteria)
        {

            string strSQL = "";


            strSQL = " UPDATE HMS_PATIENT_VISIT SET HPV_INV_AMT=HPV_INV_AMT-" + HIM_GROSS_TOTAL + " - " + HIM_HOSP_DISC_AMT + ",HPV_PT_AMT=HPV_PT_AMT-" + HIM_PT_AMOUNT + ",HPV_COMP_AMT=HPV_COMP_AMT-" + HIM_CLAIM_AMOUNT + ",HPV_PAID_AMT=HPV_PAID_AMT-" + HIM_PAID_AMOUNT + ",HPV_NPAY_AMT=HPV_NPAY_AMT-" + HIM_PT_CREDIT + " WHERE " + Criteria;


            objDB.ExecuteNonQuery(strSQL);

        }


        public void CompanyBalHistUpdate(string Criteria)
        {

            string strSQL = "";

            strSQL = "UPDATE HMS_COMPANY_BALANCE_HISTORY SET HCB_AMT=0  WHERE " + Criteria;

            objDB.ExecuteNonQuery(strSQL);


        }


        public DataSet ReVisitTypeGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL = "";


            strSQL = "SELECT    HIM_DATE, HIT_AMOUNT, HIT_SERV_TYPE,HIM_INVOICE_ID, CONVERT(VARCHAR,HIM_DATE,103)as HIM_DATEDesc FROM    HMS_INVOICE_MASTER     INNER JOIN HMS_INVOICE_TRANSACTION   ON  HIM_INVOICE_ID =  HIT_INVOICE_ID  AND  HIM_BRANCH_ID = HIT_BRANCH_ID  WHERE  " + Criteria + " ORDER BY HIM_DATE desc  ";


            DS = objDB.ExecuteReader(strSQL);

            return DS;
        }


        public DataSet GetLabValues(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL = "";


            strSQL = "SELECT     dbo.LAB_TEST_REPORT_DETAIL.LTRD_TEST_RESULT  FROM   " +
                         " dbo.LAB_TEST_REPORT_DETAIL INNER JOIN   dbo.LAB_TEST_REPORT_MASTER ON  " +
                         " dbo.LAB_TEST_REPORT_DETAIL.LTRD_BRANCH_ID = dbo.LAB_TEST_REPORT_MASTER.LTRM_BRANCH_ID " +
                         "  AND dbo.LAB_TEST_REPORT_DETAIL.LTRD_TEST_REPORT_ID = dbo.LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID" +
                         " INNER JOIN dbo.LAB_TEST_PROFILE_MASTER ON dbo.LAB_TEST_REPORT_DETAIL.LTRD_BRANCH_ID = dbo.LAB_TEST_PROFILE_MASTER.LTPM_BRANCH_ID" +
                         " AND dbo.LAB_TEST_REPORT_DETAIL.LTRD_TEST_PROFILE_ID = dbo.LAB_TEST_PROFILE_MASTER.LTPM_TEST_PROFILE_ID " +
                        " WHERE  " + Criteria;


            DS = objDB.ExecuteReader(strSQL);

            return DS;
        }



        public DataSet InvoiceTransResubGet(string Criteria, string ResubNo)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            Param.Add("ResubNo", ResubNo);

            DS = objDB.ExecuteReader("HMS_SP_InvoiceTransResubGet", Param);
            return DS;

        }

        public DataSet  IPSalesOrderTransGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_SalesOrderTransGet", Param);
            return DS;

        }

    }
}
