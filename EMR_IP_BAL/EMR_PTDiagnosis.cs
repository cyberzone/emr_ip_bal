﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace EMR_IP_BAL
{
   public  class EMR_PTDiagnosis
    {

        dbOperation objDB = new dbOperation();

        public string BRANCH_ID { set; get; }
        public string EPD_ID { set; get; }
        public string EPD_DIAG_CODE { set; get; }
        public string EPD_DIAG_NAME { set; get; }

        public string EPD_REMARKS { set; get; }
        public string EPD_TEMPLATE_CODE { set; get; }
        public string EPD_PROBLEM_TYPE { set; get; }
        public string EPD_POSITION { set; get; }
        public string EPD_TYPE { set; get; }

        public DataSet DiagnosisGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_DiagnosisGet", Param);

            return DS;


        }


        public void DiagnosisAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPD_BRANCH_ID", BRANCH_ID);
            Param.Add("EPD_ID", EPD_ID);
            Param.Add("EPD_DIAG_CODE", EPD_DIAG_CODE);
            Param.Add("EPD_DIAG_NAME", EPD_DIAG_NAME);
            Param.Add("EPD_REMARKS", EPD_REMARKS);
            Param.Add("EPD_TEMPLATE_CODE", EPD_TEMPLATE_CODE);
            Param.Add("EPD_PROBLEM_TYPE", EPD_PROBLEM_TYPE);
            Param.Add("EPD_TYPE", EPD_TYPE);
            Param.Add("EPD_POSITION", EPD_POSITION);


            objDB.ExecuteNonQuery("HMS_SP_EMR_DiagnosisAdd", Param);

        }


        public void DiagnosisDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPD_BRANCH_ID", BRANCH_ID);
            Param.Add("EPD_ID", EPD_ID);
            Param.Add("EPD_DIAG_CODE", EPD_DIAG_CODE);

            objDB.ExecuteNonQuery("HMS_SP_EMR_DiagnosisDelete", Param);

        }
    }
}
