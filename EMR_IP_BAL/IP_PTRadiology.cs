﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_PTRadiology
    {

         dbOperation objDB = new dbOperation();

        public string BRANCH_ID { set; get; }
        public string IPR_ID { set; get; }
         public string IPR_RAD_ID { set; get; }
        public string IPR_RAD_CODE { set; get; }
        public string IPR_RAD_NAME { set; get; }
        public string IPR_REMARKS { set; get; }
        public string IPR_QTY { set; get; }
        public string IPR_TEMPLATE_CODE { set; get; }
        public string IPR_SO_TRANS_ID { set; get; }

        public DataSet IPRadiologyGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTRadiologyGet", Param);

            return DS;


        }


        public void IPRadiologyAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPR_BRANCH_ID", BRANCH_ID);
            Param.Add("IPR_ID", IPR_ID);
            Param.Add("IPR_RAD_ID", IPR_RAD_ID);
            Param.Add("IPR_RAD_CODE", IPR_RAD_CODE);
            Param.Add("IPR_RAD_NAME", IPR_RAD_NAME);
            Param.Add("IPR_REMARKS", IPR_REMARKS);

            Param.Add("IPR_TEMPLATE_CODE", IPR_TEMPLATE_CODE);
            Param.Add("IPR_SO_TRANS_ID", IPR_SO_TRANS_ID);
            objDB.ExecuteNonQuery("HMS_SP_IP_PTRadiologyAdd", Param);

        }




        public void IPRadiologyDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPR_BRANCH_ID", BRANCH_ID);
            Param.Add("IPR_ID", IPR_ID);
            Param.Add("IPR_RAD_ID", IPR_RAD_ID);
            Param.Add("IPR_SO_TRANS_ID", IPR_SO_TRANS_ID);
            objDB.ExecuteNonQuery("HMS_SP_IP_PTRadiologyDelete", Param);

        }
    }
}
