﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace EMR_IP_BAL
{
    public class IP_PTVitalSign
    {


        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string IPV_VIT_ID { set; get; }
       
        public string IPV_DATE { set; get; }
        public string IPV_WEIGHT { set; get; }
        public string IPV_HEIGHT { set; get; }
        public string IPV_BMI { set; get; }
        public string IPV_TEMPERATURE_F { set; get; }
        public string IPV_TEMPERATURE_C { set; get; }
        public string IPV_PULSE { set; get; }
        public string IPV_RESPIRATION { set; get; }
        public string IPV_BP_SYSTOLIC { set; get; }
        public string IPV_BP_DIASTOLIC { set; get; }
        public string IPV_SPO2 { set; get; }




        public string UserId { set; get; }

        public string  PTVitalSignAdd()
        {
            string strReturnId;
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPV_ID", EMRID);
            Param.Add("IPV_BRANCH_ID", BranchID);
            Param.Add("IPV_PT_ID", PTID);
            Param.Add("IPV_VIT_ID", IPV_VIT_ID);
            Param.Add("IPV_DATE", IPV_DATE);
            Param.Add("IPV_WEIGHT", IPV_WEIGHT);
            Param.Add("IPV_HEIGHT", IPV_HEIGHT);
            Param.Add("IPV_BMI", IPV_BMI);
            Param.Add("IPV_TEMPERATURE_F", IPV_TEMPERATURE_F);
            Param.Add("IPV_TEMPERATURE_C", IPV_TEMPERATURE_C);
            Param.Add("IPV_PULSE", IPV_PULSE);
            Param.Add("IPV_RESPIRATION", IPV_RESPIRATION);
            Param.Add("IPV_BP_SYSTOLIC", IPV_BP_SYSTOLIC);
            Param.Add("IPV_BP_DIASTOLIC", IPV_BP_DIASTOLIC);
            Param.Add("IPV_SPO2", IPV_SPO2);

            Param.Add("UserId", UserId);

            strReturnId = objDB.ExecuteNonQuery("HMS_SP_IP_PTVitalSignAdd", Param);
            return strReturnId;

        }

        public DataSet  PTVitalSignGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTVitalSignGet", Param);
            return (DS);


        }

        public void PTVitalSignDelete()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPV_VIT_ID", IPV_VIT_ID);
            Param.Add("UserId", UserId);

            objDB.ExecuteNonQuery("HMS_SP_IP_PTVitalSignDelete", Param);

        }
    }
}
