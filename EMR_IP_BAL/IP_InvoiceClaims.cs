﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_InvoiceClaims
    {


        dbOperation objDB = new dbOperation();


        public string HIC_BRANCH_ID { set; get; }
        public string HIC_INVOICE_ID { set; get; }
        public string HIC_TYPE { set; get; }
        public string HIC_STARTDATE { set; get; }
        public string HIC_ENDDATE { set; get; }
        public string HIC_STARTTYPE { set; get; }
        public string HIC_ENDTYPE { set; get; }
        public string HIC_ICD_CODE_P { set; get; }
        public string HIC_ICD_CODE_A { set; get; }
        public string HIC_ICD_CODE_S1 { set; get; }
        public string HIC_ICD_CODE_S2 { set; get; }
        public string HIC_ICD_CODE_S3 { set; get; }
        public string HIC_ICD_CODE_S4 { set; get; }
        public string HIC_ICD_CODE_S5 { set; get; }
        public string HIC_ICD_CODE_S6 { set; get; }
        public string HIC_ICD_CODE_S7 { set; get; }
        public string HIC_ICD_CODE_S8 { set; get; }
        public string HIC_ICD_CODE_S11 { set; get; }
        public string HIC_ICD_CODE_S12 { set; get; }
        public string HIC_ICD_CODE_S13 { set; get; }
        public string HIC_ICD_CODE_S14 { set; get; }
        public string HIC_ICD_CODE_S15 { set; get; }
        public string HIC_ICD_CODE_S16 { set; get; }
        public string HIC_ICD_CODE_S17 { set; get; }
        public string HIC_ICD_CODE_S18 { set; get; }
        public string HIC_ICD_CODE_S19 { set; get; }
        public string HIC_ICD_CODE_S20 { set; get; }
        public string HIC_ICD_CODE_S21 { set; get; }
        public string HIC_ICD_CODE_S22 { set; get; }
        public string HIC_ICD_CODE_S23 { set; get; }
        public string HIC_ICD_CODE_S24 { set; get; }
        public string HIC_ICD_CODE_S25 { set; get; }
        public string HIC_ICD_CODE_S26 { set; get; }
        public string HIC_ICD_CODE_S27 { set; get; }
        public string HIC_ICD_CODE_S28 { set; get; }


        public DataSet InvoiceClaimDtlsGet(string Criteria)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();


            Param.Add("Criteria", Criteria);

            DS = objDB.ExecuteReader("HMS_SP_InvoiceClaimDtlsGet", Param);



            return DS;

        }

        public void InvoiceClaimDtlsAdd()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HIC_BRANCH_ID", HIC_BRANCH_ID);
            Param.Add("HIC_INVOICE_ID", HIC_INVOICE_ID);
            Param.Add("HIC_TYPE", HIC_TYPE);
            Param.Add("HIC_STARTDATE", HIC_STARTDATE);
            Param.Add("HIC_ENDDATE", HIC_ENDDATE);
            Param.Add("HIC_STARTTYPE", HIC_STARTTYPE);
            Param.Add("HIC_ENDTYPE", HIC_ENDTYPE);
            Param.Add("HIC_ICD_CODE_P", HIC_ICD_CODE_P);
            Param.Add("HIC_ICD_CODE_A", HIC_ICD_CODE_A);
            Param.Add("HIC_ICD_CODE_S1", HIC_ICD_CODE_S1);
            Param.Add("HIC_ICD_CODE_S2", HIC_ICD_CODE_S2);
            Param.Add("HIC_ICD_CODE_S3", HIC_ICD_CODE_S3);
            Param.Add("HIC_ICD_CODE_S4", HIC_ICD_CODE_S4);
            Param.Add("HIC_ICD_CODE_S5", HIC_ICD_CODE_S5);
            Param.Add("HIC_ICD_CODE_S6", HIC_ICD_CODE_S6);
            Param.Add("HIC_ICD_CODE_S7", HIC_ICD_CODE_S7);
            Param.Add("HIC_ICD_CODE_S8", HIC_ICD_CODE_S8);
            Param.Add("HIC_ICD_CODE_S11", HIC_ICD_CODE_S11);
            Param.Add("HIC_ICD_CODE_S12", HIC_ICD_CODE_S12);
            Param.Add("HIC_ICD_CODE_S13", HIC_ICD_CODE_S13);
            Param.Add("HIC_ICD_CODE_S14", HIC_ICD_CODE_S14);
            Param.Add("HIC_ICD_CODE_S15", HIC_ICD_CODE_S15);
            Param.Add("HIC_ICD_CODE_S16", HIC_ICD_CODE_S16);
            Param.Add("HIC_ICD_CODE_S17", HIC_ICD_CODE_S17);
            Param.Add("HIC_ICD_CODE_S18", HIC_ICD_CODE_S18);
            Param.Add("HIC_ICD_CODE_S19", HIC_ICD_CODE_S19);
            Param.Add("HIC_ICD_CODE_S20", HIC_ICD_CODE_S20);
            Param.Add("HIC_ICD_CODE_S21", HIC_ICD_CODE_S21);
            Param.Add("HIC_ICD_CODE_S22", HIC_ICD_CODE_S22);
            Param.Add("HIC_ICD_CODE_S23", HIC_ICD_CODE_S23);
            Param.Add("HIC_ICD_CODE_S24", HIC_ICD_CODE_S24);
            Param.Add("HIC_ICD_CODE_S25", HIC_ICD_CODE_S25);
            Param.Add("HIC_ICD_CODE_S26", HIC_ICD_CODE_S26);
            Param.Add("HIC_ICD_CODE_S27", HIC_ICD_CODE_S27);
            Param.Add("HIC_ICD_CODE_S28", HIC_ICD_CODE_S28);



            objDB.ExecuteNonQuery("HMS_SP_InvoiceClaimDtlsAdd", Param);

        }

    }
}
