﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace EMR_IP_BAL
{
    public class IP_AdmissionSummary
    {
        dbOperation objDB = new dbOperation();
        public string BranchID { get; set; }
        public string AdmissionID { get; set; }
        public string PatientName { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string InsuranceCompanyName { get; set; }
        // public string AdmissionNO{ get; set; }
        public string Nationality { get; set; }
        public string FileNumber { get; set; }
        public string Admission { get; set; }
        public string AdmissionDate { get; set; }
        public string ProvisionalDiagnosis { get; set; }
        public string AttendingPhysician { get; set; }
        public string Laboratory { get; set; }
        public string Radiological { get; set; }
        public string Others { get; set; }
        public string ProcedurePlanned { get; set; }
        public string Anesthesia { get; set; }
        public string Treatment { get; set; }
        public string ReferralPhysician { get; set; }
        public string AdmittingPhysician { get; set; }
        public string ReasonForAdmission { get; set; }
        public string AdmissionType { get; set; }
        public string Remarks { get; set; }
        public string PatientMasterID { get; set; }
        public string DoctorID { get; set; }
        public string DoctorName { get; set; }
        public string Department { get; set; }
        public string Status { get; set; }
        public string InpatientNO { get; set; }
        public string AdmissionDateTime { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string MobileNo { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string BloodGroup { get; set; }
        public string PatientType { get; set; }
        public string PatientCompanyName { get; set; }
        public string InsuranceCompanyID { get; set; }
        public string InsuranceCompanyType { get; set; }
        public string CompanyCashPatient { get; set; }
        public string InsuranceTypeTreatment { get; set; }
        public string PolicyNo { get; set; }
        public string PolicyType { get; set; }
        public string ContExPDate { get; set; }
        public string Address { get; set; }
        public string POBox { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string OfficePhone { get; set; }
        public string ResPhone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string IDCardNO { get; set; }
        public string DOB { get; set; }
        public string Marital { get; set; }
        public string ModifiedUser { get; set; }
        public string ModifiedDate { get; set; }
        public string CreatedUser { get; set; }
        public string CreatedDate { get; set; }
        public string DedType { get; set; }
        public string DedAmount { get; set; }
        public string CoinsType { get; set; }
        public string CoinsAmount { get; set; }
        public string PackageAmount { get; set; }
        public string AdmissionMode { get; set; }
        public string AdmisisonModeDesc { get; set; }
        public string ReferenceNo { get; set; }
        public string admittedfor { get; set; }
        public string ExistingRecord { get; set; }


        public string cptdrgcode { get; set; }
        public string cptdrgamount { get; set; }
        public string approvalcode { get; set; }
        public string expiraydate { get; set; }
        public string proposeddatetime { get; set; }




        public DataSet GetIPAdmissionSummary(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPAdmissionSummaryGet", Param);
            return DS;

        }

        public void SaveIPAdmissionSummary()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();





            Param.Add("branchid", BranchID);
            Param.Add("admissionid", AdmissionID);
            Param.Add("patientname", PatientName);
            Param.Add("filenumber", FileNumber);



            //Param.Add("inpatientno",InpatientNO);


            Param.Add("admissiondatetime", AdmissionDateTime);
            Param.Add("provisionaldiagnosis", ProvisionalDiagnosis);
            Param.Add("attendingphysician", AttendingPhysician);
            Param.Add("laboratory", Laboratory);
            Param.Add("radiological", Radiological);
            Param.Add("others", Others);
            Param.Add("procedureplanned", ProcedurePlanned);
            Param.Add("anesthesia", Anesthesia);
            Param.Add("treatment", Treatment);
            Param.Add("referralphysician", ReferralPhysician);
            Param.Add("admittingphysician", AdmittingPhysician);
            Param.Add("reasonforadmission", ReasonForAdmission);
            Param.Add("admissiontype", AdmissionType);
            Param.Add("remarks", Remarks);
            Param.Add("patientmasterid", PatientMasterID);
            Param.Add("doctorid", DoctorID);
            Param.Add("status", Status);
            Param.Add("department", Department);
            Param.Add("doctorname", DoctorName);

            Param.Add("patienttype", PatientType);
            Param.Add("middlename", MiddleName);
            Param.Add("lastname", LastName);
            Param.Add("dob", DOB);
            Param.Add("age", Age);
            Param.Add("sex", Sex);
            Param.Add("pobox", POBox);
            Param.Add("address", Address);
            Param.Add("city", City);
            Param.Add("area", Area);
            Param.Add("country", Country);
            Param.Add("nationality", Nationality);
            Param.Add("bloodgroup", BloodGroup);
            Param.Add("marital", Marital);
            Param.Add("residencephone", ResPhone);
            Param.Add("officephone", OfficePhone);
            Param.Add("mobile", Mobile);
            Param.Add("fax", Fax);
            Param.Add("insurancecompanyid", InsuranceCompanyID);
            Param.Add("insurancecompanyname", InsuranceCompanyName);
            Param.Add("policyno", PolicyNo);
            Param.Add("policytype", PolicyType);
            Param.Add("contexpdate", ContExPDate);
            Param.Add("companycashpatient", CompanyCashPatient);
            Param.Add("patientcompanyname", PatientCompanyName);
            Param.Add("deductibletype", DedType);
            Param.Add("deductibleamt", DedAmount);
            Param.Add("coinstype", CoinsType);
            Param.Add("coinsamount", CoinsAmount);
            Param.Add("idcardno", IDCardNO);
            Param.Add("referenceno", ReferenceNo);
            Param.Add("admittedfor", admittedfor);
            Param.Add("admissionmode", AdmissionMode);
            Param.Add("admissionmodedesc", AdmisisonModeDesc);
            Param.Add("modifieduser", ModifiedUser);
            Param.Add("createduser", CreatedUser);
            Param.Add("insurancetrttype", InsuranceTypeTreatment);

            Param.Add("packageamount", PackageAmount);
            Param.Add("cptdrgcode", cptdrgcode);
            Param.Add("cptdrgamount", cptdrgamount);
            Param.Add("approvalcode", approvalcode);
            Param.Add("expiraydate", expiraydate);
            Param.Add("proposeddatetime", proposeddatetime);


            objDB.ExecuteNonQuery("HMS_SP_IPAdmissionSummaryAdd", Param);

        }
    }
}
