﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
    public class EMR_PTReferral
    {
        dbOperation objDB = new dbOperation();

        public string branchid { set; get; }
        public string patientmasterid { set; get; }
        public string referencetype { set; get; }
        public string remarks { set; get; }
        public string type { set; get; }
        public string indoctorid { set; get; }
        public string kinname { set; get; }
        public string kinaddress { set; get; }
        public string kinphone { set; get; }
        public string diagnosis { set; get; }
        public string recommended { set; get; }
        public string managementtransfer { set; get; }
        public string beforetransfer { set; get; }
        public string aftertransfer { set; get; }
        public string templatecode { set; get; }
        public string outhospital { set; get; }
        public string outdoctor { set; get; }

        public DataSet WEMR_spS_GetReferral(string BranchID, string EMRID)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetReferral", Param);



            return DS;

        }

        public void WEMR_spI_SaveReferral()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("branchid", branchid);
            Param.Add("patientmasterid", patientmasterid);
            Param.Add("referencetype", referencetype);
            Param.Add("remarks", remarks);
            Param.Add("type", type);
            Param.Add("indoctorid", indoctorid);
            Param.Add("outhospital", outhospital);
            Param.Add("outdoctor", outdoctor);
            Param.Add("kinname", kinname);
            Param.Add("kinaddress", kinaddress);
            Param.Add("kinphone", kinphone);
            Param.Add("diagnosis", diagnosis);
            Param.Add("recommended", recommended);
            Param.Add("managementtransfer", managementtransfer);
            Param.Add("beforetransfer", beforetransfer);
            Param.Add("aftertransfer", aftertransfer);
            Param.Add("templatecode", templatecode);

            objDB.ExecuteNonQuery("WEMR_spI_SaveReferral", Param);

        }




    }
}
