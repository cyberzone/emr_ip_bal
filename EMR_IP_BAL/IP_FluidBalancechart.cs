﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
    public class IP_FluidBalancechart
    {
        dbOperation objDB = new dbOperation();
        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string IFBC_FLUBAL_ID { set; get; }
        public string IFBC_DATE { set; get; }
        public string IFBC_NATURE_OF_FOOD { set; get; }
        public string IFBC_ROUTE_ORAL { set; get; }
        public string IFBC_ROUTE_IV { set; get; }
        public string IFBC_ROUTE_OTHER { set; get; }
        public string IFBC_URINE { set; get; }
        public string IFBC_VOMIT { set; get; }
        public string IFBC_NG_ASPIRATION { set; get; }
        public string IFBC_OTHER { set; get; }

        public string IFBC_TOTAL_INPUT { set; get; }
        public string IFBC_TOTAL_OUTPUT { set; get; }
        public string IFBC_RESULT { set; get; }

        public string UserID { set; get; }


        public DataSet FluidBalanceChartGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPFluidBalanceChartGet", Param);

            return DS;


        }

        public void FluidBalanceChartAdd()
        {


            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IFBC_BRANCH_ID", BranchID);
            Param.Add("IFBC_ID", EMRID);
            Param.Add("IFBC_PT_ID", PTID);
            Param.Add("IFBC_FLUBAL_ID", IFBC_FLUBAL_ID);

            Param.Add("IFBC_DATE", IFBC_DATE);
            Param.Add("IFBC_NATURE_OF_FOOD", IFBC_NATURE_OF_FOOD);
            Param.Add("IFBC_ROUTE_ORAL", IFBC_ROUTE_ORAL);
            Param.Add("IFBC_ROUTE_IV", IFBC_ROUTE_IV);
            Param.Add("IFBC_ROUTE_OTHER", IFBC_ROUTE_OTHER);

            Param.Add("IFBC_URINE", IFBC_URINE);
            Param.Add("IFBC_VOMIT", IFBC_VOMIT);

            Param.Add("IFBC_NG_ASPIRATION", IFBC_NG_ASPIRATION);
            Param.Add("IFBC_OTHER", IFBC_OTHER);

            Param.Add("IFBC_TOTAL_INPUT", IFBC_TOTAL_INPUT);
            Param.Add("IFBC_TOTAL_OUTPUT", IFBC_TOTAL_OUTPUT);
            Param.Add("IFBC_RESULT", IFBC_RESULT);

            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_IPFluidBalanceChartAdd", Param);

        }
    }
}
