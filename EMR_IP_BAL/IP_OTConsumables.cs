﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
    public class IP_OTConsumables
    {
        dbOperation objDB = new dbOperation();

        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string IPOTC_OT_ID { set; get; }

        public string IPOTC_OT_CODE { set; get; }
        public string IPOTC_OT_NAME { set; get; }
        public string IPOTC_OT_TYPE { set; get; }

        public string IPOTC_QTY { set; get; }
        public string IPOTC_PRICE { set; get; }
        public string IPOTC_SO_TRANS_ID { set; get; }

        public string UserID { set; get; }


        public DataSet OTConsumablesGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_OTConsumablesGet", Param);

            return DS;


        }


        public void OTConsumablesAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BranchID", BranchID);
            Param.Add("EMRID", EMRID);
            Param.Add("PTID", PTID);
            Param.Add("IPOTC_OT_ID", IPOTC_OT_ID);

            Param.Add("IPOTC_OT_CODE", IPOTC_OT_CODE);
            Param.Add("IPOTC_OT_NAME", IPOTC_OT_NAME);
            Param.Add("IPOTC_OT_TYPE", IPOTC_OT_TYPE);

            Param.Add("IPOTC_QTY", IPOTC_QTY);
            Param.Add("IPOTC_PRICE", IPOTC_PRICE);
            Param.Add("IPOTC_SO_TRANS_ID", IPOTC_SO_TRANS_ID);
            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_IP_OTConsumablesAdd", Param);

        }

        public void OTConsumablesDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BranchID", BranchID);
            Param.Add("EMRID", EMRID);
            Param.Add("IPOTC_OT_ID", IPOTC_OT_ID);
            Param.Add("IPOTC_SO_TRANS_ID", IPOTC_SO_TRANS_ID);


            objDB.ExecuteNonQuery("HMS_SP_IP_OTConsumablesDelete", Param);

        }
    }
}
