﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace EMR_IP_BAL
{
    public class IP_ResourceDatas
    {
        dbOperation objDB = new dbOperation();
        public string RD_ID { get; set; }
        public string RD_TYPE { get; set; }
        public string RD_TYPE_ID { get; set; }
        public string RD_DATA { get; set; }
        public string RD_IS_DELETED { get; set; }
        public string UserID { get; set; }

        public DataSet  IPResourceDatasGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPResourceDatasGet", Param);
            return DS;

        }

        public void  IPResourceDatasAdd()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();


            Param.Add("RD_ID", RD_ID);
            Param.Add("RD_TYPE", RD_TYPE);
            Param.Add("RD_TYPE_ID", RD_TYPE_ID);
            Param.Add("RD_DATA", RD_DATA);
            Param.Add("RD_IS_DELETED ", RD_IS_DELETED);
            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_IPResourceDatasAdd", Param);

        }
    }
}
