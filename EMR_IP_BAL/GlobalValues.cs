﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMR_IP_BAL
{
   public  class GlobalValues
    {
        public static string EMR_ID { set; get; }
        public static string EMR_PT_ID { set; get; }
        public static string EMR_PT_SEX = "Male";


        public static string FileDescription = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["FileDescription"].ToString());

        public static string HospitalName = System.Configuration.ConfigurationSettings.AppSettings["HospitalName"].ToString().Trim();

        public static string DB_USERNAME = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["DB_USERNAME"].ToString());
        public static string DB_PASSWORD = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["DB_PASSWORD"].ToString());
        public static string DB_SERVER1 = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["DB_SERVER1"].ToString());
        public static string DB_DATABASE = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["DB_DATABASE"].ToString());
        public static string REPORT_PATH = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["REPORT_PATH"].ToString());


        public static string Hospital_FromMailID = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["Hospital_FromMailID"].ToString());
        public static string Hospital_MailServer = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["Hospital_MailServer"].ToString());
        public static string Hospital_MailUesrName = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["Hospital_MailUesrName"].ToString());
        public static string Hospital_MailPassword = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["Hospital_MailPassword"].ToString());


        public static bool BLNALLOWADVANCEPAYMENT_INVOICE;
        public static string InvoiceType;

        public static string HMS_Path = (string)System.Configuration.ConfigurationSettings.AppSettings["HMS_PATH"];
        public static string EMR_PATH = (string)System.Configuration.ConfigurationSettings.AppSettings["EMR_PATH"];


    }
}
