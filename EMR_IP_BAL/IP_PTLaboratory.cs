﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_PTLaboratory
    {
        dbOperation objDB = new dbOperation();

        public string BRANCH_ID { set; get; }
        public string IPL_ID { set; get; }
        public string IPL_LAB_ID { set; get; }
        public string IPL_LAB_CODE { set; get; }
        public string IPL_LAB_NAME { set; get; }
        public string IPL_COST { set; get; }
        public string IPL_REMARKS { set; get; }
        public string IPL_QTY { set; get; }
        public string IPL_TEMPLATE_CODE { set; get; }

        public string IPL_SO_TRANS_ID { set; get; }

        public DataSet IPLaboratoryGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTLaboratoryGet", Param);

            return DS;


        }


        public void IPLaboratoryAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPL_BRANCH_ID", BRANCH_ID);
            Param.Add("IPL_ID", IPL_ID);
            Param.Add("IPL_LAB_ID", IPL_LAB_ID);
            Param.Add("IPL_LAB_CODE", IPL_LAB_CODE);
            Param.Add("IPL_LAB_NAME", IPL_LAB_NAME);
            Param.Add("IPL_COST", IPL_COST);
            Param.Add("IPL_REMARKS", IPL_REMARKS);
            Param.Add("IPL_QTY", IPL_QTY);
            Param.Add("IPL_TEMPLATE_CODE", IPL_TEMPLATE_CODE);
            Param.Add("IPL_SO_TRANS_ID", IPL_SO_TRANS_ID);
            objDB.ExecuteNonQuery("HMS_SP_IP_PTLaboratoryAdd", Param);

        }




        public void IPLaboratoryDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPL_BRANCH_ID", BRANCH_ID);
            Param.Add("IPL_ID", IPL_ID);
            Param.Add("IPL_LAB_ID", IPL_LAB_ID);
            Param.Add("IPL_SO_TRANS_ID", IPL_SO_TRANS_ID);
            objDB.ExecuteNonQuery("HMS_SP_IP_PTLaboratoryDelete", Param);

        }
    }
}
