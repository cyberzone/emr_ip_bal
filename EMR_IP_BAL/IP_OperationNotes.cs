﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_OperationNotes
    {

        dbOperation objDB = new dbOperation();

        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string ION_DATE { set; get; }
        public string ION_SURGEON_ID { set; get; }
        public string ION_SURGEON_NAME { set; get; }
        public string ION_ASSISTANTS { set; get; }
        public string ION_PREOPERATIVE_DIAGNOSIS { set; get; }
        public string ION_POSTOPERATIVE_DIAGNOSIS { set; get; }
        public string ION_OPERATION_PERFORMED { set; get; }
        public string ION_ANAESTHETIST_ID { set; get; }
        public string ION_ANAESTHETIST_TYPE { set; get; }
        public string ION_OR_NO { set; get; }
        public string ION_THEATRE_SISTERS { set; get; }
        public string ION_PROCEDURE_FINDINGS { set; get; }
        public string ION_SPECIMENTS_PATHOLOGY { set; get; }
        public string ION_SWAB_INSTRUMENT_CHECK { set; get; }
        public string ION_STARTED_TIME { set; get; }
        public string ION_FINISHED_TIME { set; get; }
        public string ION_IV_TRANSFUSION_INFUSION { set; get; }
        public string ION_DRAINS { set; get; }
        public string ION_SEDATION_ANTIBIOTICS_DRUGS { set; get; }
        public string ION_CATHETERS { set; get; }

        public string UserID { set; get; }


        public DataSet OperationNotesGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_OperationNotesGet", Param);

            return DS;


        }


        public void OperationNotesAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BranchID", BranchID);
            Param.Add("EMRID", EMRID);
            Param.Add("PTID", PTID);
            Param.Add("ION_DATE", ION_DATE);
            Param.Add("ION_SURGEON_ID", ION_SURGEON_ID);
            Param.Add("ION_SURGEON_NAME", ION_SURGEON_NAME);
            Param.Add("ION_ASSISTANTS", ION_ASSISTANTS);
            Param.Add("ION_PREOPERATIVE_DIAGNOSIS", ION_PREOPERATIVE_DIAGNOSIS);
            Param.Add("ION_POSTOPERATIVE_DIAGNOSIS", ION_POSTOPERATIVE_DIAGNOSIS);
            Param.Add("ION_OPERATION_PERFORMED", ION_OPERATION_PERFORMED);
            Param.Add("ION_ANAESTHETIST_ID", ION_ANAESTHETIST_ID);
            Param.Add("ION_ANAESTHETIST_TYPE", ION_ANAESTHETIST_TYPE);
            Param.Add("ION_OR_NO", ION_OR_NO);
            Param.Add("ION_THEATRE_SISTERS", ION_THEATRE_SISTERS);
            Param.Add("ION_PROCEDURE_FINDINGS", ION_PROCEDURE_FINDINGS);
            Param.Add("ION_SPECIMENTS_PATHOLOGY", ION_SPECIMENTS_PATHOLOGY);
            Param.Add("ION_SWAB_INSTRUMENT_CHECK", ION_SWAB_INSTRUMENT_CHECK);
            Param.Add("ION_STARTED_TIME", ION_STARTED_TIME);
            Param.Add("ION_FINISHED_TIME", ION_FINISHED_TIME);
            Param.Add("ION_IV_TRANSFUSION_INFUSION", ION_IV_TRANSFUSION_INFUSION);
            Param.Add("ION_DRAINS", ION_DRAINS);
            Param.Add("ION_SEDATION_ANTIBIOTICS_DRUGS", ION_SEDATION_ANTIBIOTICS_DRUGS);
            Param.Add("ION_CATHETERS", ION_CATHETERS);
            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_IP_OperationNotesAdd", Param);

        }

    }
}
