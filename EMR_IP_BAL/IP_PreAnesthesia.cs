﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_PreAnesthesia
    {
        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }

        public string IPASD_TYPE { set; get; }
        public string IPASD_FIELD_ID { set; get; }
        public string IPASD_FIELD_NAME { set; get; }
        public string IPASD_VALUE { set; get; }
        public string IPASD_COMMENT { set; get; }
        public string IPASD_ENTRY_FROM { set; get; }




        public void  PreAnesthesiaSegmentDtlsAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPASD_BRANCH_ID", BranchID);
            Param.Add("IPASD_ID", EMRID);
            Param.Add("IPASD_PT_ID", PTID);
            Param.Add("IPASD_TYPE", IPASD_TYPE);
            Param.Add("IPASD_FIELD_ID", IPASD_FIELD_ID);
            Param.Add("IPASD_FIELD_NAME", IPASD_FIELD_NAME);
            Param.Add("IPASD_VALUE", IPASD_VALUE);
            Param.Add("IPASD_COMMENT", IPASD_COMMENT);
            Param.Add("IPASD_ENTRY_FROM", IPASD_ENTRY_FROM);

            objDB.ExecuteNonQuery("HMS_SP_IPPreAnesthesiaSegmentDtlsAdd", Param);

        }

        public DataSet  PreAnesthesiaSegmentDtlsGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPPreAnesthesiaSegmentDtlsGet", Param);

            return DS;


        }
    }
}
