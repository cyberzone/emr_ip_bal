﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
   public class IP_Templates
    {

        dbOperation objDB = new dbOperation();

        public string IT_BRANCH_ID { set; get; }
        public string IT_TYPE { set; get; }
        public string IT_CODE { set; get; }
        public string IT_NAME { set; get; }
        public string IT_TEMPLATE { set; get; }
        public string IT_APPLY { set; get; }
        public string IT_DR_CODE { set; get; }
        public string IT_DEP_ID { set; get; }



        public DataSet IPTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPTemplatesGet", Param);

            return DS;


        }


        public void IPTemplatesAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IT_BRANCH_ID", IT_BRANCH_ID);
            Param.Add("IT_TYPE", IT_TYPE);
            Param.Add("IT_NAME", IT_NAME);

            Param.Add("IT_TEMPLATE", IT_TEMPLATE);
            Param.Add("IT_APPLY", IT_APPLY);
            Param.Add("IT_DR_CODE", IT_DR_CODE);
            Param.Add("IT_DEP_ID", IT_DEP_ID);


            objDB.ExecuteNonQuery("HMS_SP_IPTemplatesAdd", Param);

        }
    }
}
