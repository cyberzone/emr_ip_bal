﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_DischargeSummary
    {

        dbOperation objDB = new dbOperation();
        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string IDS_PRINC_DIAG { set; get; }
        public string IDS_ADD_DIAG { set; get; }
        public string IDS_ADMIN_REASON { set; get; }
        public string IDS_SIGNIFICANT_FINDINGS { set; get; }
        public string IDS_PROCEDURES { set; get; }
        public string IDS_TREAT_MEDIC { set; get; }

        public string IDS_DATE { set; get; }
        public string IDS_CONDITION { set; get; }
        public string IDS_INSTRUCTIONS { set; get; }
        public string IDS_PHY_ACTIVITY { set; get; }
        public string IDS_DIET { set; get; }
        public string IDS_FOLLOWUPCARE { set; get; }
        public string IDS_MEDICATIONS { set; get; }
        public string IDS_INSTR_MEDICATION { set; get; }
        public string IDS_INFO_MEDIC_FOOD { set; get; }
        public string IDS_TRANSPOR_TYPE { set; get; }
        public string IDS_INSTR_MEDICATION_REMARKS { set; get; }
        public string IDS_INFO_MEDIC_FOOD_REMARKS { set; get; }

        public string AdmissionNO { set; get; }

        public string UserID { set; get; }


        public DataSet DischargeSummaryGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPDischargeSummaryGet", Param);

            return DS;


        }


        public void DischargeSummaryAdd()
        {


            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IDS_BRANCH_ID", BranchID);
            Param.Add("IDS_ID", EMRID);
            Param.Add("IDS_PT_ID", PTID);

            Param.Add("IDS_PRINC_DIAG", IDS_PRINC_DIAG);
            Param.Add("IDS_ADD_DIAG", IDS_ADD_DIAG);
            Param.Add("IDS_ADMIN_REASON", IDS_ADMIN_REASON);
            Param.Add("IDS_SIGNIFICANT_FINDINGS", IDS_SIGNIFICANT_FINDINGS);
            Param.Add("IDS_PROCEDURES", IDS_PROCEDURES);
            Param.Add("IDS_TREAT_MEDIC", IDS_TREAT_MEDIC);

            Param.Add("IDS_DATE", IDS_DATE);

            Param.Add("IDS_CONDITION", IDS_CONDITION);
            Param.Add("IDS_INSTRUCTIONS", IDS_INSTRUCTIONS);
            Param.Add("IDS_PHY_ACTIVITY", IDS_PHY_ACTIVITY);
            Param.Add("IDS_DIET", IDS_DIET);
            Param.Add("IDS_FOLLOWUPCARE", IDS_FOLLOWUPCARE);

            Param.Add("IDS_MEDICATIONS", IDS_MEDICATIONS);
            Param.Add("IDS_INSTR_MEDICATION", IDS_INSTR_MEDICATION);
            Param.Add("IDS_INFO_MEDIC_FOOD", IDS_INFO_MEDIC_FOOD);
            Param.Add("IDS_TRANSPOR_TYPE", IDS_TRANSPOR_TYPE);
            Param.Add("IDS_INSTR_MEDICATION_REMARKS", IDS_INSTR_MEDICATION_REMARKS); 
			Param.Add("IDS_INFO_MEDIC_FOOD_REMARKS", IDS_INFO_MEDIC_FOOD_REMARKS);
            Param.Add("AdmissionNO", AdmissionNO);  
            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_IPDischargeSummaryAdd", Param);

        }


        public void  PTDiagnosisAddFromDisDiag()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BranchID", BranchID);
            Param.Add("EMRID", EMRID);
          
            objDB.ExecuteNonQuery("HMS_SP_IP_PTDiagnosisAddFromDisDiag", Param);

        }
    }
}
