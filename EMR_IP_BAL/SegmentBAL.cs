﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace EMR_IP_BAL
{
   public  class SegmentBAL
    {
        dbOperation objDB = new dbOperation();

        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string Type { set; get; }
        public string SubType { set; get; }

        public string FieldName { set; get; }
        public string Value { set; get; }
        public string ValueYes { set; get; }
        public string TemplateCode { set; get; }
              

        public DataSet SegmentMasterGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRSegmentMasterGet", Param);

            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRSegmentTemplatesGet", Param);

            return DS;

        }

        public DataSet SegmentVisitDtlsGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRSegmentVisitDtlsGet", Param);

            return DS;

        }

        public DataSet SegmentVisitDtlsTopGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRSegmentVisitDtlsTopGet", Param);

            return DS;

        }

        public DataSet SegmentVisitDtlsAndSegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT * FROM EMR_SEGMENT_VISIT_DETAILS ESVD INNER  JOIN EMR_SEGMENT_TEMPLATES EST ON ESVD.ESVD_TYPE= EST.EST_TYPE AND ESVD.ESVD_FIELDNAME = EST.EST_FIELDNAME  WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }

        public DataSet WEMR_spS_GetSegmentVisits(string BranchID, string EMRID)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetSegmentVisits", Param);



            return DS;

        }


        public void EMRSegmentVisitDtlsAdd()
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("ESVD_BRANCH_ID", BranchID);
            Param.Add("ESVD_ID", EMRID);
            Param.Add("ESVD_TYPE", Type);
            Param.Add("ESVD_SUBTYPE", SubType);

            Param.Add("ESVD_FIELDNAME", FieldName);
            Param.Add("ESVD_VALUE", Value);
            Param.Add("ESVD_VALUE_YES", ValueYes);
            Param.Add("ESVD_TEMPLATE_CODE", TemplateCode);


            objDB.ExecuteNonQuery("HMS_SP_EMRSegmentVisitDtlsAdd", Param);



        }


        //public void EMRSegmentVisitDtlsDelete()
        //{
        //    DataSet DS = new DataSet();

        //    IDictionary<string, string> Param = new Dictionary<string, string>();
        //    Param.Add("ESVD_BRANCH_ID", BranchID);
        //    Param.Add("ESVD_ID", EMRID);
        //    Param.Add("ESVD_TYPE", Type);
        //  //  Param.Add("ESVD_SUBTYPE", SubType);

        //    objDB.ExecuteNonQuery("HMS_SP_EMRSegmentVisitDtlsDelete", Param);



        //}

        public void EMRSegmentVisitDtlsDelete(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " DELETE  FROM   EMR_SEGMENT_VISIT_DETAILS WHERE " + Criteria;

            objDB.ExecuteNonQuery(strSQL);



        }


        public DataSet EMRSegmentVisitDtlsWithFieldNameAlias(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT  * "
                   + " ,(SELECT TOP 1 EST_FIELDNAME_ALIAS FROM EMR_SEGMENT_TEMPLATES WHERE EST_TYPE= ESVD_TYPE AND EST_SUBTYPE=ESVD_SUBTYPE AND EST_FIELDNAME = ESVD_FIELDNAME) AS EST_FIELDNAME_ALIAS "
                   + "  FROM EMR_SEGMENT_VISIT_DETAILS  WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }
    }
}
