﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
    public class IP_PreOperativeChecklist
    {

        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }

        public string IPC_FIELD_ID { set; get; }
        public string IPC_VALUE { set; get; }
        public string IPC_REMARKS { set; get; }
        public string IPC_VALUE_OTNURSE { set; get; }

        public string IPP_PRO_ID { set; get; }

        public string IPP_PRO_CODE { set; get; }
        public string IPP_PRO_NAME { set; get; }
        public string IPP_QTY { set; get; }
        public string IPP_DATE { set; get; }
        public string IPP_SURG_ID { set; get; }
        public string IPP_SURG_NAME { set; get; }


        public DataSet PreOperativeChecklistGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPPreOperativeChecklistGet", Param);

            return DS;


        }

        public void PreOperativeChecklistAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPC_BRANCH_ID", BranchID);
            Param.Add("IPC_ID", EMRID);
            Param.Add("IPC_PT_ID", PTID);

            Param.Add("IPC_FIELD_ID", IPC_FIELD_ID);
            Param.Add("IPC_VALUE", IPC_VALUE);
            Param.Add("IPC_REMARKS", IPC_REMARKS);
            Param.Add("IPC_VALUE_OTNURSE", IPC_VALUE_OTNURSE);

            objDB.ExecuteNonQuery("HMS_SP_IPPreOperativeChecklistAdd", Param);

        }


        public DataSet PreOperativeProceduresGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPPreOperativeProceduresGet", Param);

            return DS;


        }

        public void PreOperativeProceduresAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPP_BRANCH_ID", BranchID);
            Param.Add("IPP_ID", EMRID);
            Param.Add("IPP_PRO_ID", IPP_PRO_ID);

            Param.Add("IPP_PRO_CODE", IPP_PRO_CODE);
            Param.Add("IPP_PRO_NAME", IPP_PRO_NAME);
            Param.Add("IPP_QTY", IPP_QTY);
            Param.Add("IPP_DATE", IPP_DATE);
            Param.Add("IPP_SURG_ID", IPP_SURG_ID);
            Param.Add("IPP_SURG_NAME", IPP_SURG_NAME);



            objDB.ExecuteNonQuery("HMS_SP_IPPreOperativeProceduresAdd", Param);

        }


    }
}
