﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_Anesthesia
    {
        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }

        public string IASD_TYPE { set; get; }
        public string IASD_FIELD_ID { set; get; }
        public string IASD_FIELD_NAME { set; get; }
        public string IASD_VALUE { set; get; }
        public string IASD_COMMENT { set; get; }


        public string IA_DATE { set; get; }
        public string IA_ANESTHETIC_PROCEDURES { set; get; }
        public string IA_SURGICAL_PROCEDURES { set; get; }
        public string IA_SURGEON { set; get; }
        public string IA_ASSISTANT { set; get; }
        public string IA_TIME_IN { set; get; }
        public string IA_ANESTHESIA_START { set; get; }
        public string IA_SURGERY_START { set; get; }
        public string IA_SURGERY_ENDS { set; get; }
        public string IA_ANESTHESIA_STOPES { set; get; }
        public string IA_PT_OUT_OF_OR { set; get; }
        public string IA_BP_SCORE { set; get; }
        public string IA_SPO2_SCORE { set; get; }
        public string IA_CONCIOUSNESS_SCORE { set; get; }
        public string IA_MOTOR_FUN_SCORE { set; get; }
        public string IA_PAIN_SCORE { set; get; }
        public string IA_RESPIRATION_SCORE { set; get; }
        public string IA_ANS_CHART_FILE_NAME { set; get; }


        public string IA_SURGEON1 { set; get; }
        public string IA_SURGEON2 { set; get; }

        public string UserID { set; get; }


        public DataSet AnesthesiaSegmentMasterGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPAnesthesiaSegmentMasterGet", Param);

            return DS;


        }

        public void AnesthesiaSegmentDtlsAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IASD_BRANCH_ID", BranchID);
            Param.Add("IASD_ID", EMRID);
            Param.Add("IASD_PT_ID", PTID);
            Param.Add("IASD_TYPE", IASD_TYPE);
            Param.Add("IASD_FIELD_ID", IASD_FIELD_ID);
            Param.Add("IASD_FIELD_NAME", IASD_FIELD_NAME);
            Param.Add("IASD_VALUE", IASD_VALUE);
            Param.Add("IASD_COMMENT", IASD_COMMENT);

            objDB.ExecuteNonQuery("HMS_SP_IPAnesthesiaSegmentDtlsAdd", Param);

        }

        public DataSet AnesthesiaSegmentDtlsGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPAnesthesiaSegmentDtlsGet", Param);

            return DS;


        }


        public DataSet AnesthesiaGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_AnesthesiaGet", Param);

            return DS;


        }


        public void AnesthesiaAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BranchID", BranchID);
            Param.Add("EMRID", EMRID);
            Param.Add("PTID", PTID);
            Param.Add("IA_DATE", IA_DATE);
            Param.Add("IA_ANESTHETIC_PROCEDURES", IA_ANESTHETIC_PROCEDURES);
            Param.Add("IA_SURGICAL_PROCEDURES", IA_SURGICAL_PROCEDURES);
            Param.Add("IA_SURGEON", IA_SURGEON);
            Param.Add("IA_SURGEON1", IA_SURGEON1);
            Param.Add("IA_SURGEON2", IA_SURGEON2);
            Param.Add("IA_ASSISTANT", IA_ASSISTANT);
            Param.Add("IA_TIME_IN", IA_TIME_IN);
            Param.Add("IA_ANESTHESIA_START", IA_ANESTHESIA_START);
            Param.Add("IA_SURGERY_START", IA_SURGERY_START);
            Param.Add("IA_SURGERY_ENDS", IA_SURGERY_ENDS);
            Param.Add("IA_ANESTHESIA_STOPES", IA_ANESTHESIA_STOPES);
            Param.Add("IA_PT_OUT_OF_OR", IA_PT_OUT_OF_OR);
            Param.Add("IA_BP_SCORE", IA_BP_SCORE);
            Param.Add("IA_SPO2_SCORE", IA_SPO2_SCORE);
            Param.Add("IA_CONCIOUSNESS_SCORE", IA_CONCIOUSNESS_SCORE);
            Param.Add("IA_MOTOR_FUN_SCORE", IA_MOTOR_FUN_SCORE);
            Param.Add("IA_PAIN_SCORE", IA_PAIN_SCORE);
            Param.Add("IA_RESPIRATION_SCORE", IA_RESPIRATION_SCORE);
            Param.Add("IA_ANS_CHART_FILE_NAME", IA_ANS_CHART_FILE_NAME);
          



            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_IP_AnesthesiaAdd", Param);

        }
    }
}
