﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
    public class EMR_EducationalForm
    {
        dbOperation objDB = new dbOperation();

        public string branchid { set; get; }
        public string patientmasterid { set; get; }

        public string language { set; get; }
        public string read { set; get; }

        public string write { set; get; }
        public string speak { set; get; }
        public string religion { set; get; }


        public void WEMR_spI_SaveEducationalForm()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("branchid", branchid);
            Param.Add("patientmasterid", patientmasterid);
            Param.Add("language", language);
            Param.Add("read", read);
            Param.Add("write", write);
            Param.Add("speak", speak);
            Param.Add("religion", religion);


            objDB.ExecuteNonQuery("WEMR_spI_SaveEducationalForm", Param);

        }


    }
}
