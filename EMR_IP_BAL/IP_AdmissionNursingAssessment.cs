﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
    public class IP_AdmissionNursingAssessment
    {

        dbOperation objDB = new dbOperation();


        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }

        public string IANA_ADMISSION_ROUTE { set; get; }
        public string IANA_ADMISSION_WARD { set; get; }
        public string IANA_ROOM_NO { set; get; }
        public string IANA_BED_NO { set; get; }
        public string IANA_WARD_ARRIVAL_TIME { set; get; }
        public string IANA_ADMISSION_DATE { set; get; }
        public string IANA_ADMISSION_MODE { set; get; }
        public string IANA_INFO_OBTAINED_FROM { set; get; }
        public string IANA_DIAGNOSIS { set; get; }
        public string IANA_WEIGHT { set; get; }
        public string IANA_HEIGHT { set; get; }
        public string IANA_PT_PAIN_SCORE { set; get; }
        public string IANA_ORIENTATION_UNIT { set; get; }
        public string IANA_BROUGHT_VALUABLE { set; get; }
        public string IANA_SENT_HOME_WITH { set; get; }
        public string IANA_PRO_GIVEN_BY { set; get; }
        public string IANA_LIVING_SITUATION { set; get; }
        public string IANA_PROFESSION { set; get; }
        public string IANA_LIVING_UAE { set; get; }
        public string IANA_MARRIED { set; get; }
        public string IANA_SMOKE { set; get; }
        public string IANA_SMOKE_COUNT { set; get; }
        public string IANA_QUIT_SMOKE { set; get; }
        public string IANA_ALCOHOL { set; get; }
        public string IANA_QUIT_ALCOHOL { set; get; }
        public string IANA_PRES_HEALTH_PROB { set; get; }
        public string IANA_PROB_IDENTIFIED { set; get; }
        public string IANA_PREV_HOSP_SURG { set; get; }
        public string IANA_ALLERGY { set; get; }
        public string IANA_ALLERGY_DESC { set; get; }
        public string IANA_REACTION_TYPE { set; get; }
        public string IANA_MEDICINE_BROUGHT { set; get; }
        public string IANA_PHYSICIAN_VARIFIED { set; get; }
        public string IANA_MED_SENT_HOME_WITH { set; get; }
        public string IANA_PAIN_SCORE { set; get; }
        public string IANA_PAIN_SCORE_TYPE { set; get; }
        public string IANA_PAIN_LOCATION { set; get; }
        public string IANA_PAIN_ONSET { set; get; }
        public string IANA_PAIN_VARIATIONS { set; get; }
        public string IANA_PAIN_AGGRAVATES { set; get; }
        public string IANA_PAIN_RELIEVES { set; get; }
        public string IANA_PAIN_MEDICATIONS { set; get; }
        public string IANA_PAIN_EFFECTS { set; get; }
        public string IANA_LAMA_FILE_NAME{ set; get; }

        public string UserID { set; get; }




        public string IANM_BRANCH_ID { set; get; }
        public string IANM_ID { set; get; }
        public string IANM_PHY_CODE { set; get; }
        public string IANM_PHY_NAME { set; get; }
        public string IANM_DOSAGE { set; get; }
        public string IANM_ROUTE { set; get; }
        public string IANM_FREQUENCY { set; get; }
        public string IANM_FREQUENCYTYPE { set; get; }
        public string IANM_LAST_DOSAGE { set; get; }



        public string INASD_TYPE { set; get; }
        public string INASD_FIELD_ID { set; get; }
        public string INASD_FIELD_NAME { set; get; }
        public string INASD_VALUE { set; get; }
        public string INASD_COMMENT { set; get; }



        public DataSet AdmiNursingAssessmentGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_AdmiNursingAssessmentGet", Param);
            return DS;

        }



        public void AdmiNursingAssessmentAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IANA_BRANCH_ID", BranchID);
            Param.Add("IANA_ID", EMRID);
            Param.Add("IANA_PT_ID", PTID);




            Param.Add("IANA_ADMISSION_ROUTE", IANA_ADMISSION_ROUTE);
            Param.Add("IANA_ADMISSION_WARD", IANA_ADMISSION_WARD);
            Param.Add("IANA_ROOM_NO", IANA_ROOM_NO);
            Param.Add("IANA_BED_NO", IANA_BED_NO);
            Param.Add("IANA_WARD_ARRIVAL_TIME", IANA_WARD_ARRIVAL_TIME);
            Param.Add("IANA_ADMISSION_DATE", IANA_ADMISSION_DATE);
            Param.Add("IANA_ADMISSION_MODE", IANA_ADMISSION_MODE);
            Param.Add("IANA_INFO_OBTAINED_FROM", IANA_INFO_OBTAINED_FROM);
            Param.Add("IANA_DIAGNOSIS", IANA_DIAGNOSIS);
            Param.Add("IANA_WEIGHT", IANA_WEIGHT);
            Param.Add("IANA_HEIGHT", IANA_HEIGHT);
            Param.Add("IANA_PT_PAIN_SCORE", IANA_PT_PAIN_SCORE);
            Param.Add("IANA_ORIENTATION_UNIT", IANA_ORIENTATION_UNIT);
            Param.Add("IANA_BROUGHT_VALUABLE", IANA_BROUGHT_VALUABLE);
            Param.Add("IANA_SENT_HOME_WITH", IANA_SENT_HOME_WITH);
            Param.Add("IANA_PRO_GIVEN_BY", IANA_PRO_GIVEN_BY);
            Param.Add("IANA_LIVING_SITUATION", IANA_LIVING_SITUATION);
            Param.Add("IANA_PROFESSION", IANA_PROFESSION);
            Param.Add("IANA_LIVING_UAE", IANA_LIVING_UAE);
            Param.Add("IANA_MARRIED", IANA_MARRIED);
            Param.Add("IANA_SMOKE", IANA_SMOKE);
            Param.Add("IANA_SMOKE_COUNT", IANA_SMOKE_COUNT);
            Param.Add("IANA_QUIT_SMOKE", IANA_QUIT_SMOKE);
            Param.Add("IANA_ALCOHOL", IANA_ALCOHOL);
            Param.Add("IANA_QUIT_ALCOHOL", IANA_QUIT_ALCOHOL);
            Param.Add("IANA_PRES_HEALTH_PROB", IANA_PRES_HEALTH_PROB);
            Param.Add("IANA_PROB_IDENTIFIED", IANA_PROB_IDENTIFIED);
            Param.Add("IANA_PREV_HOSP_SURG", IANA_PREV_HOSP_SURG);
            Param.Add("IANA_ALLERGY", IANA_ALLERGY);
            Param.Add("IANA_ALLERGY_DESC", IANA_ALLERGY_DESC);
            Param.Add("IANA_REACTION_TYPE", IANA_REACTION_TYPE);
            Param.Add("IANA_MEDICINE_BROUGHT", IANA_MEDICINE_BROUGHT);
            Param.Add("IANA_PHYSICIAN_VARIFIED", IANA_PHYSICIAN_VARIFIED);
            Param.Add("IANA_MED_SENT_HOME_WITH", IANA_MED_SENT_HOME_WITH);
            Param.Add("IANA_PAIN_SCORE", IANA_PAIN_SCORE);
            Param.Add("IANA_PAIN_SCORE_TYPE", IANA_PAIN_SCORE_TYPE);
            Param.Add("IANA_PAIN_LOCATION", IANA_PAIN_LOCATION);
            Param.Add("IANA_PAIN_ONSET", IANA_PAIN_ONSET);
            Param.Add("IANA_PAIN_VARIATIONS", IANA_PAIN_VARIATIONS);
            Param.Add("IANA_PAIN_AGGRAVATES", IANA_PAIN_AGGRAVATES);
            Param.Add("IANA_PAIN_RELIEVES", IANA_PAIN_RELIEVES);
            Param.Add("IANA_PAIN_MEDICATIONS", IANA_PAIN_MEDICATIONS);
            Param.Add("IANA_PAIN_EFFECTS", IANA_PAIN_EFFECTS);
            Param.Add("IANA_LAMA_FILE_NAME", IANA_LAMA_FILE_NAME);

            Param.Add("UserID", UserID);


            objDB.ExecuteNonQuery("HMS_SP_IP_AdmiNursingAssessmentAdd", Param);

        }

        public DataSet AdmiNurseMedicHistoryGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_AdmiNurseMedicHistoryGet", Param);
            return DS;

        }

        public void AdmiNurseMedicHistoryAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IANM_BRANCH_ID", BranchID);
            Param.Add("IANM_ID", EMRID);
            Param.Add("IANM_PHY_CODE", IANM_PHY_CODE);
            Param.Add("IANM_PHY_NAME", IANM_PHY_NAME);
            Param.Add("IANM_DOSAGE", IANM_DOSAGE);
            Param.Add("IANM_ROUTE", IANM_ROUTE);
            Param.Add("IANM_FREQUENCY", IANM_FREQUENCY);
            Param.Add("IANM_FREQUENCYTYPE", IANM_FREQUENCYTYPE);
            Param.Add("IANM_LAST_DOSAGE", IANM_LAST_DOSAGE);

            objDB.ExecuteNonQuery("HMS_SP_IP_AdmiNurseMedicHistoryAdd", Param);

        }


        public DataSet AdminNurseAssSegmentDtlsGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_AdminNurseAssSegmentDtlsGet", Param);

            return DS;


        }

        public void AdminNurseAssSegmentDtlsAdd()
        {


            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("INASD_BRANCH_ID", BranchID);
            Param.Add("INASD_ID", EMRID);
            Param.Add("INASD_PT_ID", PTID);

            Param.Add("INASD_TYPE", INASD_TYPE);
            Param.Add("INASD_FIELD_ID", INASD_FIELD_ID);
            Param.Add("INASD_FIELD_NAME", INASD_FIELD_NAME);
            Param.Add("INASD_VALUE", INASD_VALUE);
            Param.Add("INASD_COMMENT", INASD_COMMENT);



            objDB.ExecuteNonQuery("HMS_SP_IP_AdminNurseAssSegmentDtlsAdd", Param);

        }


        public DataSet AdminNurseAssSegmentGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_AdminNurseAssSegmentGet", Param);

            return DS;


        }
    }
}
