﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_IntraOperativeNursingRecord
    {
        dbOperation objDB = new dbOperation();

        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }

        public string IONR_DATE { set; get; }
        public string IONR_PRE_OPER_DIAG { set; get; }
        public string IONR_POST_OPER_DIAG { set; get; }
        public string IONR_OPER_PROCEDURE { set; get; }
        public string IONR_SURGEN { set; get; }
        public string IONR_ANAESTHETIST { set; get; }
        public string IONR_ANESTHESIA_TYPE { set; get; }
        public string IONR_ANESTHESIA_CLASS { set; get; }
        public string IONR_PT_IDENTIFI { set; get; }
        public string IONR_ALLERGIES { set; get; }
        public string IONR_NPO { set; get; }
        public string IONR_OPER_CONSENT { set; get; }
        public string IONR_TIME_OUT { set; get; }
        public string IONR_VARIFI_OPER_CHECKLIST { set; get; }
        public string IONR_ASSESS_COMMENT { set; get; }
        public string IONR_CASE_CLASSIFI { set; get; }
        public string IONR_CASE_TYPE { set; get; }
        public string IONR_INTUBATION_TIME { set; get; }
        public string IONR_NCISION_TIME { set; get; }
        public string IONR_EXTUBATION_TIME { set; get; }
        public string IONR_HAIR_REMOVAL { set; get; }
        public string IONR_DRAINS_PACKING { set; get; }
        public string IONR_DRAINS_LOCATION { set; get; }
        public string IONR_CLIPPER { set; get; }
        public string IONR_SKIN_CONDITION { set; get; }
        public string IONR_DRESSING { set; get; }
        public string IONR_SPONGE_COUNT { set; get; }
        public string IONR_GAUZE_COUNT { set; get; }
        public string IONR_PAENUT_COUNT { set; get; }
        public string IONR_NEEDLE_COUNT { set; get; }
        public string IONR_INSTRUMENTS_COUNT { set; get; }
        public string IONR_COTTON_BALLS_COUNT { set; get; }
        public string IONR_COUNT_CORRECT { set; get; }
        public string IONR_COUNT_CORRECT_SURG_INF { set; get; }
        public string IONR_COUNT_INCORRECT { set; get; }
        public string IONR_COUNT_INCORRECT_SURG_INF { set; get; }


        public string IONR_CORRECTIVE_ACTION { set; get; }
        public string IONR_CORRECTIVE_COMMENT { set; get; }
        public string IONR_SPECIMEN { set; get; }
        public string IONR_SPECIMEN_TYPE { set; get; }
        public string IONR_PATIENT_TRANS_TO { set; get; }


        public string IONR_POSITION { set; get; }
        public string IONR_POSITIONAL_AIDE { set; get; }
        public string IONR_SAFETY_STRAP_ON { set; get; }
        public string IONR_SAFETY_STRAP_DESC { set; get; }
        public string IONR_POSITIONED_BY { set; get; }


        public string IONR_ELECTROSURGICAL_UNIT { set; get; }
        public string IONR_RET_ELECTRODE_LOCATION { set; get; }
        public string IONR_ELECT_SURG_CUT { set; get; }
        public string IONR_ELECT_SURG_COAG { set; get; }
        public string IONR_ELECT_BIPOLAR { set; get; }
        public string IONR_ELECT_APPLIEDBY { set; get; }


        public string IONR_SKIN_COND_UNDER_PAD { set; get; }
        public string IONR_TOURNIQUETS_TIME_UP { set; get; }
        public string IONR_TOURNIQUETS_TIME_DOWN { set; get; }
        public string IONR_SKIN_STATUS_BE_PLACEMENT { set; get; }
        public string IONR_SKIN_STATUS_AF_PLACEMENT { set; get; }
        public string IONR_SKIN_SITE_LOCATION { set; get; }
        public string IONR_SKIN_APPLIED_BY { set; get; }



        public string IONR_FOLY_CATH_NO { set; get; }
        public string IONR_URIN_CAT_INSERTEDBY { set; get; }
        public string IONR_URINE_RETURNED { set; get; }
        public string IONR_PER_ANESTHESIOLOGIST { set; get; }
        public string IONR_TOTAL_BLOOD_PROD { set; get; }
        public string IONR_TOTAL_IV_FLUIDS { set; get; }
        public string IONR_URINARY_DRAINAGE { set; get; }
        public string IONR_ESTI_BLOOD_LOSS { set; get; }
        public string IONR_OUTPUT_OTHERS { set; get; }


        public string IONR_PROSTHESIS_LOCA { set; get; }
        public string IONR_PROSTHESIS_SIZE { set; get; }
        public string IONR_PROSTHESIS_SL_NO { set; get; }
        public string IONR_PROSTHESIS_MANUFA { set; get; }
        public string IONR_XRAY_CARAM { set; get; }
        public string IONR_XRAY_LASER { set; get; }
        public string IONR_MICROSCOPE { set; get; }
        public string IONR_OTHER { set; get; }
        public string IONR_COMMENT { set; get; }

        public string IONR_SCRUB_NURSE { set; get; }
        public string IONR_SCRUB_NURSE_DATE { set; get; }
        public string IONR_CIRCUL_NURSE { set; get; }
        public string IONR_CIRCUL_NURSE_DATE { set; get; }
        public string IONR_REL_SCRUB_NURSE { set; get; }
        public string IONR_REL_SCRUB_NURSE_DATE { set; get; }
        public string IONR_REL_CIRCUL_NURSE { set; get; }
        public string IONR_REL_CIRCUL_NURSE_DATE { set; get; }


        public string UserID { set; get; }

        public DataSet InterOperNursRecordGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_InterOperNursRecordGet", Param);

            return DS;


        }





        public void InterOperNursRecordAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IONR_BRANCH_ID", BranchID);
            Param.Add("IONR_ID", EMRID);
            Param.Add("IONR_PT_ID", PTID);

            Param.Add("IONR_DATE", IONR_DATE);
            Param.Add("IONR_PRE_OPER_DIAG", IONR_PRE_OPER_DIAG);
            Param.Add("IONR_POST_OPER_DIAG", IONR_POST_OPER_DIAG);
            Param.Add("IONR_OPER_PROCEDURE", IONR_OPER_PROCEDURE);

            Param.Add("IONR_SURGEN", IONR_SURGEN);
            Param.Add("IONR_ANAESTHETIST", IONR_ANAESTHETIST);
            Param.Add("IONR_ANESTHESIA_TYPE", IONR_ANESTHESIA_TYPE);
            Param.Add("IONR_ANESTHESIA_CLASS", IONR_ANESTHESIA_CLASS);


            Param.Add("IONR_PT_IDENTIFI", IONR_PT_IDENTIFI);
            Param.Add("IONR_ALLERGIES", IONR_ALLERGIES);
            Param.Add("IONR_NPO", IONR_NPO);
            Param.Add("IONR_OPER_CONSENT", IONR_OPER_CONSENT);

            Param.Add("IONR_TIME_OUT", IONR_TIME_OUT);
            Param.Add("IONR_VARIFI_OPER_CHECKLIST", IONR_VARIFI_OPER_CHECKLIST);
            Param.Add("IONR_ASSESS_COMMENT", IONR_ASSESS_COMMENT);
            Param.Add("IONR_CASE_CLASSIFI", IONR_CASE_CLASSIFI);

            Param.Add("IONR_CASE_TYPE", IONR_CASE_TYPE);
            Param.Add("IONR_INTUBATION_TIME", IONR_INTUBATION_TIME);
            Param.Add("IONR_NCISION_TIME", IONR_NCISION_TIME);
            Param.Add("IONR_EXTUBATION_TIME", IONR_EXTUBATION_TIME);

            Param.Add("IONR_HAIR_REMOVAL", IONR_HAIR_REMOVAL);
            Param.Add("IONR_DRAINS_PACKING", IONR_DRAINS_PACKING);
            Param.Add("IONR_DRAINS_LOCATION", IONR_DRAINS_LOCATION);
            Param.Add("IONR_CLIPPER", IONR_CLIPPER);

            Param.Add("IONR_SKIN_CONDITION", IONR_SKIN_CONDITION);
            Param.Add("IONR_DRESSING", IONR_DRESSING);
            Param.Add("IONR_SPONGE_COUNT", IONR_SPONGE_COUNT);
            Param.Add("IONR_GAUZE_COUNT", IONR_GAUZE_COUNT);

            Param.Add("IONR_PAENUT_COUNT", IONR_PAENUT_COUNT);
            Param.Add("IONR_NEEDLE_COUNT", IONR_NEEDLE_COUNT);
            Param.Add("IONR_INSTRUMENTS_COUNT", IONR_INSTRUMENTS_COUNT);
            Param.Add("IONR_COTTON_BALLS_COUNT", IONR_COTTON_BALLS_COUNT);

            Param.Add("IONR_COUNT_CORRECT", IONR_COUNT_CORRECT);
            Param.Add("IONR_COUNT_CORRECT_SURG_INF", IONR_COUNT_CORRECT_SURG_INF);
            Param.Add("IONR_COUNT_INCORRECT", IONR_COUNT_INCORRECT);
            Param.Add("IONR_COUNT_INCORRECT_SURG_INF", IONR_COUNT_INCORRECT_SURG_INF);


            Param.Add("IONR_CORRECTIVE_ACTION", IONR_CORRECTIVE_ACTION);
            Param.Add("IONR_CORRECTIVE_COMMENT", IONR_CORRECTIVE_COMMENT);
            Param.Add("IONR_SPECIMEN", IONR_SPECIMEN);
            Param.Add("IONR_SPECIMEN_TYPE", IONR_SPECIMEN_TYPE);
            Param.Add("IONR_PATIENT_TRANS_TO", IONR_PATIENT_TRANS_TO);


            Param.Add("IONR_POSITION", IONR_POSITION);
            Param.Add("IONR_POSITIONAL_AIDE", IONR_POSITIONAL_AIDE);
            Param.Add("IONR_SAFETY_STRAP_ON", IONR_SAFETY_STRAP_ON);
            Param.Add("IONR_SAFETY_STRAP_DESC", IONR_SAFETY_STRAP_DESC);
            Param.Add("IONR_POSITIONED_BY", IONR_POSITIONED_BY);

            Param.Add("IONR_ELECTROSURGICAL_UNIT", IONR_ELECTROSURGICAL_UNIT);
            Param.Add("IONR_RET_ELECTRODE_LOCATION", IONR_RET_ELECTRODE_LOCATION);
            Param.Add("IONR_ELECT_SURG_CUT", IONR_ELECT_SURG_CUT);
            Param.Add("IONR_ELECT_SURG_COAG", IONR_ELECT_SURG_COAG);
            Param.Add("IONR_ELECT_BIPOLAR", IONR_ELECT_BIPOLAR);
            Param.Add("IONR_ELECT_APPLIEDBY", IONR_ELECT_APPLIEDBY);


            Param.Add("IONR_SKIN_COND_UNDER_PAD", IONR_SKIN_COND_UNDER_PAD);
            Param.Add("IONR_TOURNIQUETS_TIME_UP", IONR_TOURNIQUETS_TIME_UP);
            Param.Add("IONR_TOURNIQUETS_TIME_DOWN", IONR_TOURNIQUETS_TIME_DOWN);
            Param.Add("IONR_SKIN_STATUS_BE_PLACEMENT", IONR_SKIN_STATUS_BE_PLACEMENT);
            Param.Add("IONR_SKIN_STATUS_AF_PLACEMENT", IONR_SKIN_STATUS_AF_PLACEMENT);
            Param.Add("IONR_SKIN_SITE_LOCATION", IONR_SKIN_SITE_LOCATION);
            Param.Add("IONR_SKIN_APPLIED_BY", IONR_SKIN_APPLIED_BY);

            Param.Add("IONR_FOLY_CATH_NO", IONR_FOLY_CATH_NO);
            Param.Add("IONR_URIN_CAT_INSERTEDBY", IONR_URIN_CAT_INSERTEDBY);
            Param.Add("IONR_URINE_RETURNED", IONR_URINE_RETURNED);
            Param.Add("IONR_PER_ANESTHESIOLOGIST", IONR_PER_ANESTHESIOLOGIST);
            Param.Add("IONR_TOTAL_BLOOD_PROD", IONR_TOTAL_BLOOD_PROD);
            Param.Add("IONR_TOTAL_IV_FLUIDS", IONR_TOTAL_IV_FLUIDS);
            Param.Add("IONR_URINARY_DRAINAGE", IONR_URINARY_DRAINAGE);
            Param.Add("IONR_ESTI_BLOOD_LOSS", IONR_ESTI_BLOOD_LOSS);
            Param.Add("IONR_OUTPUT_OTHERS", IONR_OUTPUT_OTHERS);


            Param.Add("IONR_PROSTHESIS_LOCA", IONR_PROSTHESIS_LOCA);
            Param.Add("IONR_PROSTHESIS_SIZE", IONR_PROSTHESIS_SIZE);
            Param.Add("IONR_PROSTHESIS_SL_NO", IONR_PROSTHESIS_SL_NO);
            Param.Add("IONR_PROSTHESIS_MANUFA", IONR_PROSTHESIS_MANUFA);
            Param.Add("IONR_XRAY_CARAM", IONR_XRAY_CARAM);
            Param.Add("IONR_XRAY_LASER", IONR_XRAY_LASER);
            Param.Add("IONR_MICROSCOPE", IONR_MICROSCOPE);
            Param.Add("IONR_OTHER", IONR_OTHER);
            Param.Add("IONR_COMMENT", IONR_COMMENT);

            Param.Add("IONR_SCRUB_NURSE", IONR_SCRUB_NURSE);
            Param.Add("IONR_SCRUB_NURSE_DATE", IONR_SCRUB_NURSE_DATE);
            Param.Add("IONR_CIRCUL_NURSE", IONR_CIRCUL_NURSE);
            Param.Add("IONR_CIRCUL_NURSE_DATE", IONR_CIRCUL_NURSE_DATE);
            Param.Add("IONR_REL_SCRUB_NURSE", IONR_REL_SCRUB_NURSE);
            Param.Add("IONR_REL_SCRUB_NURSE_DATE", IONR_REL_SCRUB_NURSE_DATE);
            Param.Add("IONR_REL_CIRCUL_NURSE", IONR_REL_CIRCUL_NURSE);
            Param.Add("IONR_REL_CIRCUL_NURSE_DATE", IONR_REL_CIRCUL_NURSE_DATE);


            Param.Add("UserID", UserID);
            objDB.ExecuteNonQuery("HMS_SP_IP_InterOperNursRecordAdd", Param);

        }
    }
}
