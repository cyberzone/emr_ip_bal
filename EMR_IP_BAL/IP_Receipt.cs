﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
   public  class IP_Receipt
    {


        dbOperation objDB = new dbOperation();

        public string HRM_BRANCH_ID { set; get; }
        public string HRM_RECEIPTNO { set; get; }
        public string HRM_DATE { set; get; }
        public string HRM_PT_ID { set; get; }
        public string HRM_PT_NAME { set; get; }
        public string HRM_PT_TYPE { set; get; }
        public string HRM_COMP_ID { set; get; }
        public string HRM_COMP_NAME { set; get; }
        public string HRM_COMP_TYPE { set; get; }
        public string HRM_DR_ID { set; get; }
        public string HRM_DR_NAME { set; get; }
        public string HRM_REF_ID { set; get; }
        public string HRM_REF_NAME { set; get; }
        public string HRM_AMT_RCVD { set; get; }

        public string HRM_PAYMENT_TYPE { set; get; }
        public string HRM_CCNO { set; get; }
        public string HRM_CCNAME { set; get; }
        public string HRM_HOLDERNAME { set; get; }
        public string HRM_CCREFNO { set; get; }

        public string HRM_CHEQUENO { set; get; }
        public string HRM_BANKNAME { set; get; }
        public string HRM_CHEQUEDATE { set; get; }
        public string HRM_CASH_AMT { set; get; }
        public string HRM_CC_AMT { set; get; }
        public string HRM_CHEQUE_AMT { set; get; }
        public string HRM_CUR_TYP { set; get; }
        public string HRM_CUR_RATE { set; get; }
        public string HRM_CUR_VALUE { set; get; }
        public string HRM_CUR_RCVD_AMT { set; get; }
        public string NewFlag { set; get; }

        public string UserID { set; get; }


        public string HRT_BRANCH_ID { set; get; }
        public string HRT_RECEIPTNO { set; get; }
        public string HRT_DATE { set; get; }
        public string HRT_INVOICE_ID { set; get; }
        public string HRT_NET_AMOUNT { set; get; }
        public string HRT_PAID_AMOUNT { set; get; }
        public string HRT_DISCOUNT { set; get; }






        public string ReceiptMasterAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HRM_BRANCH_ID", HRM_BRANCH_ID);
            Param.Add("HRM_RECEIPTNO", HRM_RECEIPTNO);
            Param.Add("HRM_DATE", HRM_DATE);
            Param.Add("HRM_PT_ID", HRM_PT_ID);
            Param.Add("HRM_PT_NAME", HRM_PT_NAME);
            Param.Add("HRM_PT_TYPE", HRM_PT_TYPE);
            Param.Add("HRM_COMP_ID", HRM_COMP_ID);
            Param.Add("HRM_COMP_NAME", HRM_COMP_NAME);
            Param.Add("HRM_COMP_TYPE", HRM_COMP_TYPE);
            Param.Add("HRM_DR_ID", HRM_DR_ID);
            Param.Add("HRM_DR_NAME", HRM_DR_NAME);
            Param.Add("HRM_REF_ID", HRM_REF_ID);
            Param.Add("HRM_REF_NAME", HRM_REF_NAME);
            Param.Add("HRM_AMT_RCVD", HRM_AMT_RCVD);

            Param.Add("HRM_PAYMENT_TYPE", HRM_PAYMENT_TYPE);
            Param.Add("HRM_CCNO", HRM_CCNO);
            Param.Add("HRM_CCNAME", HRM_CCNAME);
            Param.Add("HRM_HOLDERNAME", HRM_HOLDERNAME);
            Param.Add("HRM_CCREFNO", HRM_CCREFNO);

            Param.Add("HRM_CHEQUENO", HRM_CHEQUENO);
            Param.Add("HRM_BANKNAME", HRM_BANKNAME);
            Param.Add("HRM_CHEQUEDATE", HRM_CHEQUEDATE);
            Param.Add("HRM_CASH_AMT", HRM_CASH_AMT);
            Param.Add("HRM_CC_AMT", HRM_CC_AMT);
            Param.Add("HRM_CHEQUE_AMT", HRM_CHEQUE_AMT);
            Param.Add("HRM_CUR_TYP", HRM_CUR_TYP);
            Param.Add("HRM_CUR_RATE", HRM_CUR_RATE);
            Param.Add("HRM_CUR_VALUE", HRM_CUR_VALUE);
            Param.Add("HRM_CUR_RCVD_AMT", HRM_CUR_RCVD_AMT);
            Param.Add("NewFlag", NewFlag);
            Param.Add("UserID", UserID);

            string srtReceptNo = "";
            srtReceptNo = objDB.ExecuteNonQueryReturn("HMS_SP_ReceiptMasterAdd", Param);
            return srtReceptNo;
        }

        public void ReceiptTransAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HRT_BRANCH_ID", HRT_BRANCH_ID);
            Param.Add("HRT_RECEIPTNO", HRT_RECEIPTNO);
            Param.Add("HRT_DATE", HRT_DATE);
            Param.Add("HRT_INVOICE_ID", HRT_INVOICE_ID);
            Param.Add("HRT_NET_AMOUNT", HRT_NET_AMOUNT);
            Param.Add("HRT_PAID_AMOUNT", HRT_PAID_AMOUNT);
            Param.Add("HRT_DISCOUNT", HRT_DISCOUNT);


            objDB.ExecuteNonQuery("HMS_SP_ReceiptTransAdd", Param);

        }
    }
}
