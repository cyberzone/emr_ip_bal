﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace EMR_IP_BAL
{
    public class IP_PTMaster
    {

        dbOperation objDB = new dbOperation();

        public string IPM_ID { set; get; }
        public string IPM_BRANCH_ID { set; get; }
        public string IPM_DATE { set; get; }
        public string IPM_PT_ID { set; get; }
        public string IPM_PT_NAME { set; get; }
        public string IPM_AGE { set; get; }
        public string IPM_DR_CODE { set; get; }
        public string IPM_DR_NAME { set; get; }
        public string IPM_INS_CODE { set; get; }
        public string IPM_INS_NAME { set; get; }
        public string IPM_START_DATE { set; get; }
        public string IPM_END_DATE { set; get; }
        public string IPM_DEP_ID { set; get; }
        public string IPM_DEP_NAME { set; get; }
        public string IPM_PT_TYPE { set; get; }
        public string IPM_TYPE { set; get; }
        public string IPM_REF_DR_CODE { set; get; }
        public string UserId { set; get; }
        public string StartProcess { set; get; }
        public string VisitDate { set; get; }

        public string VisitDateTime { set; get; }

        public string IPM_PAINSCORE { set; get; }
        public string IPM_PAINSCORE_LOCATION { set; get; }
        public string IPM_PAINSCORE_INTENSITY { set; get; }
        public string IPM_PAINSCORE_CHARACTER { set; get; }
        public string IPM_PAINSCORE_DURATION { set; get; }
        public string IPM_PAINSCORE_OTHERS { set; get; }

        public string IPM_TREATMENT_PLAN { set; get; }
        public string IPM_FOLLOWUP_NOTES { set; get; }

        public string IPM_CRITICAL_NOTES { set; get; }

        public string ProcessCompleted { set; get; }
        public string IPM_VIP { set; get; }
        public string IPM_ROYAL { set; get; }


        public string AdmissionNo { set; get; }

        public string SO_ID { set; get; }
        public string DR_Name { set; get; }
        public string ServCode { set; get; }
        public string ServDesc { set; get; }
        public string ServQty { set; get; }
        public string ServType { set; get; }

        public string IAS_DATE { set; get; }
        public string IAS_EMG_CONT_PERSON { set; get; }
        public string IAS_EMG_CONT_RELATION { set; get; }
        public string IAS_EMG_CONT_NUMBER { set; get; }
        public string IAS_WARD_NO { set; get; }
        public string IAS_ROOM_NO { set; get; }
        public string IAS_BED_NO { set; get; }
        public string IAS_DRG_CODE { set; get; }
        public string IAS_DRG_AMOUNT { set; get; }
        public string IAS_AUTHORIZATION_ID { set; get; }
        public string IAS_AUTHORIZATION_START { set; get; }
        public string IAS_AUTHORIZATION_END { set; get; }
        public string IAS_LENTH_OF_STAY { set; get; }
        public string IAS_ADDITIONAL_BED { set; get; }

        public string AdmissionType { set; get; }


        public DataSet PTMasterGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_PTMasterGet", Param);



            return DS;

        }

        public void PTMasterAdd(out string IPM_ID)
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPM_BRANCH_ID", IPM_BRANCH_ID);
            Param.Add("IPM_PT_ID", IPM_PT_ID);
            Param.Add("IPM_PT_NAME", IPM_PT_NAME);
            Param.Add("IPM_AGE", IPM_AGE);

            Param.Add("IPM_DR_CODE", IPM_DR_CODE);
            Param.Add("IPM_DR_NAME", IPM_DR_NAME);
            Param.Add("IPM_INS_CODE", IPM_INS_CODE);
            Param.Add("IPM_INS_NAME", IPM_INS_NAME);


            Param.Add("IPM_DEP_ID", IPM_DEP_ID);
            Param.Add("IPM_DEP_NAME", IPM_DEP_NAME);
            Param.Add("IPM_PT_TYPE", IPM_PT_TYPE);
            Param.Add("IPM_TYPE", IPM_TYPE);
            Param.Add("IPM_REF_DR_CODE", IPM_REF_DR_CODE);
            Param.Add("VisitDate", VisitDate);
            Param.Add("UserId", UserId);

            IPM_ID = objDB.ExecuteNonQueryReturn("HMS_SP_IP_PTMasterAdd", Param);

        }

        public void IPAddToWaiting()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BranchID", IPM_BRANCH_ID);
            Param.Add("PTID", IPM_PT_ID);
            Param.Add("IAS_DATE", IAS_DATE);
            Param.Add("admissiontype", AdmissionType);
            Param.Add("doctorid", IPM_DR_CODE);

            Param.Add("doctorname", IPM_DR_NAME);
            Param.Add("department", IPM_DEP_ID);

            Param.Add("IAS_EMG_CONT_PERSON", IAS_EMG_CONT_PERSON);
            Param.Add("IAS_EMG_CONT_RELATION", IAS_EMG_CONT_RELATION);
            Param.Add("IAS_EMG_CONT_NUMBER", IAS_EMG_CONT_NUMBER);
            Param.Add("IAS_WARD_NO", IAS_WARD_NO);
            Param.Add("IAS_ROOM_NO", IAS_ROOM_NO);
            Param.Add("IAS_BED_NO", IAS_BED_NO);
            Param.Add("IAS_DRG_CODE", IAS_DRG_CODE);
            Param.Add("IAS_DRG_AMOUNT", IAS_DRG_AMOUNT);
            Param.Add("IAS_AUTHORIZATION_ID", IAS_AUTHORIZATION_ID);
            Param.Add("IAS_AUTHORIZATION_START", IAS_AUTHORIZATION_START);
            Param.Add("IAS_AUTHORIZATION_END", IAS_AUTHORIZATION_END);
            Param.Add("IAS_LENTH_OF_STAY", IAS_LENTH_OF_STAY);
            Param.Add("IAS_ADDITIONAL_BED", IAS_ADDITIONAL_BED);

            objDB.ExecuteNonQuery("HMS_SP_IP_AddToWaiting", Param);

        }


        public void IP_StartProcess()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("IPM_BRANCH_ID", IPM_BRANCH_ID);
            Param.Add("IPM_ID", IPM_ID);
            Param.Add("IPM_PT_ID", IPM_PT_ID);
            Param.Add("IPM_DR_CODE", IPM_DR_CODE);
            Param.Add("VisitDate", VisitDate);
            Param.Add("VisitDateTime", VisitDateTime);
            Param.Add("UserId", UserId);
            objDB.ExecuteNonQuery("HMS_SP_IP_StartProcess", Param);

        }

        public void IPSalesOrderAdd()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", IPM_BRANCH_ID);
            Param.Add("patientmasterid", IPM_ID);
            Param.Add("doctorid", IPM_DR_CODE);
            Param.Add("date", IPM_DATE);
            Param.Add("AdmissionNo", AdmissionNo);

            objDB.ExecuteNonQuery("HMS_SP_IP_SalesOrderAdd", Param);

        }

        public void IPSalesOrderTransAdd(out string SO_TransID)
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BranchID", IPM_BRANCH_ID);
            Param.Add("EMRID", IPM_ID);
            Param.Add("DR_ID", IPM_DR_CODE);
            Param.Add("DR_Name", DR_Name);
            Param.Add("SO_ID", SO_ID);

            Param.Add("Serv_Code", ServCode);
            Param.Add("Serv_Desc", ServDesc);
            Param.Add("Serv_Qty", ServQty);
            Param.Add("Serv_Type", ServType);

            SO_TransID = objDB.ExecuteNonQueryReturn("HMS_SP_IP_SalesOrderTransAdd", Param);

        }
    }
}
