﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace EMR_IP_BAL
{
    public class IP_RecoveryChart
    {
        dbOperation objDB = new dbOperation();

        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string IAD_ANESTHESIA_TYPE { set; get; }
        public string IAD_PT_CONDITION { set; get; }
        public string IAD_VENTILATION { set; get; }
        public string IAD_PROT_AIRWAY_REFLEXES { set; get; }
        public string IAD_AIRWAY_OBST_SIGNS { set; get; }
        public string IAD_ARTI_AIRWAY_DEVICE { set; get; }
        public string IAD_VASCULAR_CASES { set; get; }
        public string IAD_DRAINAGE { set; get; }
        public string IAD_PT_SPECIAL_EVENTS { set; get; }
        public string IAD_RR_PROCEDURES { set; get; }



        public string IRRM_MON_ID { set; get; }
        public string IRRM_TIME { set; get; }
        public string IRRM_BP_SYSTOLIC { set; get; }
        public string IRRM_BP_DIASTOLIC { set; get; }
        public string IRRM_PULSE { set; get; }
        public string IRRM_SPO2 { set; get; }
        public string IRRM_RESPIRATION { set; get; }
        public string IRRM_TEMP { set; get; }
        public string IRRM_PAIN_SCORE { set; get; }
        public string IRRM_MOTOR_BLOCK { set; get; }
        public string IRRM_COMMENT { set; get; }


        public string IRRM_MED_ID { set; get; }
        public string IRRM_SOLUTION { set; get; }
        public string IRRM_VOLUME { set; get; }
        public string IRRM_STARTED { set; get; }
        public string IRRM_FINISHED { set; get; }
        public string IRRM_DRUG_CODE { set; get; }
        public string IRRM_DRUG_NAME { set; get; }
        public string IRRM_DOSE { set; get; }
        public string IRRM_ROUTE { set; get; }
        public string IRRM_GIVEN_BY { set; get; }
        public string IRRM_TIME_GIVEN { set; get; }


        public string IRNN_NOTES_ID { set; get; }
        public string IRNN_TIME { set; get; }
        public string IRNN_REMARKS { set; get; }

        public string IRRS_STATUS_ID { set; get; }
        public string IRRS_ISSUE_TYPE { set; get; }
        public string IRRS_ISSUE_SUB_TYPE { set; get; }
        public string IRRS_ISSUE_TIMING { set; get; }
        public string IRRS_SCORE { set; get; }
        public string IRRS_DISCHARGE_TYPE { set; get; }







        public string IRRM_RECEIVED_FROM { set; get; }
        public string IRRM_ACCOMPANIED_BY { set; get; }
        public string IRRM_ARRIVAL_DATE { set; get; }

        public string IRRM_MOTOR_BLOCK_INTENSITY { set; get; }
        public string IRRM_SEDATION_AGITATION_SCORE { set; get; }
        public string IRRM_PATIENT_TEACHING { set; get; }
        public string IRRM_DISCHARGED_WITH { set; get; }

        public string IRRM_DISCHARGED_TO { set; get; }

        public string IRRM_RR_NURSE { set; get; }
        public string IRRM_ANESTHESIOLOGIST { set; get; }
        public string IRRM_WARD_NURSE { set; get; }
        public string IRRM_WARD_CALL_TIME { set; get; }
        public string IRRM_DISCHARGE_DATE { set; get; }

        public string UserID { set; get; }

        public DataSet AnesthesiaDtlsGet(string Criteria)
        {


            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IP_RecovAnesthesiaDtlsGet", Param);
            return DS;
        }

        public void AnesthesiaDtlsAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("IAD_BRANCH_ID", BranchID);
            Param.Add("IAD_ID", EMRID);
            Param.Add("IAD_PT_ID", PTID);
            Param.Add("IAD_ANESTHESIA_TYPE", IAD_ANESTHESIA_TYPE);
            Param.Add("IAD_PT_CONDITION", IAD_PT_CONDITION);
            Param.Add("IAD_VENTILATION", IAD_VENTILATION);
            Param.Add("IAD_PROT_AIRWAY_REFLEXES", IAD_PROT_AIRWAY_REFLEXES);
            Param.Add("IAD_AIRWAY_OBST_SIGNS", IAD_AIRWAY_OBST_SIGNS);
            Param.Add("IAD_ARTI_AIRWAY_DEVICE", IAD_ARTI_AIRWAY_DEVICE);
            Param.Add("IAD_VASCULAR_CASES", IAD_VASCULAR_CASES);
            Param.Add("IAD_DRAINAGE", IAD_DRAINAGE);
            Param.Add("IAD_PT_SPECIAL_EVENTS", IAD_PT_SPECIAL_EVENTS);
            Param.Add("IAD_RR_PROCEDURES", IAD_RR_PROCEDURES);
            Param.Add("UserID", UserID);
            objDB.ExecuteNonQuery("HMS_SP_IP_RecovAnesthesiaDtlsAdd", Param);

        }

        public DataSet RecoveryRoomMonitoringGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPRecoveryRoomMonitoringGet", Param);
            return DS;
        }

        public void RecoveryRoomMonitoringAdd()
        {


            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("IRRM_BRANCH_ID", BranchID);
            Param.Add("IRRM_ID", EMRID);
            Param.Add("IRRM_PT_ID", PTID);
            Param.Add("IRRM_MON_ID", IRRM_MON_ID);
            Param.Add("IRRM_TIME", IRRM_TIME);
            Param.Add("IRRM_BP_SYSTOLIC", IRRM_BP_SYSTOLIC);
            Param.Add("IRRM_BP_DIASTOLIC", IRRM_BP_DIASTOLIC);
            Param.Add("IRRM_PULSE", IRRM_PULSE);
            Param.Add("IRRM_SPO2", IRRM_SPO2);
            Param.Add("IRRM_RESPIRATION", IRRM_RESPIRATION);
            Param.Add("IRRM_TEMP", IRRM_TEMP);
            Param.Add("IRRM_PAIN_SCORE", IRRM_PAIN_SCORE);
            Param.Add("IRRM_MOTOR_BLOCK", IRRM_MOTOR_BLOCK);
            Param.Add("IRRM_COMMENT", IRRM_COMMENT);
            Param.Add("UserID", UserID);
            objDB.ExecuteNonQuery("HMS_SP_IPRecoveryRoomMonitoringAdd", Param);

        }

        public DataSet RecoveryRoomMedicationGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPRecoveryRoomMedicationGet", Param);
            return DS;
        }


        public void RecoveryRoomMedicationAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("IRRM_BRANCH_ID", BranchID);
            Param.Add("IRRM_ID", EMRID);
            Param.Add("IRRM_PT_ID", PTID);
            Param.Add("IRRM_MED_ID", IRRM_MED_ID);
            Param.Add("IRRM_SOLUTION", IRRM_SOLUTION);
            Param.Add("IRRM_VOLUME", IRRM_VOLUME);
            Param.Add("IRRM_STARTED", IRRM_STARTED);
            Param.Add("IRRM_FINISHED", IRRM_FINISHED);
            Param.Add("IRRM_DRUG_CODE", IRRM_DRUG_CODE);
            Param.Add("IRRM_DRUG_NAME", IRRM_DRUG_NAME);
            Param.Add("IRRM_DOSE", IRRM_DOSE);
            Param.Add("IRRM_ROUTE", IRRM_ROUTE);
            Param.Add("IRRM_GIVEN_BY", IRRM_GIVEN_BY);
            Param.Add("IRRM_TIME_GIVEN", IRRM_TIME_GIVEN);
            Param.Add("UserID", UserID);
            objDB.ExecuteNonQuery("HMS_SP_IPRecoveryRoomMedicationAdd", Param);

        }


        public DataSet RecoveryNurseNotesGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPRecoveryNurseNotesGet", Param);
            return DS;
        }


        public void RecoveryNurseNotesAdd()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("IRNN_BRANCH_ID", BranchID);
            Param.Add("IRNN_ID", EMRID);
            Param.Add("IRNN_PT_ID", PTID);
            Param.Add("IRNN_NOTES_ID", IRNN_NOTES_ID);
            Param.Add("IRNN_TIME", IRNN_TIME);
            Param.Add("IRNN_REMARKS", IRNN_REMARKS);

            Param.Add("UserID", UserID);
            objDB.ExecuteNonQuery("HMS_SP_IPRecoveryNurseNotesAdd", Param);

        }

        public DataSet RecoveryRoomPTStatusGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPRecoveryRoomPTStatusGet", Param);
            return DS;
        }

        public void RecoveryRoomPTStatusAdd()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("IRRS_BRANCH_ID", BranchID);
            Param.Add("IRRS_ID", EMRID);
            Param.Add("IRRS_PT_ID", PTID);
            Param.Add("IRRS_STATUS_ID", IRRS_STATUS_ID);
            Param.Add("IRRS_ISSUE_TYPE", IRRS_ISSUE_TYPE);
            Param.Add("IRRS_ISSUE_SUB_TYPE", IRRS_ISSUE_SUB_TYPE);
            Param.Add("IRRS_ISSUE_TIMING", IRRS_ISSUE_TIMING);
            Param.Add("IRRS_SCORE", IRRS_SCORE);
            Param.Add("IRRS_DISCHARGE_TYPE", IRRS_DISCHARGE_TYPE);

            objDB.ExecuteNonQuery("HMS_SP_IPRecoveryRoomPTStatusAdd", Param);

        }


        public DataSet RecoveryRoomMasterGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_IPRecoveryRoomMasterGet", Param);
            return DS;
        }


        public void RecoveryRoomMasterAdd()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();





            Param.Add("IRRM_BRANCH_ID", BranchID);
            Param.Add("IRRM_ID", EMRID);
            Param.Add("IRRM_PT_ID", PTID);
            Param.Add("IRRM_RECEIVED_FROM", IRRM_RECEIVED_FROM);
            Param.Add("IRRM_ACCOMPANIED_BY", IRRM_ACCOMPANIED_BY);
            Param.Add("IRRM_ARRIVAL_DATE", IRRM_ARRIVAL_DATE);
            Param.Add("IRRM_PAIN_SCORE", IRRM_PAIN_SCORE);
            Param.Add("IRRM_MOTOR_BLOCK_INTENSITY", IRRM_MOTOR_BLOCK_INTENSITY);
            Param.Add("IRRM_SEDATION_AGITATION_SCORE", IRRM_SEDATION_AGITATION_SCORE);

            Param.Add("IRRM_PATIENT_TEACHING", IRRM_PATIENT_TEACHING);
            Param.Add("IRRM_DISCHARGED_WITH", IRRM_DISCHARGED_WITH);
            Param.Add("IRRM_DISCHARGED_TO", IRRM_DISCHARGED_TO);
            Param.Add("IRRM_COMMENT", IRRM_COMMENT);
            Param.Add("IRRM_RR_NURSE", IRRM_RR_NURSE);

            Param.Add("IRRM_ANESTHESIOLOGIST", IRRM_ANESTHESIOLOGIST);
            Param.Add("IRRM_WARD_NURSE", IRRM_WARD_NURSE);
            Param.Add("IRRM_WARD_CALL_TIME", IRRM_WARD_CALL_TIME);
            Param.Add("IRRM_DISCHARGE_DATE", IRRM_DISCHARGE_DATE);

            Param.Add("UserID", UserID);
            objDB.ExecuteNonQuery("HMS_SP_IPRecoveryRoomMasterAdd", Param);

        }

    }
}
